package com.ibuildapp.inventory.viewmodel

import android.app.Application
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Transformations
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.ibuildapp.inventory.model.ViewSearchInventoryItem
import java.util.*

class SearchItemsViewModel(application: Application, parentItemId: String?): BaseSearchViewModel(application, parentItemId) {

    private var isFolderSearch = true

    fun updateDataWithFilter(filter: String) {
        this.filter = filter

        reloadData()
    }

    fun getData(): LiveData<List<ViewSearchInventoryItem>> {
        if (viewList.value == null)
            reloadData()

        return viewList
    }

    private fun reloadData() {
        sourceLiveData?.removeObserver(observer)

        val filter = if (filter.isNotEmpty() && filter.length >= 2) filter.toLowerCase(Locale.ROOT) else "\$_NEVER_SEARCH_$"
        val filterString = "%$filter%"

        if (isFolderSearch)
            sourceLiveData = Transformations.map(inventoryDao.getItemsForSearch(parentItemId, filterString)) { input ->
                originalInput = input
                transform(input)
            }
        else
            sourceLiveData = Transformations.map(inventoryDao.getItemsForSearchWithSubFolders(parentItemId, filterString, filter)) { input ->
                originalInput = input
                transform(input)
            }
        sourceLiveData?.observeForever(observer)
    }


    fun isFolderSearch() = isFolderSearch

    fun updateIsFolder(isFolderSearch: Boolean) {
        this.isFolderSearch = isFolderSearch

        reloadData()
    }

    class ViewModelFactory(
            val application: Application,
            private val parentItemId: String?
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return SearchItemsViewModel(application, parentItemId) as T
        }
    }
}