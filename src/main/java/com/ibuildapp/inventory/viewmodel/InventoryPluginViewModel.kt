package com.ibuildapp.inventory.viewmodel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import com.ibuildapp.inventory.stuff.BaseSessionManager

class InventoryPluginViewModel
constructor(application: Application): AndroidViewModel(application) {

    fun isAuthorized() = !BaseSessionManager.INSTANCE.getUserId(getApplication()).isNullOrEmpty()
}