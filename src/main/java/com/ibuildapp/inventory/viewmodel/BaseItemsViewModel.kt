package com.ibuildapp.inventory.viewmodel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import com.ibuildapp.inventory.backend.InventoryIBAService
import com.ibuildapp.inventory.backend.model.*
import com.ibuildapp.inventory.database.InventoryDao
import com.ibuildapp.inventory.database.InventoryDatabase
import com.ibuildapp.inventory.model.InventoryItem
import com.ibuildapp.inventory.stuff.BaseSessionManager
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody

abstract class BaseItemsViewModel(application: Application): AndroidViewModel(application) {
    protected val inventoryDao: InventoryDao = InventoryDatabase.getInstance(application, BaseSessionManager.INSTANCE.getUserId(application)!!).getDao()

    fun removeFolder(id: String) {
        val item = inventoryDao.getItemById(id)
        item.active = false
        deleteItemFromServer(item)
    }

    fun removeItem(id: String) {
        val item = inventoryDao.getItemById(id)
        item.active = false
        deleteItemFromServer(item)
    }

    private fun deleteItemFromServer(item: InventoryItem): ItemResponse {
        val token = BaseSessionManager.INSTANCE.getSession(getApplication())
        val request = item.toServerItem()

        try {
            val mapParameters = mutableMapOf<String, RequestBody>()

            if (item.id.isNotEmpty())
                mapParameters["id"] = request.id.toString().toRequestBody("text/plain".toMediaType())

            mapParameters["inventory_id"] = request.inventoryId.toString().toRequestBody("text/plain".toMediaType())

            if (request.parentId != null)
                mapParameters["parent_id"] = request.parentId.toString().toRequestBody("text/plain".toMediaType())

            mapParameters["is_folder"] = request.isFolder.toString().toRequestBody("text/plain".toMediaType())
            mapParameters["name"] = request.name.toString().toRequestBody("text/plain".toMediaType())
            mapParameters["description"] = request.description.toString().toRequestBody("text/plain".toMediaType())

            if (!request.sku.isNullOrEmpty())
                mapParameters["sku"] = request.sku.toString().toRequestBody("text/plain".toMediaType())

            if (!request.barcode.isNullOrEmpty())
                mapParameters["barcode"] = request.barcode.toString().toRequestBody("text/plain".toMediaType())

            if (!request.barcodeFormat.isNullOrEmpty())
                mapParameters["barcode_format"] = request.barcodeFormat.toString().toRequestBody("text/plain".toMediaType())

            if (request.quantity != null)
                mapParameters["quantity"] = request.quantity.toString().toRequestBody("text/plain".toMediaType())

            if (request.price != null)
                mapParameters["price"] = request.price.toString().toRequestBody("text/plain".toMediaType())

            if (request.active == false)
                mapParameters["active"] = request.active.toString().toRequestBody("text/plain".toMediaType())

            request.images?.let { images ->
                mapParameters["images"] = images.joinToString().toRequestBody("text/plain".toMediaType())
            }

            for ((k, v) in mapParameters)
                println("logMap $k = $v")

            val response = if (item.id.isEmpty())
                InventoryIBAService.Factory.create().addItem(
                        "Bearer $token",
                        mapParameters,
                        request.files
                ).execute()
            else InventoryIBAService.Factory.create().updateItem(
                    "Bearer $token",
                    mapParameters,
                    request.files
            ).execute()

            if (response.isSuccessful && response.body() != null) {
                val body = response.body()
                val error = body?.error
                when (val savedItem = body?.data) {
                    null -> return ItemResponse(error = ResponseErrorData(message = "Unknown error."))
                    else -> {
                        if (error?.type != 200)
                            return ItemResponse(error = ResponseErrorData(message = error?.message
                                    ?: "Unknown error."))

                        loadDataFromNetwork()

                        return ItemResponse(error = null, data = savedItem)
                    }
                }
            } else
                return ItemResponse(error = ResponseErrorData(message = response.errorBody().toString()))
        } catch (error: Throwable) {
            return ItemResponse(error = ResponseErrorData(message = error.message ?: "Need internet"))
        }
    }

    private fun toLocalCategories(items: List<ServerCategory>): List<InventoryItem> {
        val result = mutableListOf<InventoryItem>()

        items.forEach { item ->
            result.add(item.toLocalCategory())
        }

        return result
    }

    private fun toLocalItems(items: List<ServerItemData>): List<InventoryItem> {
        val result = mutableListOf<InventoryItem>()

        items.forEach { item ->
            result.add(item.toLocalItem(folder = false))
        }

        return result
    }

    fun loadDataFromNetwork() {
        val token = BaseSessionManager.INSTANCE.getSession(getApplication())

        try {
            do {
                val timestamp = BaseSessionManager.INSTANCE.getLastUpdate(getApplication())
                val request = UpdatesDataRequest(timestamp)

                val response = InventoryIBAService.Factory.create().getUpdates("Bearer $token", request).execute()

                val categories = response.body()?.data?.categories
                val items = response.body()?.data?.items

                val itemsToUpdate = mutableListOf<InventoryItem>()
                val itemsToDelete: MutableList<String> = mutableListOf()

                if (response.isSuccessful && response.body() != null) {
                    val convertedItems = mutableListOf<InventoryItem>()

                    if (!categories.isNullOrEmpty())
                        convertedItems.addAll(toLocalCategories(categories))

                    if (!items.isNullOrEmpty())
                        convertedItems.addAll(toLocalItems(items))

                    convertedItems.forEach { item ->
                        if (item.active)
                            itemsToUpdate.add(item)
                        else itemsToDelete.add(item.id)
                    }

                    //сохраняем timestamp обновления
                    response.body()?.data?.timestamp?.let {
                        BaseSessionManager.INSTANCE.setLastUpdate(getApplication(), it)
                    }

                    // удаляем деактивированные записи
                    inventoryDao.deleteItemsByIds(itemsToDelete)
                    inventoryDao.insertInventoryItems(itemsToUpdate)
                } else break
            }
            //повторяем запросы, пока не вернутся пустой ответ
            while (itemsToUpdate.size != 0)
        } catch (error: Throwable) {
            error.printStackTrace()
        }
    }
}