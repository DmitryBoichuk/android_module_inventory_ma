package com.ibuildapp.inventory.viewmodel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.net.Uri
import com.google.zxing.BarcodeFormat
import com.ibuildapp.inventory.backend.InventoryIBAService
import com.ibuildapp.inventory.backend.model.ItemResponse
import com.ibuildapp.inventory.backend.model.Product
import com.ibuildapp.inventory.backend.model.ResponseErrorData
import com.ibuildapp.inventory.database.InventoryDatabase
import com.ibuildapp.inventory.model.InventoryItem
import com.ibuildapp.inventory.model.ViewAddInventoryItem
import com.ibuildapp.inventory.model.ViewImageItem
import com.ibuildapp.inventory.model.ViewImageType
import com.ibuildapp.inventory.stuff.BaseSessionManager
import kotlinx.coroutines.*
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import java.util.*


class AddDataViewModel(application: Application,
                       val itemId: String? = null,
                       val isFolder: Boolean,
                       val parentItemId: String? = null,
                       val scannedText: String? = null,
                       scannedFormat: BarcodeFormat? = null,
                       product: Product? = null
) : AndroidViewModel(application) {

    private val dao = InventoryDatabase.getInstance(application, BaseSessionManager.INSTANCE.getUserId(application)!!).getDao()
    private val newItem: MutableLiveData<ViewAddInventoryItem> = MutableLiveData()

    init {
        if (itemId.isNullOrEmpty()) {
            newItem.postValue(ViewAddInventoryItem(
                    id = null,
                    isFolder = isFolder,
                    parentId = if (parentItemId.isNullOrEmpty()) null else parentItemId,
                    barcode = scannedText,
                    barcodeFormat = scannedFormat?.toString() ?: "",
                    name = product?.productName,
                    nameLower = product?.productName?.toLowerCase(Locale.ROOT),
                    notes = product?.description,
                    imageUrl = transformImageUrls(product?.images),
                    inventoryId = BaseSessionManager.INSTANCE.getCurrentInventory(getApplication())
                )
            )
        } else
            runBlocking {
                GlobalScope.launch {
                    newItem.postValue(transform(dao.getItemById(itemId)))
                }
            }
    }

    private fun transformImageUrls(imageList: List<String>?): MutableList<ViewImageItem> {
        val images = mutableListOf<ViewImageItem>()
        imageList?.let { imageUrls ->
            imageUrls.forEach { url ->
                if (url.isNotEmpty())
                    images.add(ViewImageItem(url = url, type = ViewImageType.ITEM))
            }
        }

        images.add(ViewImageItem(type = ViewImageType.ADD))
        return images
    }

    private fun transform(item: InventoryItem): ViewAddInventoryItem {

        return ViewAddInventoryItem(
                id = item.id,
                isFolder = item.isFolder,
                parentId = item.parentId,
                sku = item.sku,
                notes = item.notes,
                price = item.price,
                name = item.name,
                nameLower = item.nameLower,
                category = item.category,
                quantity = item.quantity,
                barcode = item.barcode,
                barcodeFormat = item.barcodeFormat,
                date = item.createdAt,
                imageUrl = transformImageUrls(item.imageUrl),
                inventoryId = item.inventoryId
        )
    }

    fun getData() = newItem

    fun updateBarcode(contents: String, barcodeFormat: BarcodeFormat) {
        val item = newItem.value ?: return

        item.barcode = contents
        item.barcodeFormat = barcodeFormat.name
    }

    fun clearQROrBarcode() {
        val item = newItem.value ?: return

        item.barcode = null
        item.barcodeFormat = null
    }

    fun addImages(uriList: MutableList<Uri>) {
        val item = newItem.value

        val imageList = item?.imageUrl ?: return

        val newList = mutableListOf<ViewImageItem>()
        imageList.forEach {
            if (it.type == ViewImageType.ITEM)
                newList.add(it)
        }

        uriList.forEach {
            newList.add(ViewImageItem(uri = it, type = ViewImageType.ITEM))
        }

        newList.add(ViewImageItem(type = ViewImageType.ADD))
        item.imageUrl = newList
        newItem.value = item
    }

    @Throws(Exception::class)
    fun saveFile(name: String? = null,
                         note: String? = null,
                         price: String? = null,
                         quantity: String? = null,
                         category: String? = null,
                         sku: String? = null) {

        val item = newItem.value ?: return
        item.apply {
            this.inventoryId = BaseSessionManager.INSTANCE.getCurrentInventory(getApplication())
            this.name = name
            this.nameLower = name?.toLowerCase(Locale.ROOT) ?: ""

            this.sku = sku
            this.category = category

            this.quantity = quantity?.let { if (it.isNotEmpty()) it.toInt() else 0 } ?: 0
            this.price = price?.let { if (it.isNotEmpty()) it.toFloat() else 0f } ?: 0f

            this.notes = note
            this.date = Date(System.currentTimeMillis())
        }

        newItem.value?.let {
            val response = saveItemToServer(it)
            if (response.data != null)
                dao.insertInventoryItem(response.data!!.toLocalItem(folder = false))
           else
                throw Exception(response.error?.message ?: "Invalid response")
        }
    }

    @Throws(Exception::class)
    fun saveFolder(name: String? = null, note: String? = null) {
        newItem.value?.let {
            it.name = name
            it.nameLower = name?.toLowerCase(Locale.ROOT) ?: ""
            it.notes = note
            it.date = Date(System.currentTimeMillis())

            newItem.value?.let { addItem ->
                val response: ItemResponse = saveItemToServer(addItem)
                if (response.data != null)
                    dao.insertInventoryItem(response.data!!.toLocalItem(folder = true))
                else
                    throw Exception(response.error?.message ?: "Invalid response")
            }
        }
    }

    fun updateFolder(name: String? = null, note: String? = null) {
        newItem.value?.let {
            it.name = name
            it.nameLower = name?.toLowerCase(Locale.ROOT) ?: ""
            it.notes = note
            it.date = Date(System.currentTimeMillis())

            newItem.value?.let { addItem ->
                val response: ItemResponse = saveItemToServer(addItem)
                if (response.data != null)
                    dao.insertInventoryItem(response.data!!.toLocalItem(folder = true))
                else
                    throw Exception(response.error?.message ?: "Invalid response")
            }
        }
    }

    private fun saveItemToServer(item: ViewAddInventoryItem): ItemResponse {
        val token = BaseSessionManager.INSTANCE.getSession(getApplication())
        val request = item.toServerItem(getApplication())

        try {
            val mapParameters = mutableMapOf<String, RequestBody>()

            if (!item.id.isNullOrEmpty())
                mapParameters["id"] = request.id.toString().toRequestBody("text/plain".toMediaType())

            mapParameters["inventory_id"] = request.inventoryId.toString().toRequestBody("text/plain".toMediaType())

            if (request.parentId != null)
                mapParameters["parent_id"] = request.parentId.toString().toRequestBody("text/plain".toMediaType())

            mapParameters["is_folder"] = request.isFolder.toString().toRequestBody("text/plain".toMediaType())
            mapParameters["name"] = request.name.toString().toRequestBody("text/plain".toMediaType())
            mapParameters["description"] = request.description.toString().toRequestBody("text/plain".toMediaType())

            if (!request.sku.isNullOrEmpty())
                mapParameters["sku"] = request.sku.toString().toRequestBody("text/plain".toMediaType())

            if (!request.barcode.isNullOrEmpty())
                mapParameters["barcode"] = request.barcode.toString().toRequestBody("text/plain".toMediaType())

            if (!request.barcodeFormat.isNullOrEmpty())
                mapParameters["barcode_format"] = request.barcodeFormat.toString().toRequestBody("text/plain".toMediaType())

            if (request.quantity != null)
                mapParameters["quantity"] = request.quantity.toString().toRequestBody("text/plain".toMediaType())

            if (request.price != null)
                mapParameters["price"] = request.price.toString().toRequestBody("text/plain".toMediaType())

            request.images?.let { images ->
                mapParameters["images"] = images.joinToString().toRequestBody("text/plain".toMediaType())
            }

            if (request.active == false)
                mapParameters["active"] = request.active.toString().toRequestBody("text/plain".toMediaType())

            for ((k, v) in mapParameters)
                println("logMap $k = $v")

            val response = if (item.id.isNullOrEmpty())
                InventoryIBAService.Factory.create().addItem(
                    "Bearer $token",
                    mapParameters,
                    request.files
                ).execute()
            else InventoryIBAService.Factory.create().updateItem(
                    "Bearer $token",
                    mapParameters,
                    request.files
                ).execute()

            if (response.isSuccessful && response.body() != null) {
                val body = response.body()
                val error = body?.error
                when (val savedItem = body?.data) {
                    null -> return ItemResponse(error = ResponseErrorData(message = "Unknown error."))
                    else -> {
                        if (error?.type != 200)
                            return ItemResponse(error = ResponseErrorData(message = error?.message
                                    ?: "Unknown error."))

                        dao.insertInventoryItem(savedItem.toLocalItem(folder = item.isFolder))

                        return ItemResponse(error = null, data = savedItem)
                    }
                }
            } else
                return ItemResponse(error = ResponseErrorData(message = response.errorBody().toString()))
        } catch (error: Throwable) {
            return ItemResponse(error = ResponseErrorData(message = error.message ?: "Need internet"))
        }
    }

    fun updateFile(name: String? = null,
                   note: String? = null,
                   price: String? = null,
                   quantity: String? = null,
                   category: String? = null,
                   sku: String? = null) {

        newItem.value?.let {
            it.name = name
            it.nameLower = name?.toLowerCase(Locale.ROOT) ?: ""

            it.sku = sku
            it.category = category

            it.quantity = quantity?.let { if (it.isNotEmpty()) it.toInt() else 0 } ?: 0
            it.price = price?.let { if (it.isNotEmpty()) it.toFloat() else 0f } ?: 0f

            it.notes = note
            it.date = Date(System.currentTimeMillis())

            newItem.value?.let { addItem ->
                val response: ItemResponse = saveItemToServer(addItem)
                if (response.data != null)
                    dao.insertInventoryItem(response.data!!.toLocalItem(folder = false))
                else
                    throw Exception(response.error?.message ?: "Invalid response")
            }
        }
    }

    fun nameChanged(name: String) {
        newItem.value?.name = name
    }

    fun noteChanged(note: String) {
        newItem.value?.notes = note
    }

    fun categoryChanged(category: String) {
        newItem.value?.category = category
    }

    fun skuChanged(sku: String) {
        newItem.value?.sku = sku
    }

    fun priceChanged(price: String) {
        newItem.value?.price = price.let { if (it.isNotEmpty()) it.toFloat() else 0f }
    }
    fun quantityChanged(quantity: String) {
        newItem.value?.quantity = quantity.let { if (it.isNotEmpty()) it.toInt() else 0 }
    }

    class ViewModelFactory(
            val application: Application,
            val itemId: String?,
            val isFolder: Boolean,
            val parentItemId: String?,
            val scannedText: String? = null,
            val scannedFormat: BarcodeFormat? = null,
            val product: Product? = null
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return AddDataViewModel(application, itemId, isFolder, parentItemId, scannedText, scannedFormat, product) as T
        }
    }
}