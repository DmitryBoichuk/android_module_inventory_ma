package com.ibuildapp.inventory.viewmodel

import android.app.Application
import android.arch.lifecycle.*
import com.ibuildapp.inventory.R
import com.ibuildapp.inventory.model.InventoryItemWithCounts
import com.ibuildapp.inventory.model.User
import com.ibuildapp.inventory.model.ViewInventoryItem
import com.ibuildapp.inventory.model.ViewInventoryType
import com.ibuildapp.inventory.stuff.BaseSessionManager
import com.ibuildapp.inventory.stuff.ItemUtils
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class ItemsViewModel(application: Application, private val parentItemId: String?): BaseItemsViewModel(application) {
    private var dbUser: User? = null
    private var dbCount: Int? = null

    private val userSource = inventoryDao.getUserById(BaseSessionManager.INSTANCE.getUserId(getApplication())!!)
    private val countSource = inventoryDao.getItemsCount()

    private val mainScope = MainScope()

    private val userSourceObserver = Observer<User> {
        dbUser = it

        val user = dbUser ?: return@Observer
        val count = dbCount ?: return@Observer

        transformCounts(user, count)
    }

    private val countSourceObserver = Observer<Int> {
        dbCount = it

        val user = dbUser ?: return@Observer
        val count = dbCount ?: return@Observer

        transformCounts(user, count)
    }

    private val needFabLiveData = MutableLiveData<Boolean>()

    init {
        needFabLiveData.postValue(true)

        userSource.observeForever(userSourceObserver)
        countSource.observeForever(countSourceObserver)

        runBlocking {
            mainScope.launch {
                launch(Dispatchers.IO){
                    loadDataFromNetwork()
                }
            }
        }
    }

    private fun transformCounts(user: User, count: Int) {
        val needFab = count < user.planLimit
        needFabLiveData.postValue(needFab)
    }

    fun getItems(): LiveData<List<ViewInventoryItem>> {
        return Transformations.map(inventoryDao.getItemsForParent(parentItemId)) { transform(it) }
    }

    private fun transform(items: List<InventoryItemWithCounts>): List<ViewInventoryItem> {
        val result = mutableListOf<ViewInventoryItem>()

        if (items.isNotEmpty())
            result.add(ViewInventoryItem("Search", ViewInventoryType.SEARCH, "", textId = "Search"))

        val folders = mutableListOf<InventoryItemWithCounts>()
        val files = mutableListOf<InventoryItemWithCounts>()

        items.forEach {
            if (it.isFolder)
                folders.add(it)
            else files.add(it)
        }

        if (folders.isNotEmpty())
            result.add(ViewInventoryItem("Folders", ViewInventoryType.TITLE, getApplication<Application>().getString(R.string.inventory_folder_title), textId = "Folders"))

        folders.forEachIndexed { index, item ->
            result.add(ItemUtils.transformInventoryFolder(item, getApplication()))

            if (index != folders.size - 1)
                result.add(ViewInventoryItem (
                        id = item.id+ " _separator",
                        viewInventoryType = ViewInventoryType.DELIMITER,
                        textId = item.id+ " _separator"
                    )
                )
        }

        if (files.isNotEmpty())
            result.add(ViewInventoryItem("Items", ViewInventoryType.TITLE, getApplication<Application>().getString(R.string.inventory_item_title), textId = "Items"))

        files.forEachIndexed { index, item ->

            result.add(ItemUtils.transformInventoryItem(item, getApplication()))

            if (index != files.size - 1)
                result.add(ViewInventoryItem (
                        id = item.id+ " _separator",
                        viewInventoryType = ViewInventoryType.DELIMITER,
                        textId = item.id+ " _separator"
                    )
                )
        }

        return result
    }

    fun getFabLiveData(): LiveData<Boolean> = needFabLiveData

    override fun onCleared() {
        userSource.removeObserver(userSourceObserver)
        countSource.removeObserver(countSourceObserver)
    }

    class ViewModelFactory(
            val application: Application,
            val parentItemId: String?
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return ItemsViewModel(application, parentItemId) as T
        }
    }
}