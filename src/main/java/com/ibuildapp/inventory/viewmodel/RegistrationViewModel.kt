package com.ibuildapp.inventory.viewmodel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.ibuildapp.inventory.backend.InventoryIBAService
import com.ibuildapp.inventory.backend.model.LoginResult
import com.ibuildapp.inventory.backend.model.RegisterDataRequest
import com.ibuildapp.inventory.database.InventoryDatabase
import com.ibuildapp.inventory.model.Result
import com.ibuildapp.inventory.model.UserResponse
import com.ibuildapp.inventory.stuff.BaseSessionManager
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class RegistrationViewModel(application: Application): AndroidViewModel(application) {
    private val registerResultLiveData = MutableLiveData<LoginResult>()

    fun getData(): LiveData<LoginResult> = registerResultLiveData

    suspend fun register(name: String, lastName: String, email: String, password: String) {
        registerResultLiveData.postValue(LoginResult(Result.LOADING))

        val userResponse: UserResponse = withContext(Dispatchers.IO) { registerUser(name, lastName, email, password) }
        println(userResponse);
        val loginResult = when {
            !userResponse.error.isNullOrEmpty() -> LoginResult(result = Result.ERROR, error = userResponse.error)
            userResponse.user != null -> {
                val serverUser = userResponse.user
                BaseSessionManager.INSTANCE.setCurrentUser(getApplication(), serverUser.id, userEmail = serverUser.email, userPassword = password)
                BaseSessionManager.INSTANCE.setSession(getApplication(), userResponse.session)
                BaseSessionManager.INSTANCE.setRememberMe(getApplication(), true)
                BaseSessionManager.INSTANCE.setCurrentInventory(getApplication(), userResponse.user.inventories.first())
                val dao = InventoryDatabase.getInstance(getApplication(), serverUser.id).getDao()

                val localUser = serverUser.toLocalUser()
                dao.insertUser(localUser)

                LoginResult(result = Result.SUCCESS)
            }
            else -> LoginResult(result = Result.ERROR, error = "User is null")
        }

        registerResultLiveData.postValue(loginResult)
    }

    private suspend fun registerUser(name: String, lastName: String, email: String, password: String): UserResponse = withContext(Dispatchers.IO) {
        val request = RegisterDataRequest(firstName = name, lastName = lastName, email = email, password = password)
        try {
            val response = InventoryIBAService.Factory.create().register(request).execute()
            val user = response.body()?.data
            val error = response.body()?.error

            if (response.isSuccessful && response.body() != null) {
                when (user) {
                    null -> return@withContext UserResponse(error = error?.message ?: "Unknown error")
                    else -> return@withContext UserResponse(user = user, session = user.token)
                }
            } else
                return@withContext UserResponse(error = response.errorBody().toString())

        } catch (exception: Throwable) {
            return@withContext UserResponse(error = "Need internet")
        }
    }
}