package com.ibuildapp.inventory.viewmodel

import android.app.Application
import android.arch.lifecycle.*
import com.google.zxing.BarcodeFormat
import com.ibuildapp.inventory.model.ViewBarcodeResult
import com.ibuildapp.inventory.model.ViewSearchInventoryItem
import com.ibuildapp.inventory.stuff.ItemUtils
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.util.*

class SearchQRViewModel(application: Application): BaseSearchViewModel(application, parentItemId = null) {
    private var state: MutableLiveData<State> = MutableLiveData()
    private var barcodeLiveData: MutableLiveData<ViewBarcodeResult> = MutableLiveData()

    init {
        state.postValue(State.SCAN_TO_SEARCH)
    }

    fun getState(): LiveData<State> = state

    fun getData(): LiveData<List<ViewSearchInventoryItem>> {
        if (viewList.value == null)
            reloadData()

        return viewList
    }

    fun getBarcodeData():LiveData<ViewBarcodeResult> = barcodeLiveData

    private fun reloadData() {
        sourceLiveData?.removeObserver(observer)

        val filter = if (filter.isNotEmpty() && filter.length >= 2) filter.toLowerCase(Locale.ROOT) else "\$_NEVER_SEARCH_$"
        val filterString = "%$filter%"

        sourceLiveData = Transformations.map(inventoryDao.getItemsForSearchWithSubFolders(null, filterString, filter)) { input ->
            originalInput = input
            transform(input)
        }

        sourceLiveData?.observeForever(observer)
    }

    fun updateDataWithFilter(filter: String) {
        this.filter = filter

        if (filter.isNullOrEmpty())
            state.postValue(State.SCAN_TO_SEARCH)
        else state.postValue(State.NAME_SEARCH)

        reloadData()
    }

    fun processContent(contents: String, formatName: String) {
        runBlocking {

            GlobalScope.launch(Dispatchers.IO) {
                val item = inventoryDao.findInventoryItemByCode(contents)
                val castedItem = item?.let { if (it.isFolder) ItemUtils.transformInventoryFolder(item, getApplication()) else ItemUtils.transformInventoryItem(item, getApplication())  }

                state.postValue(State.BARCODE_RESULT)
                barcodeLiveData.postValue(ViewBarcodeResult(contents, BarcodeFormat.valueOf(formatName), castedItem))
            }
        }
    }

    class ViewModelFactory(
            val application: Application
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return SearchQRViewModel(application) as T
        }
    }

    enum class State {
        SCAN_TO_SEARCH, NAME_SEARCH, BARCODE_RESULT
    }
}