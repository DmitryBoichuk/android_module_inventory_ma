package com.ibuildapp.inventory.viewmodel

import android.app.Application
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import android.support.v4.content.ContextCompat
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import com.ibuildapp.inventory.R
import com.ibuildapp.inventory.model.InventoryItemWithCounts
import com.ibuildapp.inventory.model.ViewInventoryType
import com.ibuildapp.inventory.model.ViewSearchInventoryItem
import java.util.*

open class BaseSearchViewModel(application: Application, val parentItemId: String?): BaseItemsViewModel(application) {
    protected var filter = ""
    protected val viewList: MutableLiveData<List<ViewSearchInventoryItem>> = MutableLiveData()
    protected var sourceLiveData: LiveData<List<ViewSearchInventoryItem>>? = null

    protected var originalInput: List<InventoryItemWithCounts>? = null

    protected val observer: Observer<List<ViewSearchInventoryItem>> = Observer { items ->
        viewList.postValue(items)
    }

    protected fun transform(input: List<InventoryItemWithCounts>?): List<ViewSearchInventoryItem> {
        val result = mutableListOf<ViewSearchInventoryItem>()
        if (input.isNullOrEmpty())
            return result

        val folders = mutableListOf<InventoryItemWithCounts>()
        val files = mutableListOf<InventoryItemWithCounts>()

        input.forEach {
            if (it.isFolder)
                folders.add(it)
            else files.add(it)
        }

        if (!folders.isNullOrEmpty()) {
            result.add(ViewSearchInventoryItem(
                    id = "Folders",
                    viewInventoryType = ViewInventoryType.TITLE,
                    name = getApplication<Application>().getString(R.string.inventory_folder_title),
                    textId = "Folders",
                    filter = filter,
                    spannableName = SpannableString(filter)
                )
            )

            folders.forEachIndexed { index, item ->
                val url: String? = item.imageUrl?.let { if (it.isNotEmpty() && it[0].isNotEmpty()) it[0] else null }

                result.add(ViewSearchInventoryItem(
                        id = item.id,
                        viewInventoryType = ViewInventoryType.FOLDER,
                        name = if (item.name.isNullOrEmpty())
                            getApplication<Application>().getString(R.string.inventory_without_name)
                        else item.name,
                        foldersCount = item.folderCount ?: 0,
                        filesCount = item.filesCount ?: 0,
                        textId = "ID: " + item.id.replace("_folder", ""),
                        spannableName = createColoredString(item.name ?: "", filter),
                        filter = filter,
                        firstImageUrl = url
                    )
                )

                if (index != folders.size - 1)
                    result.add(ViewSearchInventoryItem (
                            id = item.id + " _separator",
                            viewInventoryType = ViewInventoryType.DELIMITER,
                            textId = item.id+ " _separator",
                            spannableName = SpannableString("")
                        )
                    )
            }
        }

        if (!files.isNullOrEmpty()) {
            result.add(ViewSearchInventoryItem(
                    id = "FILES",
                    viewInventoryType = ViewInventoryType.TITLE,
                    name = getApplication<Application>().getString(R.string.inventory_item_title),
                    textId = "Files",
                    filter = filter,
                    spannableName = SpannableString(filter)
                )
            )

            files.forEachIndexed { index, item ->
                val url: String? = item.imageUrl?.let { if (it.isNotEmpty() && it[0].isNotEmpty()) it[0] else null }

                result.add(ViewSearchInventoryItem(
                        id = item.id,
                        viewInventoryType = ViewInventoryType.FILE,
                        name = if (item.name.isNullOrEmpty())
                            getApplication<Application>().getString(R.string.inventory_file_without_name)
                        else item.name,
                        textId = "ID: " + item.id.replace("_item", ""),
                        quantityString = getApplication<Application>().getString(R.string.inventory_quantity_format, item.quantity),
                        priceString = getApplication<Application>().getString(R.string.inventory_price_format, item.price),
                        spannableName = createColoredString(item.name ?: "", filter),
                        firstImageUrl = url,
                        filter = filter
                    )
                )

                if (index != files.size - 1)
                    result.add(ViewSearchInventoryItem(
                            id = item.id + " _separator",
                            viewInventoryType = ViewInventoryType.DELIMITER,
                            textId = item.id + " _separator",
                            spannableName = SpannableString("")
                        )
                    )
            }
        }

        return result
    }

    private fun createColoredString(text: String, filter: String): Spannable {
        if (filter.isEmpty())
            return SpannableString(text)

        if (text.isEmpty())
            return SpannableString("")

        val result = SpannableString(text)
        val availablePrevious: MutableList<Char> = ArrayList()

        val lowerText = text.toLowerCase(Locale.ROOT)

        val count = filter.length
        val lowerFilter = filter.toLowerCase(Locale.ROOT)

        var from = lowerText.indexOf(lowerFilter, 0)
        while (from != -1) {
            if (from != 0 && !availablePrevious.contains(lowerText[from - 1])) {
                from = lowerText.indexOf(lowerFilter, from + count)
                continue
            }
            result.setSpan(ForegroundColorSpan(ContextCompat.getColor(getApplication(), R.color.colorAccent)),
                    from, from + count, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            from = lowerText.indexOf(lowerFilter, from + count)
        }
        return result
    }

    fun getFilterString() = filter

    override fun onCleared() {
        super.onCleared()
        sourceLiveData?.removeObserver(observer)
    }
}