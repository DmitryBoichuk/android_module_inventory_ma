package com.ibuildapp.inventory.viewmodel

import android.app.Application
import android.arch.lifecycle.*
import com.ibuildapp.inventory.R
import com.ibuildapp.inventory.backend.InventoryIBAService
import com.ibuildapp.inventory.database.InventoryDao
import com.ibuildapp.inventory.database.InventoryDatabase
import com.ibuildapp.inventory.extensions.formatDecimal
import com.ibuildapp.inventory.model.User
import com.ibuildapp.inventory.model.ViewProfileData
import com.ibuildapp.inventory.stuff.BaseSessionManager

class ProfileViewModel(application: Application): AndroidViewModel(application) {
    private val inventoryDao: InventoryDao = InventoryDatabase.getInstance(application, BaseSessionManager.INSTANCE.getUserId(application)!!).getDao()

    private val userId = BaseSessionManager.INSTANCE.getUserId(getApplication())!!
    private val viewLiveData = MutableLiveData<ViewProfileData>()

    private var dbUser: User? = null
    private var dbCount: Int? = null

    private val userSource = inventoryDao.getUserById(userId)
    private val countSource = inventoryDao.getItemsCount()

    private val userSourceObserver = Observer<User> {
        dbUser = it

        val user = dbUser ?: return@Observer
        val count = dbCount ?: return@Observer

        transform(user, count)
    }

    private val countSourceObserver = Observer<Int> {
        dbCount = it

        val user = dbUser ?: return@Observer
        val count = dbCount ?: return@Observer

        transform(user, count)
    }

    init {
        userSource.observeForever(userSourceObserver)
        countSource.observeForever(countSourceObserver)
    }

    private fun transform(dbUser: User, dbCount: Int) {
        val planCount = dbUser.planLimit
        val percentFloat = (dbCount.toFloat()/planCount) * 100
        val percent = "${percentFloat.formatDecimal(1)}%"
        
        val viewProfileData = ViewProfileData(
                email = dbUser.email,
                fullName = dbUser.firstName + " " + dbUser.lastName,
                planName = dbUser.planName,
                
                percent = percent,
                remainItems = if (dbCount >= planCount)
                        0.toString()
                    else (planCount - dbCount).toString(),
                
                itemsCount = getApplication<Application>().getString(R.string.inventory_space_items, dbCount, planCount),
                percentInt = percentFloat.toInt(),
                buttonName = dbUser.buttonName,
                buttonLink = dbUser.buttonLink
        )

        viewLiveData.postValue(viewProfileData)
    }

    fun loadUserInfo() {
         val token = BaseSessionManager.INSTANCE.getSession(getApplication())

        try {
            val response = InventoryIBAService.Factory.create().getUserInfo("Bearer $token").execute()
            val user = response.body()?.data
            val error = response.body()?.error

            if (response.isSuccessful && error?.type == 200 && user != null) {
                val localUser = user.toLocalUser()
                inventoryDao.insertUser(localUser)
            }
        } catch (error: Throwable) {
            error.printStackTrace()
        }
     }

    fun getViewData(): LiveData<ViewProfileData> = viewLiveData

    override fun onCleared() {
        userSource.removeObserver(userSourceObserver)
        countSource.removeObserver(countSourceObserver)
    }

    class ViewModelFactory(
            val application: Application
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return ProfileViewModel(application) as T
        }
    }
}