package com.ibuildapp.inventory.viewmodel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.ibuildapp.inventory.backend.InventoryIBAService
import com.ibuildapp.inventory.backend.model.LoginDataRequest
import com.ibuildapp.inventory.backend.model.LoginResult
import com.ibuildapp.inventory.database.InventoryDao
import com.ibuildapp.inventory.database.InventoryDatabase
import com.ibuildapp.inventory.model.Result
import com.ibuildapp.inventory.model.Result.ERROR
import com.ibuildapp.inventory.model.Result.LOADING
import com.ibuildapp.inventory.model.UserResponse
import com.ibuildapp.inventory.stuff.BaseSessionManager
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class AuthorizationViewModel
constructor(application: Application) : AndroidViewModel(application) {

    private val loginResultLiveData = MutableLiveData<LoginResult>()

    fun getLogin(): CharSequence? = BaseSessionManager.INSTANCE.getUserEmail(getApplication())

    fun getPassword(): CharSequence? = BaseSessionManager.INSTANCE.getUserPassword(getApplication())

    fun isChecked(): Boolean = BaseSessionManager.INSTANCE.getRememberMe(getApplication())

    suspend fun login(login: String, password: String, checked: Boolean) {
        loginResultLiveData.postValue(LoginResult(LOADING))
        val userResponse: UserResponse = withContext(Dispatchers.IO) { loadUser(login, password) }
        val loginResult = when {
            userResponse.user != null -> {
                BaseSessionManager.INSTANCE.setCurrentUser(getApplication(), userResponse.user.id, login, password)
                BaseSessionManager.INSTANCE.setSession(getApplication(), userResponse.session)
                BaseSessionManager.INSTANCE.setCurrentInventory(getApplication(), userResponse.user.inventories.first())
                BaseSessionManager.INSTANCE.setRememberMe(getApplication(), checked)
                LoginResult(result = Result.SUCCESS)
            }
            !userResponse.error.isNullOrEmpty() -> LoginResult(result = ERROR, error = userResponse.error)
            else -> LoginResult(result = ERROR, error = "User is null")
        }

        loginResultLiveData.postValue(loginResult)
    }


    private suspend fun loadUser(login: String, password: String): UserResponse = withContext(Dispatchers.IO) {
        val request = LoginDataRequest(email = login, password = password)

        try {
            val response = InventoryIBAService.Factory.create().login(request).execute()

            if (response.isSuccessful && response.body() != null) {
                val body = response.body()

                val error = body?.error

                when (val user = body?.data) {
                    null -> return@withContext UserResponse(error = "Unknown error.")
                    else -> {
                        if (error?.type != 200)
                            return@withContext UserResponse(error = error?.message
                                    ?: "Unknown error.")

                        val localUser = user.toLocalUser()

                        val inventoryDao: InventoryDao = InventoryDatabase.getInstance(this@AuthorizationViewModel.getApplication(), localUser.id).getDao()
                        inventoryDao.insertUser(localUser)

                        return@withContext UserResponse(user = user, session = user.token)
                    }
                }
            } else
                return@withContext UserResponse(error = response.body()?.error?.message ?: "Unknown error")
        } catch (error: Throwable) {
            return@withContext UserResponse(error = "Need internet")
        }
    }

    fun getData(): LiveData<LoginResult> = loginResultLiveData
}