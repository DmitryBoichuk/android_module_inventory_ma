package com.ibuildapp.inventory.viewmodel

import android.app.Application
import android.arch.lifecycle.*
import com.google.zxing.BarcodeFormat
import com.ibuildapp.inventory.backend.InventoryIBAService
import com.ibuildapp.inventory.backend.model.LookupDataRequest
import com.ibuildapp.inventory.backend.model.LookupResult
import com.ibuildapp.inventory.backend.model.LookupStatus
import com.ibuildapp.inventory.database.InventoryDao
import com.ibuildapp.inventory.database.InventoryDatabase
import com.ibuildapp.inventory.model.LoadQRData
import com.ibuildapp.inventory.stuff.BaseSessionManager
import com.ibuildapp.inventory.stuff.ItemUtils
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class LoadQRDataViewModel(application: Application, val parentItemId: String?,
                          private val scannedText: String,
                          private val scannedFormat: BarcodeFormat)
    : AndroidViewModel(application) {

    enum class State {
        NONE, NEW_LABEL, LABEL_FOUND
    }

    private val inventoryDao: InventoryDao = InventoryDatabase.getInstance(application, BaseSessionManager.INSTANCE.getUserId(application)!!).getDao()

    private val state: MutableLiveData<State> = MutableLiveData<State>().apply { postValue(State.NONE) }
    private val data: MutableLiveData<LoadQRData?> = MutableLiveData()

    init {
        runBlocking {
            GlobalScope.launch (Dispatchers.IO) {
                val dbItem = inventoryDao.findInventoryItemByCode(scannedText)

                val result = LoadQRData(scannedText, scannedFormat)

                if (dbItem != null){
                    result.inventoryItem =
                            if (dbItem.isFolder)
                                ItemUtils.transformInventoryFolder(dbItem, getApplication())
                            else ItemUtils.transformInventoryItem(dbItem, getApplication())

                    state.postValue(State.LABEL_FOUND)
                } else state.postValue(State.NEW_LABEL)

                data.postValue(result)
            }
        }
    }

    fun getState(): LiveData<State> = state

    fun getData(): LiveData<LoadQRData?> = data

    fun loadDataForQR(): LookupResult {
        val body = LookupDataRequest (barcode = scannedText, action = "get_product")
        val token = BaseSessionManager.INSTANCE.getSession(getApplication())

        try {
            val response = InventoryIBAService.Factory.create().search("Bearer $token", body).execute()
            response?.let {
                if (response.isSuccessful) {
                    response.body()?.data?.products?.let {
                        return if (it.isEmpty())
                            LookupResult(LookupStatus.NOTHING_FOUND)
                        else LookupResult(LookupStatus.SUCCESS, it[0])
                    }

                    return LookupResult(LookupStatus.NOTHING_FOUND)
                } else {
                    return LookupResult(status = LookupStatus.ERROR, error = response.errorBody().toString())
                }
            } ?: return LookupResult(LookupStatus.NOTHING_FOUND)

        }catch (e: Exception){
            return LookupResult(status = LookupStatus.ERROR, error = e.localizedMessage)
        }
    }

    class ViewModelFactory(
            val application: Application,
            private val parentItemId: String?,
            private val scannedText: String,
            private val scannedFormat: BarcodeFormat
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return LoadQRDataViewModel(application, parentItemId, scannedText, scannedFormat) as T
        }
    }
}