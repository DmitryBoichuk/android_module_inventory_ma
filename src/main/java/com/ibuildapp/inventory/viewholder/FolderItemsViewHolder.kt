package com.ibuildapp.inventory.viewholder

import android.support.v7.widget.PopupMenu
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.ibuildapp.inventory.R
import com.ibuildapp.inventory.callback.ItemsCallback
import com.ibuildapp.inventory.databinding.InventoryItemsItemFolderBinding
import com.ibuildapp.inventory.model.ViewInventoryItem

class FolderItemsViewHolder private constructor(
        val binding: InventoryItemsItemFolderBinding,
        val callback: ItemsCallback
): BaseItemsViewHolder(binding.root), PopupMenu.OnMenuItemClickListener {

    lateinit var item: ViewInventoryItem

    override fun bind(item: ViewInventoryItem) {
        this.item = item
        innerBind()
    }

    override fun rebindOnChanges(payload: ViewInventoryItem) {
        this.item = payload
        innerBind()
    }

    private fun innerBind() {
        binding.item = item

        binding.tvName.text = item.name

        if (item.firstImageUrl.isNullOrEmpty())
            binding.ivMainPhoto.visibility = View.GONE
        else {
            binding.ivMainPhoto.visibility = View.VISIBLE

            Glide.with(binding.root.context).load(item.firstImageUrl).transform(CircleCrop()).into(binding.ivMainPhoto)
        }

        binding.ivMore.setOnClickListener { showPopup(it) }
        binding.clRoot.setOnClickListener { callback.onFolderClick(item.id, item.name!!) }
    }

    private fun showPopup(view: View) {
        val popup = PopupMenu(binding.root.context, view)
        popup.setOnMenuItemClickListener(this)
        popup.inflate(R.menu.inventory_folder_popup)
        popup.show()
    }

    companion object {
        fun create(parent: ViewGroup, callback: ItemsCallback): FolderItemsViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            val binding: InventoryItemsItemFolderBinding = InventoryItemsItemFolderBinding.inflate(inflater, parent, false)
            return FolderItemsViewHolder(binding, callback)
        }
    }

    override fun onMenuItemClick(menuItem: MenuItem?): Boolean {
        if (menuItem?.itemId == R.id.action_edit)
            callback.onEditFolderClick(item.id)

        if (menuItem?.itemId == R.id.action_delete)
            callback.onDeleteFolderClick(item.id, true)

        return true
    }
}