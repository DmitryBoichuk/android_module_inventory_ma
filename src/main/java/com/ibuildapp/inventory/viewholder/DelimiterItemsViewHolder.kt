package com.ibuildapp.inventory.viewholder

import android.view.LayoutInflater
import android.view.ViewGroup
import com.ibuildapp.inventory.databinding.InventoryItemsItemDelimiterBinding
import com.ibuildapp.inventory.model.ViewInventoryItem

class DelimiterItemsViewHolder private constructor(
        val binding: InventoryItemsItemDelimiterBinding
): BaseItemsViewHolder(binding.root) {

    override fun bind(item: ViewInventoryItem) {
    }

    override fun rebindOnChanges(payload: ViewInventoryItem) {
    }

    companion object {
        fun create(parent: ViewGroup): DelimiterItemsViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            val binding: InventoryItemsItemDelimiterBinding = InventoryItemsItemDelimiterBinding.inflate(inflater)
            return DelimiterItemsViewHolder(binding)
        }
    }
}