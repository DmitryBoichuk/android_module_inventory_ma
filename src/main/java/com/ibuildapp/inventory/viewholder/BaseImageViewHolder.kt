package com.ibuildapp.inventory.viewholder

import android.support.v7.widget.RecyclerView
import android.view.View
import com.ibuildapp.inventory.model.ViewImageItem

abstract class BaseImageViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    abstract fun bind(item: ViewImageItem)
    abstract fun rebindOnChanges(payload: ViewImageItem)
    open fun onViewRecycled() {}
}