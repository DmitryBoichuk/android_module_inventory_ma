package com.ibuildapp.inventory.viewholder

import android.graphics.drawable.Drawable
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.ibuildapp.inventory.databinding.InventoryImageItemBinding
import com.ibuildapp.inventory.model.ViewImageItem

class ItemImageViewHolder private constructor(
        val binding: InventoryImageItemBinding
): BaseImageViewHolder(binding.root) {

    lateinit var item: ViewImageItem

    override fun bind(item: ViewImageItem) {
        this.item = item
        innerBind()
    }

    override fun rebindOnChanges(payload: ViewImageItem) {
        this.item = payload
        innerBind()
    }

    private fun innerBind() {
        if (!item.url.isNullOrEmpty())
            Glide.with(binding.ivMainPhoto).load(item.url).centerCrop().into(binding.ivMainPhoto)

        if (item.uri != null)
            Glide.with(binding.ivMainPhoto).load(item.uri) .listener(object: RequestListener<Drawable> {
                override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                    return false
                }

                override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                    println(e)
                    return false
                }

            }).centerCrop().into(binding.ivMainPhoto)
    }

    companion object {
        fun create(parent: ViewGroup): ItemImageViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            val binding: InventoryImageItemBinding = InventoryImageItemBinding.inflate(inflater, parent, false)
            return ItemImageViewHolder(binding)
        }
    }
}