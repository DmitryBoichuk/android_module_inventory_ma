package com.ibuildapp.inventory.viewholder

import android.support.v7.widget.RecyclerView
import android.view.View
import com.ibuildapp.inventory.model.ViewInventoryItem

abstract class BaseItemsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    abstract fun bind(item: ViewInventoryItem)
    abstract fun rebindOnChanges(payload: ViewInventoryItem)
    open fun onViewRecycled() {}
}