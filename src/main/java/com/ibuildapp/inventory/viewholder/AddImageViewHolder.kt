package com.ibuildapp.inventory.viewholder

import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.ibuildapp.inventory.callback.ImagesCallback
import com.ibuildapp.inventory.databinding.InventoryImageAddBinding
import com.ibuildapp.inventory.model.ViewImageItem

class AddImageViewHolder private constructor(
        val binding: InventoryImageAddBinding,
        val callback: ImagesCallback
): BaseImageViewHolder(binding.root) {

    lateinit var item: ViewImageItem

    override fun bind(item: ViewImageItem) {
        this.item = item
        innerBind()
    }

    override fun rebindOnChanges(payload: ViewImageItem) {
        this.item = payload
        innerBind()
    }

    private fun innerBind() {
        binding.vImageBackground.setOnClickListener { callback.onAddClick() }
    }

    companion object {
        fun create(parent: ViewGroup, callback: ImagesCallback): AddImageViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            val binding: InventoryImageAddBinding = InventoryImageAddBinding.inflate(inflater, parent, false)
            return AddImageViewHolder(binding, callback)
        }
    }
}