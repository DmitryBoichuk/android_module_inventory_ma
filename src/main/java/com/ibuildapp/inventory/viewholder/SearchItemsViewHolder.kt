package com.ibuildapp.inventory.viewholder

import android.view.LayoutInflater
import android.view.ViewGroup
import com.ibuildapp.inventory.callback.ItemsCallback
import com.ibuildapp.inventory.databinding.InventoryItemsItemSearchBinding
import com.ibuildapp.inventory.model.ViewInventoryItem

class SearchItemsViewHolder private constructor(
        val binding: InventoryItemsItemSearchBinding,
        val callback: ItemsCallback
): BaseItemsViewHolder(binding.root) {

    override fun bind(item: ViewInventoryItem) {
        binding.vSearch.setOnClickListener { callback.onSearchClick() }
    }

    override fun rebindOnChanges(payload: ViewInventoryItem) {
    }

    companion object {
        fun create(parent: ViewGroup, callback: ItemsCallback): SearchItemsViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            val binding: InventoryItemsItemSearchBinding = InventoryItemsItemSearchBinding.inflate(inflater, parent, false)
            return SearchItemsViewHolder(binding, callback)
        }
    }
}