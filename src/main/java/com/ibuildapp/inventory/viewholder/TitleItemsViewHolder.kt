package com.ibuildapp.inventory.viewholder

import android.view.LayoutInflater
import android.view.ViewGroup
import com.ibuildapp.inventory.databinding.InventoryItemsItemTitleBinding
import com.ibuildapp.inventory.model.ViewInventoryItem

class TitleItemsViewHolder private constructor(
        val binding: InventoryItemsItemTitleBinding
): BaseItemsViewHolder(binding.root) {

    lateinit var item: ViewInventoryItem

    override fun bind(item: ViewInventoryItem) {
        this.item = item
        innerBind()
    }

    override fun rebindOnChanges(payload: ViewInventoryItem) {
        this.item = payload
        innerBind()
    }

    private fun innerBind() {
        binding.item = item
    }

    companion object {
        fun create(parent: ViewGroup): TitleItemsViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            val binding: InventoryItemsItemTitleBinding = InventoryItemsItemTitleBinding.inflate(inflater)
            return TitleItemsViewHolder(binding)
        }
    }
}