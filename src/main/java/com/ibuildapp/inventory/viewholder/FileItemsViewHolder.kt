package com.ibuildapp.inventory.viewholder

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.ibuildapp.inventory.callback.ItemsCallback
import com.ibuildapp.inventory.databinding.InventoryItemsItemFileBinding
import com.ibuildapp.inventory.model.ViewInventoryItem

class FileItemsViewHolder private constructor(
        val binding: InventoryItemsItemFileBinding,
        val callback: ItemsCallback
): BaseItemsViewHolder(binding.root) {

    lateinit var item: ViewInventoryItem

    override fun bind(item: ViewInventoryItem) {
        this.item = item
        innerBind()
    }

    override fun rebindOnChanges(payload: ViewInventoryItem) {
        this.item = payload
        innerBind()
    }

    private fun innerBind() {
        binding.item = item

        binding.tvName.text = item.name

        if (item.firstImageUrl.isNullOrEmpty())
            binding.ivMainPhoto.visibility = View.GONE
        else {
            binding.ivMainPhoto.visibility = View.VISIBLE

            Glide.with(binding.root.context).load(item.firstImageUrl).transform(CircleCrop()).into(binding.ivMainPhoto)
        }

        if (item.priceString == null)
            binding.tvPrice.visibility = View.GONE
        else
            binding.tvPrice.visibility = View.VISIBLE


        binding.ivDelete.setOnClickListener { callback.deleteItemClick(item.id) }
        binding.clRoot.setOnClickListener { callback.onItemClick(item.id, item.name!!) }
    }

    companion object {
        fun create(parent: ViewGroup, callback: ItemsCallback): FileItemsViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            val binding: InventoryItemsItemFileBinding = InventoryItemsItemFileBinding.inflate(inflater, parent, false)
            return FileItemsViewHolder(binding, callback)
        }
    }
}