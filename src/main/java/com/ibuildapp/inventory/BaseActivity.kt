package com.ibuildapp.inventory

import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar

abstract class BaseActivity: AppCompatActivity() {
    abstract fun getToolbar(): Toolbar
}