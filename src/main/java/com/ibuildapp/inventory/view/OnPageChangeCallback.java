package com.ibuildapp.inventory.view;

import android.support.annotation.Px;

public abstract class OnPageChangeCallback {
/**
 * This method will be invoked when the current page is scrolled, either as part
 * of a programmatically initiated smooth scroll or a user initiated touch scroll.
 *
 * @param position Position index of the first page currently being displayed.
 *                 Page position+1 will be visible if positionOffset is nonzero.
 * @param positionOffset Value from [0, 1) indicating the offset from the page at position.
 * @param positionOffsetPixels Value in pixels indicating the offset from position.
 */
public void onPageScrolled(int position, float positionOffset,
        @Px int positionOffsetPixels) {
}

/**
 * This method will be invoked when a new page becomes selected. Animation is not
 * necessarily complete.
 *
 * @param position Position index of the new selected page.
 */
public void onPageSelected(int position) {
}

public void onPageScrollStateChanged(int state) {
}
}