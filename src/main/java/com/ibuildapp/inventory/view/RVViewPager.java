package com.ibuildapp.inventory.view;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSmoothScroller;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.widget.FrameLayout;

import com.ibuildapp.inventory.databinding.InventoryViewPagerBinding;


public class RVViewPager extends FrameLayout {
    private static final float MILLISECONDS_PER_INCH = 50f;
    private static final float FAST_MILLISECONDS_PER_INCH = 10f;

    private LinearLayoutManager mLayoutManager;

    public interface OnPageSelectedListener {
        void onPageSelected(int position);
    }

    private InventoryViewPagerBinding mBinding;
    private RVSnapHelper mHelper;
    private CompositeOnPageChangeCallback mPageChangeEventDispatcher;
    private ScrollEventAdapter mScrollEventListener;
    private PageTransformerAdapter mTransformerAdapter;

    private OnPageSelectedListener mListener;

    public RVViewPager(Context context) {
        super(context);

        createView();
    }

    public RVViewPager(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        createView();
    }

    public RVViewPager(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        createView();
    }

    public void createView(){
        mBinding = InventoryViewPagerBinding.inflate(LayoutInflater.from(getContext()), this, true);

        mLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        mBinding.rvContainer.setLayoutManager(mLayoutManager);

        mHelper = new RVPagerSnapHelper();
        mHelper.attachToRecyclerView(mBinding.rvContainer);

        mPageChangeEventDispatcher =
                new CompositeOnPageChangeCallback(3);

        mScrollEventListener = new ScrollEventAdapter(mLayoutManager);
        mTransformerAdapter = new PageTransformerAdapter(mLayoutManager){
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);

                if (!mScrollEventListener.isFakeDragging())
                    if (mListener != null)
                        mListener.onPageSelected(position);
            }
        };

        mScrollEventListener.setOnPageChangeCallback(mTransformerAdapter);

        mBinding.rvContainer.addOnScrollListener(mScrollEventListener);
    }

    public void setAdapter(RecyclerView.Adapter adapter) {
        mBinding.rvContainer.setAdapter(adapter);
    }

//    public void scrollToPosition(int scrollPosition) {
//        if (mBinding.rvContainer.getLayoutManager() != null
//                && mBinding.rvContainer.getLayoutManager() instanceof LinearLayoutManager)
//            ((LinearLayoutManager) mBinding.rvDeadStopContainer.getLayoutManager()).scrollToPositionWithOffset(scrollPosition, 0);
//    }
//
    public void fakeScroll() {
        mScrollEventListener.notifyBeginFakeDrag();
        mScrollEventListener.onScrolled(mBinding.rvContainer, 0, 0);
        mScrollEventListener.notifyEndFakeDrag();
    }

    public void setListener(OnPageSelectedListener listener) {
        this.mListener = listener;
    }

    public OnPageSelectedListener getListener() {
        return mListener;
    }

    public int getScrollState() {
        return mBinding.rvContainer.getScrollState();
    }

    public void animateToPosition(int scrollPosition) {
        RecyclerView.SmoothScroller smoothScroller = new LinearSmoothScroller(getContext()) {
            @Override protected int getVerticalSnapPreference() {
                return LinearSmoothScroller.SNAP_TO_START;
            }

            @Override
            protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                return MILLISECONDS_PER_INCH / displayMetrics.densityDpi;
            }
        };

        if (mBinding.rvContainer.getLayoutManager() != null
                && mBinding.rvContainer.getLayoutManager() instanceof LinearLayoutManager)
            smoothScroller.setTargetPosition(scrollPosition);
        mBinding.rvContainer.getLayoutManager().startSmoothScroll(smoothScroller);
    }

    public void fastToPosition(int scrollPosition) {
        RecyclerView.SmoothScroller smoothScroller = new LinearSmoothScroller(getContext()) {
            @Override protected int getVerticalSnapPreference() {
                return LinearSmoothScroller.SNAP_TO_START;
            }

            @Override
            protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                return FAST_MILLISECONDS_PER_INCH / displayMetrics.densityDpi;
            }
        };

        if (mBinding.rvContainer.getLayoutManager() != null
                && mBinding.rvContainer.getLayoutManager() instanceof LinearLayoutManager)
            smoothScroller.setTargetPosition(scrollPosition);
        mBinding.rvContainer.getLayoutManager().startSmoothScroll(smoothScroller);
    }

//    public int getCurrentItem() {
//        return mManager.findFirstVisibleItemPosition();
//    }
}
