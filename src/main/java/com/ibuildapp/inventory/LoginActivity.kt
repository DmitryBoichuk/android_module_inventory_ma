package com.ibuildapp.inventory

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.ibuildapp.inventory.databinding.InventoryLoginBinding

class LoginActivity : AppCompatActivity() {
    private lateinit var binding: InventoryLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = InventoryLoginBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
    }
}