package com.ibuildapp.inventory.stuff

import android.app.Application
import android.view.View
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.ibuildapp.inventory.R
import com.ibuildapp.inventory.databinding.InventoryItemsItemFileBinding
import com.ibuildapp.inventory.databinding.InventoryItemsItemFolderBinding
import com.ibuildapp.inventory.model.InventoryItemWithCounts
import com.ibuildapp.inventory.model.ViewInventoryItem
import com.ibuildapp.inventory.model.ViewInventoryType

class ItemUtils {
    companion object {
        fun transformInventoryFolder(item: InventoryItemWithCounts, application: Application): ViewInventoryItem {
            val url: String? = item.imageUrl?.let { if (it.isNotEmpty() && it[0].isNotEmpty()) it[0] else null }

            return ViewInventoryItem(
                    id = item.id,
                    viewInventoryType = ViewInventoryType.FOLDER,
                    name = if (item.name.isNullOrEmpty())
                        application.getString(R.string.inventory_without_name)
                    else item.name,
                    foldersCount = item.folderCount ?: 0,
                    filesCount = item.filesCount ?: 0,
                    textId = "ID: " + item.id.removeSuffix("_folder").removeSuffix("_item"),
                    firstImageUrl = url
            )
        }

        fun transformInventoryItem(item: InventoryItemWithCounts, application: Application): ViewInventoryItem {
            val url: String? = item.imageUrl?.let { if (it.isNotEmpty() && it[0].isNotEmpty()) it[0] else null }

            return ViewInventoryItem(
                    id = item.id,
                    viewInventoryType = ViewInventoryType.FILE,
                    name = if (item.name.isNullOrEmpty())
                        application.getString(R.string.inventory_file_without_name)
                    else item.name,
                    textId = "ID: " + item.id.removeSuffix("_folder").removeSuffix("_item"),
                    quantityString = application.getString(R.string.inventory_quantity_format, item.quantity),
                    priceString = if (item.price !=  null) application.getString(R.string.inventory_price_format, item.price) else null,
                    firstImageUrl = url
            )
        }

        fun bindFile(binding: InventoryItemsItemFileBinding, item: ViewInventoryItem) {
            binding.item = item

            binding.tvName.text = item.name

            if (item.firstImageUrl.isNullOrEmpty())
                binding.ivMainPhoto.visibility = View.GONE
            else {
                binding.ivMainPhoto.visibility = View.VISIBLE

                Glide.with(binding.root.context).load(item.firstImageUrl).transform(CircleCrop()).into(binding.ivMainPhoto)
            }

            binding.ivDelete.visibility = View.GONE
        }

        fun bindFolder(binding: InventoryItemsItemFolderBinding, item: ViewInventoryItem) {
            binding.item = item

            binding.tvName.text = item.name

            if (item.firstImageUrl.isNullOrEmpty())
                binding.ivMainPhoto.visibility = View.GONE
            else {
                binding.ivMainPhoto.visibility = View.VISIBLE

                Glide.with(binding.root.context).load(item.firstImageUrl).transform(CircleCrop()).into(binding.ivMainPhoto)
            }

        }
    }
}