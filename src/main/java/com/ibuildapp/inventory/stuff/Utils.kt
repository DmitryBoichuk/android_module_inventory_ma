package com.ibuildapp.inventory.stuff

import android.content.Context
import android.content.res.Resources
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build.VERSION
import android.util.TypedValue
import com.bumptech.glide.Glide
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream

class Utils {
    companion object {
        fun getStatusBarHeight(context: Context): Int {
            var result = 0
            val resourceId: Int = context.resources.getIdentifier("status_bar_height", "dimen", "android")
            if (resourceId > 0) {
                result = context.resources.getDimensionPixelSize(resourceId)
            }
            return result
        }

        fun dpToPixels(context: Context?, dp: Int): Int {
            return if (context != null) dpToPixels(context.resources, dp.toFloat()).toInt() else 0
        }

        private fun dpToPixels(resources: Resources?, dp: Float): Float {
            return if (resources != null) TypedValue.applyDimension(
                    TypedValue.COMPLEX_UNIT_DIP, dp, resources.displayMetrics) else 0f
        }

        fun getMissingPermissions(): List<String> {
            val manifestPermissions = mutableListOf<String>()
            return if (VERSION.SDK_INT < 23) {
                manifestPermissions
            } else {
                manifestPermissions.add("android.permission.CAMERA")
                manifestPermissions
            }
        }

        fun getFileFromUri(context: Context, uri: Uri): File {
            val res: Bitmap = Glide.with(context).asBitmap().load(uri).submit().get()
            return convertBitmapToFile(context, fileName = "file-${System.currentTimeMillis()}.jpg", bitmap = res)
        }

        private fun convertBitmapToFile(context: Context, fileName: String, bitmap: Bitmap): File {
            val file = File(context.cacheDir, fileName)
            file.createNewFile()

            val bos = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, bos)
            val bitMapData = bos.toByteArray()

            FileOutputStream(file).use { inputStream ->
                inputStream.write(bitMapData)
            }

            return file
        }
    }
}