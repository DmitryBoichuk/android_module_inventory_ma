package com.ibuildapp.inventory.stuff

import java.util.regex.Pattern

class LoginUtils {
    companion object {
        private const val MIN_LOGIN_LENGTH = 3
        private const val MIN_PASSWORD_LENGTH = 8
        private const val MAX_PASSWORD_LENGTH = 16

        fun isLoginValid(login: String): Boolean {
            val trimLogin = login.trim { it <= ' ' }
            return (trimLogin.isNotEmpty() && trimLogin.length >= MIN_LOGIN_LENGTH)
        }

        fun isEmailValid(email: String): Boolean {
            val trimEmail = email.trim();
            val ePattern = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
            val p = Pattern.compile(ePattern)
            val m = p.matcher(trimEmail)
            return m.matches()
        }

        fun isPasswordValid(password: String): Boolean {
            val trimPassword = password.trim { it <= ' ' }
            return (trimPassword.isNotEmpty() &&
                    trimPassword.length >= MIN_PASSWORD_LENGTH &&
                    trimPassword.length <= MAX_PASSWORD_LENGTH)
        }
    }
}