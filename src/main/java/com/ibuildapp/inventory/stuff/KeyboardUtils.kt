package com.ibuildapp.inventory.stuff

import android.app.Activity
import android.content.Context
import android.view.inputmethod.InputMethodManager
import android.widget.EditText

class KeyboardUtils {
    fun hideSoftInput(activity: Activity?): Boolean {
        if (activity != null && activity.window != null) {
            val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            return imm.hideSoftInputFromWindow(activity.window.decorView.windowToken, 0)
        }
        return false
    }

    fun showSoftInput(inputView: EditText) {
        if (inputView.requestFocus()) {
            val imm = inputView.context
                    .getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(inputView, InputMethodManager.SHOW_IMPLICIT)
        }
    }

    fun hideKeyboard(act: Activity?) {
        if (act != null && act.currentFocus != null) {
            val inputMethodManager = act.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(act.currentFocus!!.windowToken, 0)
        }
    }
}