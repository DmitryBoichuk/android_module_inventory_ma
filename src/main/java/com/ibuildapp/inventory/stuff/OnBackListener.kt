package com.ibuildapp.inventory.stuff

interface OnBackListener {
    fun onBackPressed(): Boolean
}