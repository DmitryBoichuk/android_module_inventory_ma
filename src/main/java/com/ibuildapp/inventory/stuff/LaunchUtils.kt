package com.ibuildapp.inventory.stuff

class LaunchUtils {
    companion object {
        const val isAdd = "is_add"
        const val isFolder = "is_folder"
        const val parentId = "parent_id"
        const val title = "title"
        const val itemId = "item_id"
        const val scannedText = "scanned_text"
        const val scannedFormat = "scanned_format"
        const val data = "data"

        const val pickerRequest = 1000
        const val cameraRequest = 1001
    }
}