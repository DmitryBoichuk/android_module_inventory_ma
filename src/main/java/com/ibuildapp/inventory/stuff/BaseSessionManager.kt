package com.ibuildapp.inventory.stuff

import android.content.Context

enum class BaseSessionManager {
    INSTANCE;

    fun getUserEmail(context: Context): String? {
        val sp = context.getSharedPreferences(COMMON_SETTINGS, Context.MODE_PRIVATE)
        return sp.getString(USER_EMAIL, null)
    }

    fun getUserName(context: Context): String? {
        val sp = context.getSharedPreferences(COMMON_SETTINGS, Context.MODE_PRIVATE)
        return sp.getString(USER_NAME, null)
    }

    fun getUserPassword(context: Context): String? {
        val sp = context.getSharedPreferences(COMMON_SETTINGS, Context.MODE_PRIVATE)
        return sp.getString(USER_PASSWORD, null)
    }

    fun getSession(context: Context): String? {
        val sp = context.getSharedPreferences(COMMON_SETTINGS, Context.MODE_PRIVATE)
        return sp.getString(SESSION, null)
    }

    fun setSession(context: Context, session: String?) {
        val sp = context.getSharedPreferences(COMMON_SETTINGS, Context.MODE_PRIVATE)
        sp.edit().putString(SESSION, session).apply()
    }

    fun getCurrentInventory(context: Context): Int {
        val userId = getUserId(context)
        val sp = context.getSharedPreferences(USER_SETTINGS + "_" + userId, Context.MODE_PRIVATE)
        return sp.getInt(INVENTORY, -1)
    }

    fun setCurrentInventory(context: Context, inventory: Int?) {
        val userId = getUserId(context)
        val sp = context.getSharedPreferences(USER_SETTINGS + "_" + userId, Context.MODE_PRIVATE)
        sp.edit().putInt(INVENTORY, inventory ?: -1).apply()

    }

    fun setLastUpdate(context: Context, timestamp: Long?) {
        val userId = getUserId(context)
        val sp = context.getSharedPreferences(USER_SETTINGS + "_" + userId, Context.MODE_PRIVATE)
        sp.edit().putLong(LAST_UPDATE, timestamp ?: 0L).apply()
    }

    fun getLastUpdate(context: Context): Long? {
        val userId = getUserId(context)

        val sp = context.getSharedPreferences(USER_SETTINGS + "_" + userId, Context.MODE_PRIVATE)
        return sp.getLong(LAST_UPDATE, 0L)
    }

    fun getUserId(context: Context): String? {
        val sp = context.getSharedPreferences(COMMON_SETTINGS, Context.MODE_PRIVATE)
        return sp.getString(USER_ID, null)
    }

    fun setUserId(context: Context, userId: String?) {
        val sp = context.getSharedPreferences(COMMON_SETTINGS, Context.MODE_PRIVATE)
        sp.edit()
                .putString(USER_ID, userId)
                .apply()
    }

    fun setCurrentUser(context: Context, userId: String?, userEmail: String?, userPassword: String?) {
        val sp = context.getSharedPreferences(COMMON_SETTINGS, Context.MODE_PRIVATE)
        sp.edit()
                .putString(USER_EMAIL, userEmail)
                .putString(USER_ID, userId)
                .putString(USER_PASSWORD, userPassword)
                .apply()
    }

    fun getRememberMe(context: Context): Boolean {
        return context.getSharedPreferences(COMMON_SETTINGS, Context.MODE_PRIVATE).getBoolean(REMEMBER_ME, false)
    }

    fun setRememberMe(context: Context, state: Boolean?) {
        context.getSharedPreferences(COMMON_SETTINGS, Context.MODE_PRIVATE)
                .edit().putBoolean(REMEMBER_ME, state!!)
                .apply()
    }

    fun clearOnlyUser(context: Context) {
        val sp = context.getSharedPreferences(COMMON_SETTINGS, Context.MODE_PRIVATE)
        sp.edit().remove(USER_ID).remove(SESSION).apply()
    }

    fun clearAllUser(context: Context) {
        val sp = context.getSharedPreferences(COMMON_SETTINGS, Context.MODE_PRIVATE)
        sp.edit().remove(USER_ID).remove(SESSION).remove(USER_NAME).remove(USER_PASSWORD).apply()
    }

    companion object {
        private const val USER_ID = "USER_ID"
        private const val USER_EMAIL = "USER_EMAIL"
        private const val USER_NAME = "USER_NAME"
        private const val USER_PASSWORD = "USER_PASSWORD"
        private const val SESSION = "SESSION"
        private const val REMEMBER_ME = "REMEMBER_ME"
        private const val COMMON_SETTINGS = "COMMON_SETTINGS"
        private const val USER_SETTINGS = "USER_SETTINGS"
        private const val LAST_UPDATE = "LAST_UPDATE"
        private const val INVENTORY = "INVENTORY"
    }
}