package com.ibuildapp.inventory

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.KeyEvent
import android.view.View
import android.widget.TextView
import com.ibuildapp.inventory.databinding.InventoryQrBinding
import com.journeyapps.barcodescanner.CaptureManager

class QRCodeReaderActivity : AppCompatActivity() {
    private lateinit var capture: CaptureManager
    private lateinit var binding: InventoryQrBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = InventoryQrBinding.inflate(layoutInflater)
        val view = binding.root

        val status = binding.inventoryQrLayout.findViewById<TextView>(R.id.zxing_status_view)
        status?.let { it.visibility = View.GONE }

        setContentView(view)

        capture = CaptureManager(this, binding.inventoryQrLayout)
        capture.initializeFromIntent(intent, savedInstanceState)
        capture.decode()
    }

    override fun onResume() {
        super.onResume()
        capture.onResume()
    }

    override fun onPause() {
        super.onPause()
        capture.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        capture.onDestroy()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        capture.onSaveInstanceState(outState)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        return binding.inventoryQrLayout.onKeyDown(keyCode, event) || super.onKeyDown(keyCode, event)
    }
}