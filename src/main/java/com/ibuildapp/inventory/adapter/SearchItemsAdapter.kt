package com.ibuildapp.inventory.adapter

import android.support.v7.recyclerview.extensions.ListAdapter
import android.view.ViewGroup
import com.ibuildapp.inventory.callback.ItemsCallback
import com.ibuildapp.inventory.model.ViewInventoryItem
import com.ibuildapp.inventory.model.ViewInventoryType
import com.ibuildapp.inventory.model.ViewSearchInventoryItem
import com.ibuildapp.inventory.viewholder.*

class SearchItemsAdapter (
        private val callback: ItemsCallback
) : ListAdapter<ViewSearchInventoryItem, BaseItemsViewHolder>(ViewSearchInventoryItem.DiffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseItemsViewHolder {
        return when (ViewInventoryType.values()[viewType]) {
            ViewInventoryType.TITLE -> TitleItemsViewHolder.create(parent)
            ViewInventoryType.DELIMITER -> DelimiterItemsViewHolder.create(parent)
            ViewInventoryType.SEARCH -> SearchItemsViewHolder.create(parent, callback)
            ViewInventoryType.FOLDER -> SearchFolderItemsViewHolder.create(parent, callback)
            ViewInventoryType.FILE -> SearchFileItemsViewHolder.create(parent, callback)
        }
    }

    override fun onBindViewHolder(holder: BaseItemsViewHolder, position: Int, payloads: List<Any>) {
        if (payloads.isEmpty()) onBindViewHolder(holder, position)
        else {
            for (payload in payloads) {
                if (payload is ViewInventoryItem) holder.rebindOnChanges(payload)
            }
        }
    }

    override fun onBindViewHolder(holder: BaseItemsViewHolder, position: Int) {
        holder.bind(getItem(position)!!)
    }

    override fun getItemViewType(position: Int): Int {
        return getItem(position)!!.viewInventoryType.ordinal
    }

    override fun onViewRecycled(holder: BaseItemsViewHolder) {
        holder.onViewRecycled()
    }
}