package com.ibuildapp.inventory.adapter

import android.support.v7.recyclerview.extensions.ListAdapter
import android.view.ViewGroup
import com.ibuildapp.inventory.callback.ImagesCallback
import com.ibuildapp.inventory.model.ViewImageItem
import com.ibuildapp.inventory.model.ViewImageType
import com.ibuildapp.inventory.viewholder.AddImageViewHolder
import com.ibuildapp.inventory.viewholder.BaseImageViewHolder
import com.ibuildapp.inventory.viewholder.ItemImageViewHolder

class ImagesAdapter (
        private val callback: ImagesCallback
) : ListAdapter<ViewImageItem, BaseImageViewHolder>(ViewImageItem.DiffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseImageViewHolder {
        return when (ViewImageType.values()[viewType]) {
            ViewImageType.ITEM -> ItemImageViewHolder.create(parent)
            ViewImageType.ADD -> AddImageViewHolder.create(parent, callback)
        }
    }

    override fun onBindViewHolder(holder: BaseImageViewHolder, position: Int, payloads: List<Any>) {
        if (payloads.isEmpty()) onBindViewHolder(holder, position)
        else {
            for (payload in payloads) {
                if (payload is ViewImageItem) holder.rebindOnChanges(payload)
            }
        }
    }

    override fun onBindViewHolder(holder: BaseImageViewHolder, position: Int) {
        holder.bind(getItem(position)!!)
    }

    override fun getItemViewType(position: Int): Int {
        return getItem(position)!!.type.ordinal
    }

    override fun onViewRecycled(holder: BaseImageViewHolder) {
        holder.onViewRecycled()
    }
}