package com.ibuildapp.inventory.model

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.content.Context
import com.ibuildapp.inventory.backend.model.ServerMultipartItem
import com.ibuildapp.inventory.stuff.Utils
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import java.util.*

@Entity(tableName = "inventory_items")
class InventoryItem (
        @PrimaryKey
        @ColumnInfo(name = "inventory_item_id")
        val id: String,

        @ColumnInfo(name = "inventory_item_is_folder")
        val isFolder: Boolean,

        @ColumnInfo(name = "inventory_item_parent_id")
        val parentId: String? = null,

        @ColumnInfo(name = "inventory_item_inventory_id")
        val inventoryId: Int,

        @ColumnInfo(name = "inventory_item_sku")
        var sku: String? = null,

        @ColumnInfo(name = "inventory_item_notes")
        var notes: String? = null,

        @ColumnInfo(name = "inventory_item_image_url")
        var imageUrl: List<String>? = null,

        @ColumnInfo(name = "inventory_item_price")
        var price: Float? = null,

        @ColumnInfo(name = "inventory_item_active")
        var active: Boolean = true,

        @ColumnInfo(name = "inventory_item_name")
        var name: String? = null,

        @ColumnInfo(name = "inventory_item_name_lower")
        var nameLower: String? = null,

        @ColumnInfo(name = "inventory_item_category")
        var category: String? = null,

        @ColumnInfo(name = "inventory_item_quantity")
        var quantity: Int? = null,

        @ColumnInfo(name = "inventory_item_barcode")
        var barcode: String? = null,

        @ColumnInfo(name = "inventory_item_barcode_format")
        var barcodeFormat: String? = null,

        @ColumnInfo(name = "inventory_item_created_at")
        var createdAt: Date? = null,

        @ColumnInfo(name = "inventory_item_updated_at")
        var updatedAt: Date? = null
) {
        fun toServerItem(): ServerMultipartItem {

                val id = when {
                        id.isNullOrEmpty() -> null
                        isFolder -> id.replace("_folder", "")
                        else -> id.replace("_item", "")
                }

                return ServerMultipartItem(
                        id = id?.toInt(),
                        inventoryId = inventoryId,
                        parentId = parentId?.removeSuffix("_folder")?.removeSuffix("_item")?.toInt(),
                        isFolder = isFolder,
                        active = active,
                        name = name,
                        description = notes,
                        sku = sku,
                        barcode = barcode,
                        barcodeFormat = barcodeFormat,
                        quantity = quantity,
                        price = price,
                        images = imageUrl
                )
        }
}