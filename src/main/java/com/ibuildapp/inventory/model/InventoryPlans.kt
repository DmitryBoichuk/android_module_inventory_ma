package com.ibuildapp.inventory.model

import com.ibuildapp.inventory.R

enum class InventoryPlans(val entriesCount: Int, val nameRes: Int) {
    FREE(100, R.string.inventory_plan_free),
    BUSINESS(1000, R.string.inventory_plan_business)


}