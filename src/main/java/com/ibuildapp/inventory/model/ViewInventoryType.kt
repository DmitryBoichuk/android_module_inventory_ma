package com.ibuildapp.inventory.model

enum class ViewInventoryType {
    TITLE, FOLDER, FILE, DELIMITER, SEARCH
}
