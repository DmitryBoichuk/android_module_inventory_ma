package com.ibuildapp.inventory.model

import com.google.zxing.BarcodeFormat

data class ViewBarcodeResult (val barcode: String, val barcodeFormat: BarcodeFormat, val item: ViewInventoryItem?= null)