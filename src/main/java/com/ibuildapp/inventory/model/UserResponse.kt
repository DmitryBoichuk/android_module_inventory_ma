package com.ibuildapp.inventory.model

import com.ibuildapp.inventory.backend.model.ServerUser

class UserResponse(
        val user: ServerUser? = null,
        val error: String? = null,
        val session: String? = null
)