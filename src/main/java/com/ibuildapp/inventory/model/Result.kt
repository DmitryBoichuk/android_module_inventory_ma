package com.ibuildapp.inventory.model

enum class Result {
    LOADING, SUCCESS, ERROR
}