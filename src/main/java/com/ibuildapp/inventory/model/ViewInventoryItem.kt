package com.ibuildapp.inventory.model

import android.support.v7.util.DiffUtil

open class ViewInventoryItem(
        val id: String,
        val viewInventoryType: ViewInventoryType,
        val name: String? = null,
        val foldersCount: Int = 0,
        val filesCount: Int = 0,
        val textId: String,
        val quantityString: String? = null,
        val priceString: String? = null,
        val firstImageUrl: String? = null
) {

    object DiffCallback : DiffUtil.ItemCallback<ViewInventoryItem>() {
        override fun areItemsTheSame(oldItem: ViewInventoryItem, newItem: ViewInventoryItem): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: ViewInventoryItem, newItem: ViewInventoryItem): Boolean {
            return oldItem.toString() == newItem.toString()
        }

        override fun getChangePayload(oldItem: ViewInventoryItem, newItem: ViewInventoryItem): Any? {
            return newItem
        }
    }

    override fun toString(): String {
        return "ViewInventoryItem(id='$id', viewInventoryType=$viewInventoryType, name=$name, " +
                "foldersCount=$foldersCount, filesCount=$filesCount, textId='$textId', " +
                "quantityString=$quantityString, priceString=$priceString, firstImageUrl=$firstImageUrl)"
    }


}