package com.ibuildapp.inventory.model

class UserRequest(val user: User? = null, val error: String? = null, val session: String)