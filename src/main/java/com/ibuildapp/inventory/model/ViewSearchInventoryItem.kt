package com.ibuildapp.inventory.model

import android.support.v7.util.DiffUtil
import android.text.Spannable

class ViewSearchInventoryItem(id: String, viewInventoryType: ViewInventoryType, name: String? = null,
                              foldersCount: Int = 0, filesCount: Int = 0, textId: String,
                              val filter: String = "", val spannableName: Spannable,
                              quantityString: String? = null,
                              firstImageUrl: String? = null,
                              priceString: String? = null)
    : ViewInventoryItem(id, viewInventoryType, name, foldersCount, filesCount, textId,
        quantityString = quantityString, priceString = priceString, firstImageUrl = firstImageUrl) {

    object DiffCallback : DiffUtil.ItemCallback<ViewSearchInventoryItem>() {
        override fun areItemsTheSame(oldItem: ViewSearchInventoryItem, newItem: ViewSearchInventoryItem): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: ViewSearchInventoryItem, newItem: ViewSearchInventoryItem): Boolean {
            return oldItem.contentToString() == newItem.contentToString()
        }

        override fun getChangePayload(oldItem: ViewSearchInventoryItem, newItem: ViewSearchInventoryItem): Any? {
            return newItem
        }
    }

    fun contentToString(): String {
        return super.toString() + " " + filter
    }
}