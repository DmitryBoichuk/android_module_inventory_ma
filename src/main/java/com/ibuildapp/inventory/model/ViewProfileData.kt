package com.ibuildapp.inventory.model

data class ViewProfileData(val email: String,
                           val fullName: String,
                           val percent: String,
                           val percentInt: Int,
                           val planName: String,
                           val itemsCount: String,
                           val remainItems: String,
                           var buttonName: String,
                           var buttonLink: String )