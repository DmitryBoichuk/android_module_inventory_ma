package com.ibuildapp.inventory.model

import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Relation

class InventoryItemWithItems {
        @Embedded
        lateinit var inventoryItem: InventoryItem

        @Relation(parentColumn = "inventory_item_id", entityColumn = "inventory_item_parent_id", entity = InventoryItem::class)
        lateinit  var subItems: List<InventoryItem>
}