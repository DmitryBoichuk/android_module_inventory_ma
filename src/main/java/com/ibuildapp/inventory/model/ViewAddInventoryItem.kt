package com.ibuildapp.inventory.model

import android.content.Context
import com.ibuildapp.inventory.backend.model.ServerMultipartItem
import com.ibuildapp.inventory.stuff.Utils
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import java.util.*

data class ViewAddInventoryItem (
        val id: String?,

        val isFolder: Boolean,

        val parentId: String? = null,

        var inventoryId: Int,

        var sku: String? = null,

        var notes: String? = null,

        var imageUrl: List<ViewImageItem>? = null,

        var price: Float? = null,

        var name: String? = null,

        var nameLower: String? = null,

        var category: String? = null,

        var quantity: Int? = null,

        var barcode: String? = null,

        var barcodeFormat: String? = null,

        var date: Date? = null
) {
    fun toServerItem(context: Context): ServerMultipartItem {
        val partFiles = mutableListOf<MultipartBody.Part>()
        val imageUrls = mutableListOf<String>()

        imageUrl?.forEach { imageUrl ->
            if (imageUrl.type == ViewImageType.ITEM) {
                imageUrl.uri?.let { uri ->
                    val file = Utils.getFileFromUri(context, uri)
                    val requestFile: RequestBody = file.asRequestBody("multipart/form-data".toMediaTypeOrNull())
                    val body: MultipartBody.Part = MultipartBody.Part.createFormData("file", file.name, requestFile)
                    partFiles.add(body)
                }

                imageUrl.url?.let {
                    imageUrls.add(it)
                }
            }
        }

        val id = when {
            id.isNullOrEmpty() -> null
            isFolder -> id.replace("_folder", "")
            else -> id.replace("_item", "")
        }

        return ServerMultipartItem(
                id = id?.toInt(),
                inventoryId = inventoryId,
                parentId = parentId?.removeSuffix("_folder")?.removeSuffix("_item")?.toInt(),
                isFolder = isFolder,
                active = true,
                name = name,
                description = notes,
                sku = sku,
                barcode = barcode,
                barcodeFormat = barcodeFormat,
                quantity = quantity,
                price = price,
                images = imageUrls,
                files = partFiles
        )
    }

    fun toInventoryItem(): InventoryItem {
        val imageItems = mutableListOf<String>()
        imageUrl?.forEach {
            if (it.type == ViewImageType.ITEM && !it.url.isNullOrEmpty())
                imageItems.add(it.url)
        }

        if (id.isNullOrEmpty())
            throw Throwable("id is null")

        val id = if (isFolder) id + "_folder" else id + "_item"

        return InventoryItem (
                id = id,
                isFolder = isFolder,
                parentId = parentId,
                sku = sku,
                notes = notes,
                price = price,
                name = name,
                nameLower = nameLower,
                category = category,
                quantity = quantity,
                barcode = barcode,
                barcodeFormat = barcodeFormat,
                createdAt = date,
                imageUrl = imageItems,
                inventoryId = inventoryId
        )
    }
}