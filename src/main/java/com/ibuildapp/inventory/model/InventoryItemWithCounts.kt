package com.ibuildapp.inventory.model

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Ignore
import java.util.*

class InventoryItemWithCounts(
        @ColumnInfo(name = "inventory_item_id")
        val id: String,

        @ColumnInfo(name = "inventory_item_is_folder")
        val isFolder: Boolean,

        @ColumnInfo(name = "inventory_item_parent_id")
        val parentId: String? = null,

        @ColumnInfo(name = "inventory_item_sku")
        var sku: String? = null,

        @ColumnInfo(name = "inventory_item_notes")
        var notes: String? = null,

        @ColumnInfo(name = "inventory_item_image_url")
        var imageUrl: List<String>? = null,

        @ColumnInfo(name = "inventory_item_name")
        var name: String? = null,

        @ColumnInfo(name = "inventory_item_name_lower")
        var nameLower: String,

        @ColumnInfo(name = "inventory_item_category")
        var category: String? = null,

        @ColumnInfo(name = "inventory_item_quantity")
        var quantity: Int? = null,

        @ColumnInfo(name = "inventory_item_price")
        var price: Float? = null,

        @ColumnInfo(name = "inventory_item_barcode")
        var barcode: String? = null,

        @ColumnInfo(name = "inventory_item_barcode_format")
        var barcodeFormat: String? = null,

        @ColumnInfo(name = "inventory_item_created_at")
        var createdAt: Date? = null,

        @ColumnInfo(name = "inventory_item_updated_at")
        var updatedAt: Date? = null,

        @ColumnInfo(name = "inventory_folder_count")
        var folderCount: Int? = null,

        @ColumnInfo(name = "inventory_files_count")
        var filesCount: Int? = null
) {
    @Ignore
    var subItems = mutableListOf<InventoryItemWithCounts>()

    @Ignore
    var filterString: String = ""

    fun calculateItemsCount() {
        subItems.forEach {
            it.calculateItemsCount()
        }

        var filesCount = 0
        var foldersCount = 0

        var partFilter = nameLower

        subItems.forEach {
            if (it.isFolder) {
                foldersCount++
                foldersCount += it.folderCount ?: 0
                filesCount += it.filesCount ?: 0

                partFilter += it.filterString
            } else {
                filesCount++
                partFilter += it.filterString
            }
        }

        this.folderCount = foldersCount
        this.filesCount = filesCount
        this.filterString = partFilter

        println("-ttt ${this.name} folders: ${this.folderCount} files: ${this.filesCount} filterString: $filterString")
    }
}