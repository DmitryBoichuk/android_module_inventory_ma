package com.ibuildapp.inventory.model

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "users")
open class User (
        @PrimaryKey
        @ColumnInfo(name = "user_id")
        val id: String,

        @ColumnInfo(name = "user_first_name")
        val firstName: String,

        @ColumnInfo(name = "user_last_name")
        val lastName: String,

        @ColumnInfo(name = "user_email")
        val email: String,

        @ColumnInfo(name = "user_picture")
        val picture: String? = null,

        @ColumnInfo(name = "user_plan_name")
        val planName: String,

        @ColumnInfo(name = "user_plan_limit")
        val planLimit: Int,

        @ColumnInfo(name = "user_button_name")
        val buttonName: String,

        @ColumnInfo(name = "user_button_link")
        val buttonLink: String,

        @ColumnInfo(name = "user_is_owner")
        val isOwner: Boolean,

        @ColumnInfo(name = "user_inventories")
        val inventories: List<Int>
)