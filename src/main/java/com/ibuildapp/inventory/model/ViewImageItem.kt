package com.ibuildapp.inventory.model

import android.net.Uri
import android.support.v7.util.DiffUtil

data class ViewImageItem(val url:String? = null, val uri: Uri? = null, val type: ViewImageType) {
    object DiffCallback: DiffUtil.ItemCallback<ViewImageItem>() {
        override fun areItemsTheSame(oldItem: ViewImageItem, newItem: ViewImageItem): Boolean {
            return oldItem.toString() == newItem.toString()
        }

        override fun areContentsTheSame(oldItem: ViewImageItem, newItem: ViewImageItem): Boolean {
            return oldItem.toString() == newItem.toString()
        }

        override fun getChangePayload(oldItem: ViewImageItem, newItem: ViewImageItem): Any? {
            return newItem
        }
    }
}

enum class ViewImageType {
    ITEM, ADD
}