package com.ibuildapp.inventory.model

import com.google.zxing.BarcodeFormat


data class LoadQRData (val scannedText: String, val scannedFormat: BarcodeFormat){
    var inventoryItem: ViewInventoryItem? = null
}
