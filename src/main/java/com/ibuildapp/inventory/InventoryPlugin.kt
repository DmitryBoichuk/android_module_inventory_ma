package com.ibuildapp.inventory

import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.ibuildapp.inventory.databinding.InventoryPluginBinding
import com.ibuildapp.inventory.viewmodel.InventoryPluginViewModel

open class InventoryPlugin : AppCompatActivity() {
    private lateinit var binding: InventoryPluginBinding

    private val viewModel: InventoryPluginViewModel by lazy { ViewModelProviders.of(this).get(InventoryPluginViewModel::class.java) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = InventoryPluginBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        overridePendingTransition(0, 0)
        if (viewModel.isAuthorized()) {
            val intent = Intent(this, InventoryActivity::class.java)
            startActivity(intent, Bundle())
        } else {
            val mainIntent = Intent(this, LoginActivity::class.java)
            startActivity(mainIntent, Bundle())
        }

        overridePendingTransition(0, 0)
        finish()
    }
}