package com.ibuildapp.inventory.fragment

import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.view.inputmethod.InputMethodManager
import androidx.navigation.Navigation
import com.ibuildapp.inventory.R
import com.ibuildapp.inventory.stuff.OnBackListener

open class BaseFragment : Fragment(), OnBackListener {
    private var dialog: ProgressDialog? = null
    protected fun showProgress() {
        dialog = ProgressDialog.show(requireContext(), "",
                getString(R.string.inventory_dialog_wait), true)
    }

    protected fun hideProgress() {
        dialog?.let {
            if (it.isShowing) it.dismiss()
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        arguments?.let { onProcessArguments(it) }
    }

    open fun onProcessArguments(arguments: Bundle) {

    }

    override fun onStart() {
        super.onStart()

        if (hideKeyBoardOnStart())
            hideSoftInput(activity)
    }

    open fun hideKeyBoardOnStart(): Boolean {
        return true
    }

    override fun onBackPressed(): Boolean {
        val manager: FragmentManager = fragmentManager ?: return false
        val view = view ?: return false

        return if (manager.backStackEntryCount == 0) false else view.let { Navigation.findNavController(it).popBackStack() }
    }

    open fun hideSoftInput(activity: Activity?): Boolean {
        if (activity != null && activity.window != null) {
            val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            return imm.hideSoftInputFromWindow(activity.window.decorView.windowToken, 0)
        }
        return false
    }
}