package com.ibuildapp.inventory.fragment

import android.Manifest
import android.app.Activity.RESULT_OK
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.support.v4.content.PermissionChecker
import android.widget.Toast
import com.google.zxing.BarcodeFormat
import com.google.zxing.integration.android.IntentIntegrator
import com.ibuildapp.inventory.AddDataActivity
import com.ibuildapp.inventory.QRCodeReaderActivity
import com.ibuildapp.inventory.R
import com.ibuildapp.inventory.adapter.ImagesAdapter
import com.ibuildapp.inventory.backend.model.Product
import com.ibuildapp.inventory.callback.ImagesCallback
import com.ibuildapp.inventory.stuff.LaunchUtils
import com.ibuildapp.inventory.viewmodel.AddDataViewModel
import kotlinx.coroutines.MainScope
import kotlin.properties.Delegates

abstract class BaseAddDataFragment: BaseFragment(), ImagesCallback {
    private val permissionCode = 1001

    protected var isAdd: Boolean = false
    private var itemId: String? = null
    protected var parentItemId: String? = null
    private var isFolder by Delegates.notNull<Boolean>()

    private var scannedText: String? = null
    private var scannedFormat: BarcodeFormat? = null

    private var product: Product? = null

    protected val imagesAdapter: ImagesAdapter by lazy { ImagesAdapter(this) }

    protected var mainScope = MainScope()

    protected val viewModel: AddDataViewModel by lazy {
        val factory = AddDataViewModel.ViewModelFactory(
                application = activity!!.application,
                itemId = itemId,
                isFolder = isFolder,
                parentItemId = parentItemId,
                scannedText = scannedText,
                scannedFormat = scannedFormat,
                product = product
        )

        ViewModelProviders.of(this, factory).get(AddDataViewModel::class.java)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        arguments?.let {
            isAdd = it.getBoolean(LaunchUtils.isAdd)
            itemId = it.getString(LaunchUtils.itemId)
            parentItemId = it.getString(LaunchUtils.parentId)
            isFolder = it.getBoolean(LaunchUtils.isFolder)

            scannedText = it.getString(LaunchUtils.scannedText)
            scannedFormat = it.getSerializable(LaunchUtils.scannedFormat) as BarcodeFormat?

            product = it.getSerializable(LaunchUtils.data) as Product?
        }
    }

    protected val addDataActivity: AddDataActivity?
        get() = activity as AddDataActivity?

    override fun onStart() {
        super.onStart()

        addDataActivity?.setInternalOnBackPressedListener(this)
    }

    override fun onAddClick() {
        openImageFromGallery()
    }

    private fun openImageFromGallery() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if (PermissionChecker.checkSelfPermission(requireContext(), Manifest.permission.READ_EXTERNAL_STORAGE) ==
                    PackageManager.PERMISSION_DENIED){
                val permissions = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE);
                requestPermissions(permissions, permissionCode)
            }
            else innerOpenImageFromGallery()
        }
        else innerOpenImageFromGallery()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when(requestCode){
            permissionCode -> {
                if (grantResults.isNotEmpty() && grantResults[0] ==
                        PackageManager.PERMISSION_GRANTED){
                    innerOpenImageFromGallery()
                }
                else Toast.makeText(requireContext(), getString(R.string.inventory_permission_denied), Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun innerOpenImageFromGallery() {
        val intent = Intent(Intent.ACTION_PICK).apply {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
            }

            action = Intent.ACTION_GET_CONTENT
            type = "image/*"
        }

        startActivityForResult(Intent.createChooser(intent,"Select Picture"),  LaunchUtils.pickerRequest)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == IntentIntegrator.REQUEST_CODE) {
            val result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
            if (result != null) {
                if (result.contents != null)
                    processContent(result.contents, result.formatName)

            }
        } else if (requestCode == LaunchUtils.pickerRequest
                && resultCode == RESULT_OK) {
            val uriList = mutableListOf<Uri>()

            data?.clipData?.let {
                val count = it.itemCount
                for (i in 0 until count) {
                    val imageUri: Uri = it.getItemAt(i).uri
                    uriList.add(imageUri)
                }
            } ?: let {
                data?.data?.let {
                    uriList.add(it)
                }
            }

            onImageAdded(uriList)

        }else super.onActivityResult(requestCode, resultCode, data)
    }

    protected abstract fun onImageAdded(uriList: MutableList<Uri>)

    protected abstract fun processContent(contents: String, formatName: String)

    protected fun startQRCodeActivity() {
        val intent = Intent(activity, QRCodeReaderActivity::class.java)
        startActivityForResult(intent, IntentIntegrator.REQUEST_CODE)
    }
}