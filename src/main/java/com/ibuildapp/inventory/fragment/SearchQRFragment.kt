package com.ibuildapp.inventory.fragment

import android.Manifest
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v4.content.PermissionChecker
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.Navigation
import com.google.zxing.BarcodeFormat
import com.google.zxing.integration.android.IntentIntegrator
import com.ibuildapp.inventory.AddDataActivity
import com.ibuildapp.inventory.QRCodeReaderActivity
import com.ibuildapp.inventory.R
import com.ibuildapp.inventory.adapter.SearchItemsAdapter
import com.ibuildapp.inventory.callback.ItemsCallback
import com.ibuildapp.inventory.databinding.InventoryFragmentSearchQrBinding
import com.ibuildapp.inventory.model.ViewInventoryType
import com.ibuildapp.inventory.stuff.ItemUtils
import com.ibuildapp.inventory.stuff.LaunchUtils
import com.ibuildapp.inventory.stuff.SimpleTextWatcher
import com.ibuildapp.inventory.stuff.Utils
import com.ibuildapp.inventory.viewmodel.SearchQRViewModel
import com.journeyapps.barcodescanner.BarcodeEncoder
import kotlinx.coroutines.*

class SearchQRFragment : BaseSearchQRFragment(), ItemsCallback {
    private lateinit var binding: InventoryFragmentSearchQrBinding

    private val dp50: Int by lazy { Utils.dpToPixels(requireContext(), 50) }
    private val dp100: Int by lazy { Utils.dpToPixels(requireContext(), 100) }
    private val val150: Int = 150
    private val val300: Int = 300

    private var mainScope = MainScope()

    private val viewModel: SearchQRViewModel by lazy {
        val factory = SearchQRViewModel.ViewModelFactory(activity!!.application)
        ViewModelProviders.of(this, factory).get(SearchQRViewModel::class.java)
    }

    private val searchAdapter: SearchItemsAdapter by lazy { SearchItemsAdapter(this) }

    private var filter: String = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = InventoryFragmentSearchQrBinding.inflate(inflater)

        initViews()
        initViewModel()

        return binding.root
    }

    private fun initViewModel() {
        viewModel.getState().observe(viewLifecycleOwner, Observer { state ->
            when (state) {
                SearchQRViewModel.State.SCAN_TO_SEARCH -> {
                    binding.gScanToSearch.visibility = View.VISIBLE
                    binding.gRvItems.visibility = View.GONE
                    binding.gBarcode.visibility = View.GONE
                }
                SearchQRViewModel.State.NAME_SEARCH -> {
                    binding.gScanToSearch.visibility = View.GONE
                    binding.gRvItems.visibility = View.VISIBLE
                    binding.gBarcode.visibility = View.GONE
                }
                SearchQRViewModel.State.BARCODE_RESULT -> {
                    binding.gScanToSearch.visibility = View.GONE
                    binding.gRvItems.visibility = View.GONE
                    binding.gBarcode.visibility = View.VISIBLE
                }
            }
        })

        viewModel.getData().observe(viewLifecycleOwner, Observer {items ->
            searchAdapter.submitList(items)

            items?.let {
                if (viewModel.getState().value == SearchQRViewModel.State.NAME_SEARCH){
                    if (items.isEmpty() && filter.length >= 2)
                        binding.tvNothingFound.visibility = View.VISIBLE
                    else binding.tvNothingFound.visibility = View.GONE
                }
            }
        })

        viewModel.getBarcodeData().observe(viewLifecycleOwner, Observer {
            val result = it ?: return@Observer

            binding.tvBarcodeResult.text = result.barcode
            showBarcode(result.barcode, result.barcodeFormat)

            binding.ivBarcodeImage.visibility = View.VISIBLE
            binding.tvBarcodeResult.visibility = View.VISIBLE

            it.item?.let {viewItem ->

                binding.tvBarcodeNothingFound.visibility = View.GONE
                binding.vBarcodeNewBackground.visibility = View.GONE
                binding.tvBarcodeNewScan.visibility = View.GONE

                if (viewItem.viewInventoryType == ViewInventoryType.FILE){
                    binding.incFile.root.visibility = View.VISIBLE
                    binding.incFolder.root.visibility = View.GONE

                    ItemUtils.bindFile(binding.incFile, viewItem)

                    binding.incFile.clRoot.setOnClickListener { onItemClick(viewItem.id, viewItem.name ?: "") }
                } else {
                    binding.incFile.root.visibility = View.GONE
                    binding.incFolder.root.visibility = View.VISIBLE

                    ItemUtils.bindFolder(binding.incFolder, viewItem)
                    binding.incFolder.clRoot.setOnClickListener { onItemClick(viewItem.id, viewItem.name ?: "") }
                }
            }?: let {
                binding.incFile.root.visibility = View.GONE
                binding.incFolder.root.visibility = View.GONE

                binding.tvBarcodeNothingFound.visibility = View.VISIBLE
                binding.vBarcodeNewBackground.visibility = View.VISIBLE
                binding.tvBarcodeNewScan.visibility = View.VISIBLE
            }
        })
    }

    private fun showBarcode(barcode: String, barcodeFormat: BarcodeFormat) {
        runBlocking {
            GlobalScope.launch(Dispatchers.IO) {
                try {
                    val barcodeEncoder = BarcodeEncoder()

                    val height = if (barcodeFormat == BarcodeFormat.QR_CODE) val300 else val150

                    val bitmap = barcodeEncoder.encodeBitmap(barcode, barcodeFormat, val300, height)

                    val bitmapViewHeight = if (barcodeFormat == BarcodeFormat.QR_CODE) dp100 else dp50

                    mainScope.launch {
                        (binding.ivBarcodeImage.layoutParams as ConstraintLayout.LayoutParams).height = bitmapViewHeight
                        binding.ivBarcodeImage.setImageBitmap(bitmap)
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }
    }

    private fun initViews() {
        inventoryActivity?.let {
            it.setSupportActionBar(it.getToolbar())
            it.getToolbar().title = getString(R.string.inventory_items_search)
        }

        inventoryActivity?.supportActionBar?.let { actionBar ->
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2)
                actionBar.setHomeAsUpIndicator(R.drawable.svg_inventory_arrow_back)

            actionBar.setHomeButtonEnabled(true)
            actionBar.setDisplayHomeAsUpEnabled(true)
        }

        binding.etSearch.addTextChangedListener(object : SimpleTextWatcher() {
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                processFilter(s)
            }
        })

        binding.ivClear.setOnClickListener { binding.etSearch.setText("") }
        binding.rvItems.adapter = searchAdapter

        binding.vSearchBackground.setOnClickListener { openCameraForQR() }
        binding.vBarcodeNewBackground.setOnClickListener { openCameraForQR() }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == LaunchUtils.cameraRequest) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                innerOpenCameraForQR()
            else
                Toast.makeText(requireContext(), "camera permission denied", Toast.LENGTH_LONG).show()
        }
    }

    private fun openCameraForQR() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if (PermissionChecker.checkSelfPermission(requireContext(), Manifest.permission.READ_EXTERNAL_STORAGE) ==
                    PackageManager.PERMISSION_DENIED){
                val permissions = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE);
                requestPermissions(permissions, LaunchUtils.cameraRequest)
            }
            else innerOpenCameraForQR()
        }
        else innerOpenCameraForQR()
    }

    private fun innerOpenCameraForQR() {
        val intent = Intent(activity, QRCodeReaderActivity::class.java)
        startActivityForResult(intent, IntentIntegrator.REQUEST_CODE)
    }

    private fun processFilter(text: CharSequence) {
        filter = text.toString()
        binding.ivClear.visibility = if (filter.isEmpty()) View.GONE else View.VISIBLE
        viewModel.updateDataWithFilter(filter)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        inventoryActivity?.let {
            it.getToolbar().setNavigationOnClickListener { inventoryActivity!!.onBackPressed() }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == IntentIntegrator.REQUEST_CODE) {
            val result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
            if (result != null) {
                if (result.contents != null)
                    processContent(result.contents, result.formatName)

            }
        } else super.onActivityResult(requestCode, resultCode, data)
    }

    private fun processContent(contents: String, formatName: String) {
        viewModel.processContent(contents, formatName)
    }

    override fun onFolderClick(id: String, name: String) {
        val bundle = Bundle().apply {
            putString(LaunchUtils.title, name)
            putString(LaunchUtils.parentId, id)
        }

        view?.let {
            Navigation.findNavController(it).navigate(R.id.action_items_to_items, bundle)
        }
    }

    override fun onSearchClick() {
        TODO("Not yet implemented")
    }

    override fun onEditFolderClick(id: String) {
        TODO("Not yet implemented")
    }

    override fun onDeleteFolderClick(id: String, isFolder: Boolean) {
        TODO("Not yet implemented")
    }

    override fun onItemClick(id: String, name: String) {
        val intent = Intent(activity, AddDataActivity::class.java).apply {
            putExtra(LaunchUtils.isAdd, false)
            putExtra(LaunchUtils.isFolder, false)
            putExtra(LaunchUtils.itemId, id)
        }

        startActivity(intent)
    }

    override fun deleteItemClick(id: String) {
        TODO("Not yet implemented")
    }

}