package com.ibuildapp.inventory.fragment

import androidx.navigation.Navigation
import com.ibuildapp.inventory.InventoryActivity

open class BaseMainFragment : BaseFragment() {
    override fun onBackPressed(): Boolean {
        val view = view ?: return false
        val manager = fragmentManager
        return if (manager != null && manager.backStackEntryCount == 0) false else Navigation.findNavController(view).popBackStack()
    }

    override fun onStart() {
        super.onStart()
        (activity as InventoryActivity).getToolbar().menu.clear()
        (activity as InventoryActivity).setInternalOnBackPressedListener(this)
    }
}