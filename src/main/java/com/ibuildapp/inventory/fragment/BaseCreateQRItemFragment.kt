package com.ibuildapp.inventory.fragment

import com.ibuildapp.inventory.CreateItemFromQRActivity

open class BaseCreateQRItemFragment: BaseFragment() {
    protected val createQRActivity: CreateItemFromQRActivity?
        get() = activity as CreateItemFromQRActivity?

    override fun onStart() {
        super.onStart()

        createQRActivity?.setInternalOnBackPressedListener(this)
    }
}