package com.ibuildapp.inventory.fragment

import com.ibuildapp.inventory.InventoryActivity

abstract class BaseSearchQRFragment: BaseFragment() {
    protected val inventoryActivity: InventoryActivity?
        get() = activity as InventoryActivity?

    override fun onStart() {
        super.onStart()

        inventoryActivity?.setInternalOnBackPressedListener(this)
    }

}