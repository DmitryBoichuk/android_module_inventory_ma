package com.ibuildapp.inventory.fragment

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.Navigation
import com.google.zxing.BarcodeFormat
import com.ibuildapp.inventory.AddDataActivity
import com.ibuildapp.inventory.R
import com.ibuildapp.inventory.backend.model.LookupStatus
import com.ibuildapp.inventory.backend.model.Product
import com.ibuildapp.inventory.databinding.InventoryFragmentLoadQrDataBinding
import com.ibuildapp.inventory.model.LoadQRData
import com.ibuildapp.inventory.model.ViewInventoryType
import com.ibuildapp.inventory.stuff.ItemUtils
import com.ibuildapp.inventory.stuff.LaunchUtils
import com.ibuildapp.inventory.stuff.Utils
import com.ibuildapp.inventory.viewmodel.LoadQRDataViewModel
import com.journeyapps.barcodescanner.BarcodeEncoder
import kotlinx.coroutines.*

class LoadQRDataFragment : BaseCreateQRItemFragment() {
    private var parentItemId: String? = null
    private lateinit var scannedText: String
    private lateinit var scannedFormat: BarcodeFormat

    private lateinit var binding: InventoryFragmentLoadQrDataBinding

    private var mainScope = MainScope()

    private val dp100: Int by lazy { Utils.dpToPixels(requireContext(), 100) }
    private val dp200: Int by lazy { Utils.dpToPixels(requireContext(), 200) }
    private val val300: Int = 300
    private val val600: Int = 600

    private val viewModel: LoadQRDataViewModel by lazy {
        val factory = LoadQRDataViewModel.ViewModelFactory(
                application = activity!!.application,
                parentItemId = parentItemId,
                scannedText = scannedText,
                scannedFormat = scannedFormat
        )
        ViewModelProviders.of(this, factory).get(LoadQRDataViewModel::class.java)
    }

    override fun onProcessArguments(arguments: Bundle) {
        super.onProcessArguments(arguments)

        parentItemId = arguments.getString(LaunchUtils.parentId)
        // Мы не можем зайти на экран если текст пустой или нул, а если это происходит то это ошибка
        // в разработке и мы смело крашимся
        scannedText = arguments.getString(LaunchUtils.scannedText)!!
        scannedFormat = arguments.getSerializable(LaunchUtils.scannedFormat) as BarcodeFormat
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = InventoryFragmentLoadQrDataBinding.inflate(inflater)

        initViews()
        initViewModel()

        return binding.root
    }

    private fun initViewModel() {
        viewModel.getState().observe(viewLifecycleOwner, Observer {state ->
            when (state) {
                LoadQRDataViewModel.State.NONE -> {
                    binding.gNewProduct.visibility = View.GONE
                    binding.gExists.visibility = View.GONE
                }
                LoadQRDataViewModel.State.NEW_LABEL -> {
                    binding.gNewProduct.visibility = View.VISIBLE
                    binding.gExists.visibility = View.GONE
                }
                LoadQRDataViewModel.State.LABEL_FOUND -> {
                    binding.gNewProduct.visibility = View.GONE
                    binding.gExists.visibility = View.VISIBLE
                }
            }
        })

        viewModel.getData().observe(viewLifecycleOwner, Observer {data ->
            data?.let {
                when (viewModel.getState().value) {
                    LoadQRDataViewModel.State.NEW_LABEL -> setupNewLabel(it)
                    LoadQRDataViewModel.State.LABEL_FOUND -> setupLabelFound(it)
                    else -> {

                    }
                }
            }
        })
    }

    private fun setupLabelFound(loadQRData: LoadQRData) {
        loadQRData.inventoryItem?.let {item ->
           if (item.viewInventoryType == ViewInventoryType.FILE){
                binding.incFile.root.visibility = View.VISIBLE
                binding.incFolder.root.visibility = View.GONE

               ItemUtils.bindFile(binding.incFile, item)
           } else {
               binding.incFile.root.visibility = View.GONE
               binding.incFolder.root.visibility = View.VISIBLE

               ItemUtils.bindFolder(binding.incFolder, item)
           }

        }?:let {
            binding.incFile.root.visibility = View.GONE
            binding.incFolder.root.visibility = View.GONE
        }
    }

    private fun setupNewLabel(data: LoadQRData) {
        runBlocking {
            GlobalScope.launch (Dispatchers.IO) {
                try {
                    val barcodeEncoder = BarcodeEncoder()

                    val height = if (data.scannedFormat == BarcodeFormat.QR_CODE) val600 else val300

                    val bitmap = barcodeEncoder.encodeBitmap(data.scannedText, data.scannedFormat, val600, height)

                    val bitmapViewHeight = if (data.scannedFormat == BarcodeFormat.QR_CODE) dp200 else dp100

                    mainScope.launch {

                        (binding.ivNewBarcodeImage.layoutParams as ConstraintLayout.LayoutParams).height = bitmapViewHeight
                        binding.ivNewBarcodeImage.setImageBitmap(bitmap)

                        binding.tvNewScanResult.text = data.scannedText
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        createQRActivity?.apply {
            getToolbar().setNavigationOnClickListener { onBackPressed() }
            showSkip()
        }
    }

    private fun initViews() {
        binding.srlNewRefresh.isEnabled = false
        binding.srlNewRefresh.setColorSchemeColors(ContextCompat.getColor(requireContext(), R.color.colorAccent))

        createQRActivity?.apply {
            setSupportActionBar(getToolbar())
            getToolbar().title = getString(R.string.inventory_title_scan)
        }

        createQRActivity?.supportActionBar?.let { actionBar ->
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2)
                actionBar.setHomeAsUpIndicator(R.drawable.svg_inventory_close)

            actionBar.setHomeButtonEnabled(true)
            actionBar.setDisplayHomeAsUpEnabled(true)
        }

        binding.vNewLookupBackground.setOnClickListener { loadDataProductData() }

        binding.vNewLinkBackground.setOnClickListener { linkToAddItem() }

        binding.vNewRescanBackground.setOnClickListener { reopenQRActivity() }

        binding.vExistsNewBackground.setOnClickListener { reopenQRActivity() }
    }

    private fun linkToAddItem() {
        val parentId =  arguments?.getString(LaunchUtils.parentId) ?: ""

        val intent = Intent(activity, AddDataActivity::class.java)
        intent.putExtra(LaunchUtils.isAdd, true)
        intent.putExtra(LaunchUtils.isFolder, false)
        intent.putExtra(LaunchUtils.parentId, parentId)
        intent.putExtra(LaunchUtils.scannedText, scannedText)
        intent.putExtra(LaunchUtils.scannedFormat, scannedFormat)

        startActivity(intent)
    }

    private fun reopenQRActivity() {
        createQRActivity?.reopenQRActivity()
    }

    private fun loadDataProductData() {
        runBlocking {
            mainScope.launch {
                binding.srlNewRefresh.isRefreshing = true

                launch(Dispatchers.IO){
                    val result = withContext(Dispatchers.Default) { viewModel.loadDataForQR() }

                    mainScope.launch {
                        binding.srlNewRefresh.isRefreshing = false

                        when (result.status) {
                            LookupStatus.SUCCESS -> showLoadedData(result.product!!)
                            LookupStatus.ERROR -> Toast.makeText(requireContext(), getString(R.string.inventory_loading_error), Toast.LENGTH_SHORT).show()
                            LookupStatus.NOTHING_FOUND -> Toast.makeText(requireContext(), getString(R.string.inventory_nothing_found), Toast.LENGTH_SHORT).show()
                        }
                    }
                }
            }
        }
    }

    private fun showLoadedData(product: Product) {
        view?.let {
            val bundle = Bundle().apply {
                putString(LaunchUtils.scannedText, scannedText)
                putSerializable(LaunchUtils.scannedFormat, scannedFormat)
                putString(LaunchUtils.parentId, parentItemId)
                putSerializable(LaunchUtils.data, product)
            }
            Navigation.findNavController(it).navigate(R.id.action_load_qr_to_show_qr, bundle)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        mainScope.cancel()
    }
}