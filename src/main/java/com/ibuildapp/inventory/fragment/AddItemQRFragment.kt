package com.ibuildapp.inventory.fragment

import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.google.zxing.ResultPoint
import com.ibuildapp.inventory.QRAddItemActivity
import com.ibuildapp.inventory.R
import com.ibuildapp.inventory.databinding.InventoryFragmentAddItemQrBinding
import com.ibuildapp.inventory.stuff.Utils
import com.journeyapps.barcodescanner.BarcodeCallback
import com.journeyapps.barcodescanner.BarcodeResult
import com.journeyapps.barcodescanner.CaptureManager
import kotlin.properties.Delegates

class AddItemQRFragment : Fragment() {
    private lateinit var binding: InventoryFragmentAddItemQrBinding
    private lateinit var capture: CaptureManager
    private var isTorchOn = false

    private var torchTopOffset by Delegates.notNull<Int>()
    private var torchEndOffset by Delegates.notNull<Int>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = InventoryFragmentAddItemQrBinding.inflate(inflater)

        initViews()

        return binding.root
    }

    private fun initViews() {
        torchTopOffset = Utils.dpToPixels(requireContext(), 56) + Utils.getStatusBarHeight(requireContext())
        torchEndOffset = Utils.dpToPixels(requireContext(), 8)

        val status = binding.inventoryQrLayout.findViewById<TextView>(R.id.zxing_status_view)
        status?.let { it.visibility = View.GONE }

        (binding.ivFlashLight.layoutParams as ConstraintLayout.LayoutParams).setMargins(0, torchTopOffset, torchEndOffset, 0)
        capture = CaptureManager(activity, binding.inventoryQrLayout)
        capture.decode()

        binding.inventoryQrLayout.decodeSingle(object: BarcodeCallback{
            override fun barcodeResult(result: BarcodeResult?) {
                result?.text?.let {
                    (activity as QRAddItemActivity).startAddItem (it, result.barcodeFormat)
                }
            }

            override fun possibleResultPoints(resultPoints: MutableList<ResultPoint>?) {
            }

        })
        processTorch()

        binding.ivFlashLight.setOnClickListener {
            isTorchOn = !isTorchOn
            processTorch()
        }
    }

    private fun processTorch() {
        if (isTorchOn) {
            binding.inventoryQrLayout.setTorchOn()
            binding.ivFlashLight.setImageResource(R.drawable.svg_inventory_highlight)
        } else {
            binding.inventoryQrLayout.setTorchOff()
            binding.ivFlashLight.setImageResource(R.drawable.svg_inventory_outline_highlight)
        }
    }

    override fun onResume() {
        super.onResume()
        capture.onResume()
    }

    override fun onPause() {
        super.onPause()
        capture.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        capture.onDestroy()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        capture.onSaveInstanceState(outState)
    }

//    override fun onSupportNavigateUp(): Boolean {
//        onBackPressed()
//        return true
//    }
//
//    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
//        return binding.inventoryQrLayout.onKeyDown(keyCode, event) || super.onKeyDown(keyCode, event)
//    }
}