package com.ibuildapp.inventory.fragment

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ibuildapp.inventory.LoginActivity
import com.ibuildapp.inventory.R
import com.ibuildapp.inventory.database.InventoryDatabase
import com.ibuildapp.inventory.databinding.InventoryFragmentProfileBinding
import com.ibuildapp.inventory.stuff.BaseSessionManager
import com.ibuildapp.inventory.viewmodel.ProfileViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class ProfileFragment : BaseProfileFragment() {
    private lateinit var binding: InventoryFragmentProfileBinding

    private val viewModel: ProfileViewModel by lazy {
        val factory = ProfileViewModel.ViewModelFactory(activity!!.application)
        ViewModelProviders.of(this, factory).get(ProfileViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = InventoryFragmentProfileBinding.inflate(inflater)

        initViews()
        initViewModel()

        return binding.root
    }

    private fun initViewModel() {
        viewModel.getViewData().observe(viewLifecycleOwner, Observer {
            val viewData = it ?: return@Observer

            binding.tvEmail.text = viewData.email
            binding.tvName.text = viewData.fullName
            binding.tvCounts.text = viewData.percent

            binding.tvEntriesCreated.text = viewData.itemsCount
            binding.tvPlanName.text = viewData.planName
            binding.tvEntriesRemain.text = viewData.remainItems

            binding.vButtonInfo.text = viewData.buttonName
            binding.vUpgradeBackground.tag = viewData.buttonLink

            binding.pbCounts.progress = viewData.percentInt
        })
    }

    private fun initViews() {
        inventoryActivity?.let {
            it.setSupportActionBar(it.getToolbar())
            it.getToolbar().title = getString(R.string.inventory_profile_title)
        }

        inventoryActivity?.supportActionBar?.let { actionBar ->
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2)
                actionBar.setHomeAsUpIndicator(R.drawable.svg_inventory_arrow_back)

            actionBar.setHomeButtonEnabled(true)
            actionBar.setDisplayHomeAsUpEnabled(true)
        }

        binding.vUpgradeBackground.setOnClickListener {
            //Toast.makeText(requireContext(), "Открытие странички с прокачкой плана:"+ binding.vUpgradeBackground.tag, Toast.LENGTH_SHORT).show()
            openWebPage(binding.vUpgradeBackground.tag.toString(), this.context!!)
        }

        binding.vLogoutBackground.setOnClickListener {
            val dialogClickListener = DialogInterface.OnClickListener { _: DialogInterface?, which: Int ->
                when (which) {
                    DialogInterface.BUTTON_POSITIVE -> processExit()
                    DialogInterface.BUTTON_NEGATIVE -> {
                    }
                }
            }

            val builder: AlertDialog.Builder = AlertDialog.Builder(requireContext())


            builder.setMessage(R.string.inventory_logout_dialog_title)
                    .setPositiveButton(R.string.inventory_yes, dialogClickListener)
                    .setNegativeButton(R.string.inventory_no, dialogClickListener)
                    .show()
        }

        runBlocking(Dispatchers.IO) {
            GlobalScope.launch {
                viewModel.loadUserInfo()
            }
        }
    }

    private fun openWebPage(urls: String, mContext : Context) {
        val uris = Uri.parse(urls)
        val intents = Intent(Intent.ACTION_VIEW, uris)
        val b = Bundle()
        b.putBoolean("new_window", true)
        intents.putExtras(b)
        mContext.startActivity(intents)
    }

    private fun processExit() {
        inventoryActivity?.let {

            InventoryDatabase.clearInstance()

            if (BaseSessionManager.INSTANCE.getRememberMe(requireContext())){
                BaseSessionManager.INSTANCE.clearOnlyUser(requireContext())
            } else {
                BaseSessionManager.INSTANCE.clearAllUser(requireContext())
            }
            val mainIntent = Intent(requireContext(), LoginActivity::class.java)
            startActivity(mainIntent, Bundle())
            it.finish()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        inventoryActivity?.let {
            it.getToolbar().setNavigationOnClickListener { inventoryActivity!!.onBackPressed() }
        }
    }
}