package com.ibuildapp.inventory.fragment

import android.app.Activity
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.annotation.NonNull
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.navigation.Navigation
import com.ibuildapp.inventory.AddDataActivity
import com.ibuildapp.inventory.R
import com.ibuildapp.inventory.adapter.SearchItemsAdapter
import com.ibuildapp.inventory.callback.ItemsCallback
import com.ibuildapp.inventory.databinding.InventoryFragmentItemsSearchBinding
import com.ibuildapp.inventory.stuff.LaunchUtils
import com.ibuildapp.inventory.stuff.SimpleTextWatcher
import com.ibuildapp.inventory.viewmodel.SearchItemsViewModel
import kotlinx.coroutines.*

class SearchItemsFragment: BaseItemsFragment(), ItemsCallback {
    private lateinit var binding: InventoryFragmentItemsSearchBinding

    private var filter: String = ""
    private var parentItemId: String? = null

    private var menu: Menu? = null

    private val viewModel: SearchItemsViewModel by lazy {
        val factory = SearchItemsViewModel.ViewModelFactory(activity!!.application, parentItemId = parentItemId)
        ViewModelProviders.of(this, factory).get(SearchItemsViewModel::class.java)
    }

    private val searchAdapter: SearchItemsAdapter by lazy { SearchItemsAdapter(this) }
    private val mainScope = MainScope()

    override fun onProcessArguments(arguments: Bundle) {
        parentItemId = arguments.getString(LaunchUtils.parentId)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = InventoryFragmentItemsSearchBinding.inflate(inflater)

        initViews()
        initViewModel()

        return binding.root
    }

    private fun initViewModel() {
        viewModel.getData().observe(viewLifecycleOwner, Observer {
            if (it.isNullOrEmpty()){
                if (viewModel.getFilterString().isEmpty() || viewModel.getFilterString().length <= 2){
                    binding.gEmpty.visibility = View.GONE
                    binding.gNoFilter.visibility = View.VISIBLE
                } else {
                    binding.gEmpty.visibility = View.VISIBLE
                    binding.gNoFilter.visibility = View.GONE
                }
            } else {
                binding.gEmpty.visibility = View.GONE
                binding.gNoFilter.visibility = View.GONE
            }

            searchAdapter.submitList(it)
        })
    }

    override fun hideKeyBoardOnStart(): Boolean = false

    private fun initViews() {
        inventoryActivity?.let {
            it.setSupportActionBar(it.getToolbar())
            it.getToolbar().title = getString(R.string.inventory_search)

            it.getToolbar().setOnMenuItemClickListener { menuItem ->
                if (menuItem.itemId == R.id.action_sub_folder){
                    menu?.findItem(R.id.action_sub_folder)?.isChecked = true
                    viewModel.updateIsFolder(false)
                } else if (menuItem.itemId == R.id.action_folder){
                    menu?.findItem(R.id.action_folder)?.isChecked = true
                    viewModel.updateIsFolder(true)
                }

                true
            }
        }

        inventoryActivity?.supportActionBar?.let { actionBar ->
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2)
                actionBar.setHomeAsUpIndicator(R.drawable.svg_inventory_close)

            actionBar.setHomeButtonEnabled(true)
            actionBar.setDisplayHomeAsUpEnabled(true)
        }

        binding.etSearch.addTextChangedListener(object : SimpleTextWatcher() {
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                processFilter(s)
            }
        })

        setHasOptionsMenu(true)
        binding.ivClear.setOnClickListener { binding.etSearch.setText("") }

        binding.rvItems.layoutManager = LinearLayoutManager(requireContext())
        binding.rvItems.adapter = searchAdapter

        binding.srlRefresh.setOnRefreshListener {
            runBlocking {
                mainScope.launch {
                    binding.srlRefresh.isRefreshing = true

                    launch(Dispatchers.IO){
                        viewModel.loadDataFromNetwork()

                        mainScope.launch {
                            binding.srlRefresh.isRefreshing = false
                        }
                    }
                }
            }
        }

        binding.srlRefresh.setColorSchemeColors(ContextCompat.getColor(requireContext(), R.color.colorAccent))
    }

    override fun onCreateOptionsMenu(@NonNull menu: Menu?, @NonNull inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.inventory_search_menu, menu)
        this.menu = menu

        if (viewModel.isFolderSearch())
            menu?.findItem(R.id.action_folder)?.isChecked = true
        else
            menu?.findItem(R.id.action_sub_folder)?.isChecked = true
    }

    private fun processFilter(text: CharSequence) {
        filter = text.toString()
        binding.ivClear.visibility = if (filter.isEmpty()) View.GONE else View.VISIBLE
        viewModel.updateDataWithFilter(filter)
    }

    private fun showSoftKeyboard(view: View) {
        val inputMethodManager: InputMethodManager = activity!!.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        view.requestFocus()
        inputMethodManager.showSoftInput(view, 0)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        inventoryActivity?.let {
            it.getToolbar().setNavigationOnClickListener { onBackPressed() }
        }

        showSoftKeyboard(binding.etSearch)
    }

    override fun onFolderClick(id: String, name: String) {
        val bundle = Bundle().apply {
            putString(LaunchUtils.title, name)
            putString(LaunchUtils.parentId, id)
        }

        view?.let {
            Navigation.findNavController(it).navigate(R.id.actions_search_to_items, bundle)
        }
    }

    override fun onSearchClick() {

    }

    override fun onEditFolderClick(id: String) {
        Toast.makeText(requireContext(), "EDIT", Toast.LENGTH_SHORT).show()
    }

    override fun onDeleteFolderClick(id: String, isFolder: Boolean) {
        showDeleteItemDialog(id, true)
    }

    override fun onItemClick(id: String, name: String) {
        val intent = Intent(activity, AddDataActivity::class.java).apply {
            putExtra(LaunchUtils.isAdd, false)
            putExtra(LaunchUtils.isFolder, false)
            putExtra(LaunchUtils.itemId, id)
        }

        startActivity(intent)
    }

    override fun deleteItemClick(id: String) {
        showDeleteItemDialog(id, false)
    }

    override fun deleteItem(id: String) {
        runBlocking {
            mainScope.launch {
                binding.srlRefresh.isRefreshing = true

                launch(Dispatchers.IO){
                    viewModel.removeItem(id)

                    mainScope.launch {
                        binding.srlRefresh.isRefreshing = false
                    }
                }
            }
        }
    }

    override fun deleteFolder(id: String) {
        runBlocking {
            mainScope.launch {
                binding.srlRefresh.isRefreshing = true

                launch(Dispatchers.IO){
                    viewModel.removeFolder(id)

                    mainScope.launch {
                        binding.srlRefresh.isRefreshing = false
                    }
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        mainScope.cancel()
    }
}