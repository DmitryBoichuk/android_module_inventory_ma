package com.ibuildapp.inventory.fragment

import android.content.DialogInterface
import android.support.v7.app.AlertDialog
import com.ibuildapp.inventory.InventoryActivity
import com.ibuildapp.inventory.R

abstract class BaseItemsFragment: BaseFragment() {
    protected val inventoryActivity: InventoryActivity?
        get() = activity as InventoryActivity?

    override fun onStart() {
        super.onStart()

        inventoryActivity?.setInternalOnBackPressedListener(this)
    }

    fun showDeleteItemDialog(id: String, isFolder: Boolean) {
        val dialogClickListener = DialogInterface.OnClickListener { _: DialogInterface?, which: Int ->
            when (which) {
                DialogInterface.BUTTON_POSITIVE -> if (isFolder) deleteFolder(id) else deleteItem(id)
                DialogInterface.BUTTON_NEGATIVE -> {
                }
            }
        }

        val builder: AlertDialog.Builder = AlertDialog.Builder(requireContext())


        builder.setMessage(if (isFolder) R.string.inventory_delete_folder else R.string.inventory_delete_item)
                .setPositiveButton(R.string.inventory_yes, dialogClickListener)
                .setNegativeButton(R.string.inventory_no, dialogClickListener)
                .show()
    }

    abstract fun deleteItem(id: String)

    abstract fun deleteFolder(id: String)
}