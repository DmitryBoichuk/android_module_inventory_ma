package com.ibuildapp.inventory.fragment

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.Navigation
import com.ibuildapp.inventory.InventoryActivity
import com.ibuildapp.inventory.LoginActivity
import com.ibuildapp.inventory.R
import com.ibuildapp.inventory.databinding.InventoryFragmentRegistrationBinding
import com.ibuildapp.inventory.model.Result
import com.ibuildapp.inventory.stuff.KeyboardUtils
import com.ibuildapp.inventory.stuff.LoginUtils
import com.ibuildapp.inventory.stuff.afterTextChanged
import com.ibuildapp.inventory.viewmodel.RegistrationViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import rx.android.schedulers.AndroidSchedulers

class RegistrationFragment: BaseFragment() {
    private lateinit var binding: InventoryFragmentRegistrationBinding
    private val viewModel: RegistrationViewModel by lazy { ViewModelProviders.of(this).get(RegistrationViewModel::class.java) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = InventoryFragmentRegistrationBinding.inflate(inflater, container, false)

        initViews()
        return binding.root
    }

    private fun initViews() {
        binding.toolbar.setNavigationIcon(R.drawable.svg_inventory_arrow_back)
        binding.toolbar.title = ""

        (activity as LoginActivity).let {
            it.setSupportActionBar(binding.toolbar)
            it.title = getString(R.string.inventory_sign_up)

            it.supportActionBar?.let { actionBar ->
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2)
                    actionBar.setHomeAsUpIndicator(R.drawable.svg_inventory_arrow_back)

                actionBar.setHomeButtonEnabled(true)
                actionBar.setDisplayHomeAsUpEnabled(true)
            }
        }

        viewModel.getData().observe(viewLifecycleOwner, Observer {
            it?.let {
                when (it.result) {
                    Result.LOADING -> AndroidSchedulers.mainThread().createWorker().schedule { showProgress() }
                    Result.SUCCESS -> {
                        hideProgress()
                        startMainActivity()
                    }
                    else -> {
                        hideProgress()
                        Toast.makeText(requireContext(), it.error, Toast.LENGTH_LONG).show()
                    }
                }
            }
        })

        binding.etFirstName.afterTextChanged { processRegistration(
                name = it,
                lastName = binding.etLastName.editableText.toString(),
                email = binding.etEmail.editableText.toString(),
                password = binding.etPassword.editableText.toString(),
                passwordConfirm = binding.etPasswordConfirm.editableText.toString()
        )}

        binding.etLastName.afterTextChanged { processRegistration(
                name = binding.etFirstName.editableText.toString(),
                lastName = it,
                email = binding.etEmail.editableText.toString(),
                password = binding.etPassword.editableText.toString(),
                passwordConfirm = binding.etPasswordConfirm.editableText.toString()
        )}

        binding.etEmail.afterTextChanged { processRegistration(
                name = binding.etFirstName.editableText.toString(),
                lastName = binding.etLastName.editableText.toString(),
                email = it,
                password = binding.etPassword.editableText.toString(),
                passwordConfirm = binding.etPasswordConfirm.editableText.toString()
        )}

        binding.etPassword.afterTextChanged { processRegistration(
                name = binding.etFirstName.editableText.toString(),
                email = binding.etEmail.editableText.toString(),
                lastName = binding.etLastName.editableText.toString(),
                password = it,
                passwordConfirm = binding.etPasswordConfirm.editableText.toString()
        )}

        binding.etPasswordConfirm.afterTextChanged { processRegistration(
                name = binding.etFirstName.editableText.toString(),
                email = binding.etEmail.editableText.toString(),
                lastName = binding.etLastName.editableText.toString(),
                password = binding.etPassword.editableText.toString(),
                passwordConfirm = it
        )}

        binding.btRegistration.setOnClickListener {
            KeyboardUtils().hideKeyboard(activity)

            runBlocking(Dispatchers.IO) {
                GlobalScope.launch {
                    viewModel.register(
                            name = binding.etFirstName.editableText.toString(),
                            lastName = binding.etLastName.editableText.toString(),
                            email = binding.etEmail.editableText.toString(),
                            password = binding.etPassword.editableText.toString())
                }
            }
        }
    }

    private fun processRegistration(name: String, lastName: String, email: String, password: String, passwordConfirm: String) {
        binding.btRegistration.isEnabled = LoginUtils.isLoginValid(name) && LoginUtils.isLoginValid(lastName) && LoginUtils.isEmailValid(email)
                && LoginUtils.isPasswordValid(password) && LoginUtils.isPasswordValid(passwordConfirm)
                && password == passwordConfirm
    }

    private fun startMainActivity() {
        activity?.let {activity->
            val intent = Intent(activity, InventoryActivity::class.java)
            activity.overridePendingTransition(0, 0)

            activity.startActivity(intent)

            activity.overridePendingTransition(0, 0)
            activity.finish()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.toolbar.setNavigationOnClickListener { Navigation.findNavController(view).popBackStack() }
    }
}