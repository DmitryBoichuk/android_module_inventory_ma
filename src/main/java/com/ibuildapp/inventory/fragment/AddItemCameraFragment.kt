package com.ibuildapp.inventory.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.camerakit.CameraKitView.*
import com.ibuildapp.inventory.databinding.InventoryFragmentAddItemCameraBinding
import com.jpegkit.Jpeg
import kotlinx.android.synthetic.main.inventory_fragment_add_item_camera.*

class AddItemCameraFragment : Fragment() {
    private lateinit var binding: InventoryFragmentAddItemCameraBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = InventoryFragmentAddItemCameraBinding.inflate(inflater)

        initViews()

        return binding.root
    }

    private fun initViews() {
        binding.camera.setCameraListener(object : CameraListener {
            override fun onOpened() {
                Log.v("CameraKitView", "CameraListener: onOpened()")
            }

            override fun onClosed() {
                Log.v("CameraKitView", "CameraListener: onClosed()")
            }
        })

        binding.camera.setPreviewListener(object : PreviewListener {
            override fun onStart() {
                Log.v("CameraKitView", "PreviewListener: onStart()")
//                updateInfoText()
            }

            override fun onStop() {
                Log.v("CameraKitView", "PreviewListener: onStop()")
            }
        })

        binding.vCameraButton.setOnClickListener { processImage() }
    }

    private fun processImage() {
        camera.captureImage { _, photo ->
            Thread(Runnable {
                val jpeg = Jpeg(photo)
                imageView.post { imageView.setJpeg(jpeg) }
            }).start()
        }
    }

    override fun onStart() {
        super.onStart()
        binding.camera.onStart()
    }

    override fun onResume() {
        super.onResume()
        binding.camera.onResume()
    }

    override fun onPause() {
        binding.camera.onPause()
        super.onPause()
    }

    override fun onStop() {
        binding.camera.onStop()
        super.onStop()
    }
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String?>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        binding.camera.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }
}