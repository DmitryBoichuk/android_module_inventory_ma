package com.ibuildapp.inventory.fragment

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.zxing.BarcodeFormat
import com.ibuildapp.inventory.AddDataActivity
import com.ibuildapp.inventory.R
import com.ibuildapp.inventory.adapter.ImagesAdapter
import com.ibuildapp.inventory.backend.model.Product
import com.ibuildapp.inventory.callback.ImagesCallback
import com.ibuildapp.inventory.databinding.InventoryFragmentShowQrDataBinding
import com.ibuildapp.inventory.model.ViewImageItem
import com.ibuildapp.inventory.model.ViewImageType
import com.ibuildapp.inventory.stuff.LaunchUtils

class ShowQRDataFragment: BaseCreateQRItemFragment(), ImagesCallback {
    private lateinit var binding: InventoryFragmentShowQrDataBinding
    private val imagesAdapter: ImagesAdapter by lazy { ImagesAdapter(this) }

    private lateinit var scannedText: String
    private lateinit var scannedFormat: BarcodeFormat

    private lateinit var parentItemId: String
    private lateinit var product: Product

    override fun onProcessArguments(arguments: Bundle) {
        scannedText = arguments.getString(LaunchUtils.scannedText) ?: return
        scannedFormat = arguments.getSerializable(LaunchUtils.scannedFormat) as BarcodeFormat
        parentItemId = arguments.getString(LaunchUtils.parentId) ?: return
        product = arguments.getSerializable(LaunchUtils.data) as Product
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = InventoryFragmentShowQrDataBinding.inflate(inflater)

        initViews()
        initViewModel()

        return binding.root
    }

    private fun initViewModel() {
    }

    private fun initViews() {
        createQRActivity?.apply {
            setSupportActionBar(getToolbar())
            getToolbar().title = getString(R.string.inventory_title_product_info)
        }

        createQRActivity?.supportActionBar?.let { actionBar ->
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2)
                actionBar.setHomeAsUpIndicator(R.drawable.svg_inventory_close)

            actionBar.setHomeButtonEnabled(true)
            actionBar.setDisplayHomeAsUpEnabled(true)
        }

        binding.vpImages.setAdapter(imagesAdapter)

        binding.vCancelBackground.setOnClickListener { onCancelClick() }
        binding.vUseInfoBackground.setOnClickListener { onUseInfoClick() }
    }

    private fun onUseInfoClick() {
        createQRActivity?.finish()

        val parentId =  arguments?.getString(LaunchUtils.parentId) ?: ""

        val intent = Intent(activity, AddDataActivity::class.java)
        intent.putExtra(LaunchUtils.isAdd, true)
        intent.putExtra(LaunchUtils.isFolder, false)
        intent.putExtra(LaunchUtils.parentId, parentId)
        intent.putExtra(LaunchUtils.scannedText, scannedText)
        intent.putExtra(LaunchUtils.scannedFormat, scannedFormat)
        intent.putExtra(LaunchUtils.data, product)

        startActivity(intent)
    }

    private fun onCancelClick() {
        createQRActivity?.finish()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        createQRActivity?.apply {
            getToolbar().setNavigationOnClickListener { onBackPressed() }
            hideSkip()
        }

        if (product.images.isNullOrEmpty()){
            binding.vpImages.visibility = View.GONE
            binding.tlDots.visibility = View.GONE
        } else {
            product.images?.let { images ->
                binding.vpImages.visibility = View.VISIBLE
                val imageItems = mutableListOf<ViewImageItem>()

                images.forEach {imageUrl ->
                    binding.tlDots.addTab(binding.tlDots.newTab())
                    imageItems.add(ViewImageItem(url = imageUrl, type = ViewImageType.ITEM))
                }

                if (images.size > 1){
                    binding.tlDots.visibility = View.VISIBLE
                    binding.vpImages.setListener { binding.tlDots.getTabAt(it)?.select() }
                } else
                    binding.tlDots.visibility = View.GONE

                imagesAdapter.submitList(imageItems)
            }
        }

        binding.tvName.text = product.productName
        binding.tvNote.text = product.description
    }

    override fun onBackPressed(): Boolean {
        createQRActivity?.finish()
        return true
    }

    override fun onAddClick() {
        TODO("Not yet implemented")
    }
}