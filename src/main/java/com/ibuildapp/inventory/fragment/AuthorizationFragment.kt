package com.ibuildapp.inventory.fragment

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.Navigation
import com.ibuildapp.inventory.InventoryActivity
import com.ibuildapp.inventory.LoginActivity
import com.ibuildapp.inventory.R
import com.ibuildapp.inventory.databinding.InventoryFragmentAuthorizationBinding
import com.ibuildapp.inventory.model.Result
import com.ibuildapp.inventory.stuff.BaseSessionManager
import com.ibuildapp.inventory.stuff.KeyboardUtils
import com.ibuildapp.inventory.stuff.LoginUtils
import com.ibuildapp.inventory.stuff.afterTextChanged
import com.ibuildapp.inventory.viewmodel.AuthorizationViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import rx.android.schedulers.AndroidSchedulers

class AuthorizationFragment : BaseFragment() {
    private lateinit var binding: InventoryFragmentAuthorizationBinding
    private val viewModel: AuthorizationViewModel by lazy { ViewModelProviders.of(this).get(AuthorizationViewModel::class.java) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = InventoryFragmentAuthorizationBinding.inflate(inflater, container, false)

        initViews()
        return binding.root
    }

    private fun initViews() {
        (activity as LoginActivity).let {
            it.setSupportActionBar(binding.toolbar)
            it.title = getString(R.string.inventory_sign_in)
        }

        activity?.actionBar?.let { actionBar ->
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2)
                actionBar.setHomeAsUpIndicator(null)

            actionBar.setHomeButtonEnabled(false)
            actionBar.setDisplayShowTitleEnabled(false)
        }

        viewModel.getData().observe(viewLifecycleOwner, Observer {
            println("-TT " + it?.result)
            it?.let {
                when (it.result) {
                    Result.LOADING -> AndroidSchedulers.mainThread().createWorker().schedule { showProgress() }
                    Result.SUCCESS -> {
                        hideProgress()
                        startMainActivity()
                    }
                    else -> {
                        hideProgress()
                        val text = it.error ?: getString(R.string.inventory_invalid_login_or_password)
                        Toast.makeText(requireContext(), text, Toast.LENGTH_LONG).show()
                    }
                }
            }
        })

        binding.etLogin.afterTextChanged { processAuthorization(login = it, password = binding.etPassword.editableText.toString())}
        binding.etPassword.afterTextChanged { processAuthorization(password = it, login = binding.etLogin.editableText.toString())}
        binding.tvNewUser.setOnClickListener { processNewUser() }

        if (BaseSessionManager.INSTANCE.getRememberMe(requireContext())) {
            binding.etLogin.setText(viewModel.getLogin() ?: "")
            binding.etPassword.setText(viewModel.getPassword() ?: "")

            processAuthorization(binding.etLogin.editableText.toString(), binding.etPassword.editableText.toString())

            binding.cbRemember.isChecked = viewModel.isChecked()
        }

        binding.btLogin.setOnClickListener {
            KeyboardUtils().hideKeyboard(activity)

            runBlocking(Dispatchers.IO) {
                GlobalScope.launch {
                    viewModel.login(binding.etLogin.editableText.toString(), binding.etPassword.editableText.toString(), binding.cbRemember.isChecked)
                }
            }
        }
    }

    private fun processNewUser() = Navigation.findNavController(binding.root).navigate(R.id.action_authorization_to_registration)

    private fun startMainActivity() {
        activity?.let {activity->
            val intent = Intent(activity, InventoryActivity::class.java)

            activity. overridePendingTransition(0, 0)
            activity.startActivity(intent)
            activity.overridePendingTransition(0, 0)
            activity.finish()
        }
    }

    private fun processAuthorization(login: String, password: String) {
        binding.btLogin.isEnabled = LoginUtils.isEmailValid(login) && LoginUtils.isPasswordValid(password)
    }
}
