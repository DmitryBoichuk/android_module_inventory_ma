package com.ibuildapp.inventory.fragment

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.annotation.NonNull
import android.support.annotation.Nullable
import android.support.design.widget.BottomSheetDialogFragment
import android.support.v4.content.ContextCompat.checkSelfPermission
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.ibuildapp.inventory.AddDataActivity
import com.ibuildapp.inventory.QRAddItemActivity
import com.ibuildapp.inventory.R
import com.ibuildapp.inventory.databinding.InventoryFragmentChooseCreationBinding
import com.ibuildapp.inventory.stuff.LaunchUtils


class ChooseCreationFragment: BottomSheetDialogFragment() {
    private lateinit var binding: InventoryFragmentChooseCreationBinding

    override fun onCreate(@Nullable savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.CustomBottomSheetDialogTheme)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        binding = InventoryFragmentChooseCreationBinding.inflate(inflater)
        initView()
        return binding.root
    }

    private fun initView() {
        binding.fabClose.setOnClickListener { dismiss() }

        binding.vFolderClick.setOnClickListener { startAddFolderActivity() }

        binding.vFileClick.setOnClickListener { startAddItemActivity() }
    }

    private fun startAddFolderActivity() {
        dismiss()

        val parentId =  arguments?.getString(LaunchUtils.parentId) ?: ""

        val intent = Intent(activity, AddDataActivity::class.java)
        intent.putExtra(LaunchUtils.isAdd, true)
        intent.putExtra(LaunchUtils.isFolder, true)
        intent.putExtra(LaunchUtils.parentId, parentId)

        startActivity(intent)
    }

    private fun startAddItemActivity() {
        if (checkSelfPermission(requireContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
            requestPermissions(arrayOf(Manifest.permission.CAMERA), LaunchUtils.cameraRequest)
        else
            processCreateItem()
    }

    private fun processCreateItem() {
        dismiss()

        val parentId =  arguments?.getString(LaunchUtils.parentId) ?: ""

        val intent = Intent(requireContext(), QRAddItemActivity::class.java)
        intent.putExtra(LaunchUtils.parentId, parentId)
        startActivity(intent)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == LaunchUtils.cameraRequest) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                processCreateItem()
            else
                Toast.makeText(requireContext(), "camera permission denied", Toast.LENGTH_LONG).show()
        }
    }
}