package com.ibuildapp.inventory.fragment

import android.arch.lifecycle.Observer
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import androidx.navigation.Navigation
import com.google.zxing.BarcodeFormat
import com.ibuildapp.inventory.R
import com.ibuildapp.inventory.databinding.InventoryFragmentAddItemBinding
import com.ibuildapp.inventory.model.ViewImageItem
import com.ibuildapp.inventory.stuff.KeyboardUtils
import com.ibuildapp.inventory.stuff.LaunchUtils
import com.ibuildapp.inventory.stuff.SimpleTextWatcher
import com.journeyapps.barcodescanner.BarcodeEncoder
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class AddItemFragment: BaseAddDataFragment() {
    private lateinit var binding: InventoryFragmentAddItemBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = InventoryFragmentAddItemBinding.inflate(inflater)

        initViews()
        initViewModel()

        return binding.root
    }

    private fun initViewModel() {
        viewModel.getData().observe(viewLifecycleOwner, Observer {
            val item = it ?: return@Observer

            updateImages(item.imageUrl)

            if (it.id != null) {
                binding.tvId.text = "ID: ${it.id}"
                binding.tvId.visibility = View.VISIBLE
            } else binding.tvId.visibility = View.GONE

            item.name?.let {
                binding.etName.append(item.name)
            }

            item.notes?.let {
                binding.etNote.append(item.notes)
            }

            item.sku?.let {
                binding.etSku.append(item.sku)
            }

            item.category?.let {
                binding.etCategory.append(item.category)
            }

            item.quantity?.let { quantity -> binding.etQuantity.append(quantity.toString()) }
            item.price?.let { binding.etPrice.append(item.price.toString()) }

            if (item.barcode != null && item.barcodeFormat != null)
                showBarcode(item.barcode!!, item.barcodeFormat!!)
            else hideBarcode()
        })
    }

    private fun initViews() {
        addDataActivity?.supportActionBar?.let { actionBar ->
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2)
                actionBar.setHomeAsUpIndicator(R.drawable.svg_inventory_close)

            actionBar.setHomeButtonEnabled(true)
            actionBar.setDisplayHomeAsUpEnabled(true)
        }

        binding.etName.imeOptions = EditorInfo.IME_ACTION_DONE
        binding.etName.setRawInputType(InputType.TYPE_CLASS_TEXT)

        binding.etNote.imeOptions = EditorInfo.IME_ACTION_DONE
        binding.etNote.setRawInputType(InputType.TYPE_CLASS_TEXT)

        binding.etCategory.imeOptions = EditorInfo.IME_ACTION_DONE
        binding.etCategory.setRawInputType(InputType.TYPE_CLASS_TEXT)

        binding.etSku.imeOptions = EditorInfo.IME_ACTION_DONE
        binding.etSku.setRawInputType(InputType.TYPE_CLASS_TEXT)

        addDataActivity?.let {
            it.setSupportActionBar(it.getToolbar())
            it.getToolbar().title = if (isAdd) getString(R.string.inventory_add_data_create) else getString(R.string.inventory_add_data_edit)
        }

        if(isAdd){
            binding.gBottom.visibility = View.VISIBLE
            binding.fabSave.hide()
        } else {
            binding.gBottom.visibility = View.GONE
            binding.fabSave.show()
        }

        binding.vpImages.setAdapter(imagesAdapter)

        binding.vQrBackground.setOnClickListener { startQRCodeActivity() }

        binding.ivQrClose.setOnClickListener { clearQROrBarcode() }

        binding.vSaveCloseClick.setOnClickListener { saveAndClose() }

        binding.fabSave.setOnClickListener { updateAndClose() }

        binding.vSaveNewClick.setOnClickListener { saveAndNew() }

        binding.etName.addTextChangedListener(object: SimpleTextWatcher() {
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                viewModel.nameChanged(s.toString())
            }
        })

        binding.etNote.addTextChangedListener(object: SimpleTextWatcher() {
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                viewModel.noteChanged(s.toString())
            }
        })

        binding.etCategory.addTextChangedListener(object: SimpleTextWatcher() {
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                viewModel.categoryChanged(s.toString())
            }
        })

        binding.etSku.addTextChangedListener(object: SimpleTextWatcher() {
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                viewModel.skuChanged(s.toString())
            }
        })

        binding.etPrice.addTextChangedListener(object: SimpleTextWatcher() {
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                viewModel.priceChanged(s.toString())
            }
        })

        binding.etQuantity.addTextChangedListener(object: SimpleTextWatcher() {
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                viewModel.quantityChanged(s.toString())
            }
        })
    }

    private fun hideBarcode() {
        if (binding.gQrButton.visibility == View.GONE)
            binding.gQrButton.visibility = View.VISIBLE

        binding.vQrImageBackground.visibility = View.GONE
        binding.tvScanResult.visibility = View.GONE
        binding.sQrBottom.visibility = View.GONE
        binding.ivQrClose.visibility = View.GONE

        binding.ivQrImage.visibility = View.GONE
        binding.ivBarcodeImage.visibility = View.GONE
    }

    private fun updateImages(imageUrl: List<ViewImageItem>?) {
        if (imageUrl.isNullOrEmpty()){
            binding.vpImages.visibility = View.GONE
            binding.tlDots.visibility = View.GONE
        } else {
            imageUrl.let { images ->
                binding.vpImages.visibility = View.VISIBLE

                if (images.size > 1){
                    binding.tlDots.visibility = View.VISIBLE

                    repeat(images.size) {
                        binding.tlDots.addTab(binding.tlDots.newTab())
                    }

                    binding.vpImages.setListener { binding.tlDots.getTabAt(it)?.select() }

                } else
                    binding.tlDots.visibility = View.GONE

                imagesAdapter.submitList(imageUrl)
            }
        }
    }

    private fun showBarcode(barcode: String, barcodeFormatString: String) {
        if (binding.gQrButton.visibility == View.VISIBLE)
            binding.gQrButton.visibility = View.GONE

        runBlocking {
            GlobalScope.launch (Dispatchers.IO) {
                try {
                    val barcodeEncoder = BarcodeEncoder()

                    val barcodeFormat = BarcodeFormat.valueOf(barcodeFormatString)
                    val height = if (barcodeFormat == BarcodeFormat.QR_CODE) 600 else 300

                    val bitmap = barcodeEncoder.encodeBitmap(barcode, barcodeFormat, 600, height)

                    mainScope.launch {
                        binding.vQrImageBackground.visibility = View.VISIBLE
                        binding.tvScanResult.visibility = View.VISIBLE
                        binding.sQrBottom.visibility = View.VISIBLE
                        binding.ivQrClose.visibility = View.VISIBLE

                        if (barcodeFormat == BarcodeFormat.QR_CODE){
                            binding.ivQrImage.visibility = View.VISIBLE
                            binding.ivBarcodeImage.visibility = View.GONE
                            binding.ivQrImage.setImageBitmap(bitmap)
                        } else {
                            println("-ttt end")
                            binding.ivQrImage.visibility = View.GONE
                            binding.ivBarcodeImage.visibility = View.VISIBLE
                            binding.ivBarcodeImage.setImageBitmap(bitmap)
                        }

                        binding.tvScanResult.text = barcode
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }
    }

    override fun onImageAdded(uriList: MutableList<Uri>) {
        if (uriList.isNullOrEmpty()) return
        viewModel.addImages(uriList)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        addDataActivity?.let {
            it.getToolbar().setNavigationOnClickListener { addDataActivity?.finish() }
        }
    }

    override fun processContent(contents: String, formatName: String) {
        if (binding.gQrButton.visibility == View.VISIBLE)
            binding.gQrButton.visibility = View.GONE

        runBlocking {
            GlobalScope.launch (Dispatchers.IO) {
                try {
                    val barcodeEncoder = BarcodeEncoder()

                    val barcodeFormat = BarcodeFormat.valueOf(formatName)
                    viewModel.updateBarcode(contents, barcodeFormat)

                    val height = if (barcodeFormat == BarcodeFormat.QR_CODE) 600 else 300

                    val bitmap = barcodeEncoder.encodeBitmap(contents, barcodeFormat, 600, height)

                    mainScope.launch {
                        binding.vQrImageBackground.visibility = View.VISIBLE
                        binding.tvScanResult.visibility = View.VISIBLE
                        binding.sQrBottom.visibility = View.VISIBLE
                        binding.ivQrClose.visibility = View.VISIBLE

                        if (barcodeFormat == BarcodeFormat.QR_CODE){
                            binding.ivQrImage.visibility = View.VISIBLE
                            binding.ivBarcodeImage.visibility = View.GONE
                            binding.ivQrImage.setImageBitmap(bitmap)
                        } else {
                            binding.ivQrImage.visibility = View.GONE
                            binding.ivBarcodeImage.visibility = View.VISIBLE
                            binding.ivBarcodeImage.setImageBitmap(bitmap)
                        }

                        binding.tvScanResult.text = contents
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }
    }

    private fun clearQROrBarcode() {
        viewModel.clearQROrBarcode()

        hideBarcode()
    }

    private fun saveAndClose() {
        KeyboardUtils().hideKeyboard(activity)

        runBlocking {
            mainScope.launch {
                showProgress()

                launch(Dispatchers.IO) {
                    try {
                        viewModel.saveFile(
                                name = binding.etName.editableText.toString(),
                                note = binding.etNote.editableText.toString(),
                                price = binding.etPrice.editableText.toString(),
                                quantity = binding.etQuantity.editableText.toString(),
                                category = binding.etCategory.editableText.toString(),
                                sku = binding.etSku.editableText.toString()
                        )

                        mainScope.launch {
                            hideProgress()
                            activity?.finish()
                        }
                    } catch (error: Exception) {
                        mainScope.launch {
                            view?.let {
                                hideProgress()
                                Toast.makeText(it.context, error.message, Toast.LENGTH_SHORT).show()
                            }
                        }
                    }
                }
            }
        }
    }

    private fun updateAndClose() {
        KeyboardUtils().hideKeyboard(activity)

        runBlocking {
            mainScope.launch {
                showProgress()

                launch(Dispatchers.IO) {
                    try {
                        viewModel.updateFile(
                                name = binding.etName.editableText.toString(),
                                note = binding.etNote.editableText.toString(),
                                price = binding.etPrice.editableText.toString(),
                                quantity = binding.etQuantity.editableText.toString(),
                                category = binding.etCategory.editableText.toString(),
                                sku = binding.etSku.editableText.toString()
                        )

                        mainScope.launch {
                            hideProgress()
                            activity?.finish()
                        }
                    } catch (error: Exception) {
                        mainScope.launch {
                            view?.let {
                                hideProgress()
                                Toast.makeText(it.context, error.message, Toast.LENGTH_SHORT).show()
                            }
                        }
                    }
                }
            }
        }
    }

    private fun saveAndNew() {
        KeyboardUtils().hideKeyboard(activity)

        runBlocking {
            mainScope.launch {
                showProgress()

                launch(Dispatchers.IO) {
                    try {
                        viewModel.saveFile(
                                name = binding.etName.editableText.toString(),
                                note = binding.etNote.editableText.toString(),
                                price = binding.etPrice.editableText.toString(),
                                quantity = binding.etQuantity.editableText.toString(),
                                category = binding.etCategory.editableText.toString(),
                                sku = binding.etSku.editableText.toString()
                        )

                        mainScope.launch {
                            hideProgress()
                            view?.let {
                                val arguments = Bundle().apply {
                                    putString(LaunchUtils.itemId, null)
                                    putString(LaunchUtils.parentId, parentItemId)
                                    putBoolean(LaunchUtils.isFolder, false)
                                    putBoolean(LaunchUtils.isAdd, true)
                                }
                                Navigation.findNavController(it).navigate(R.id.action_add_item_to_add_item, arguments)
                            }
                        }
                    } catch (error: Exception) {
                        mainScope.launch {
                            view?.let {
                                hideProgress()
                                Toast.makeText(it.context, error.message, Toast.LENGTH_SHORT).show()
                            }
                        }
                    }
                }
            }
        }
    }
}