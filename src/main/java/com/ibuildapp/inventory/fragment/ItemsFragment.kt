package com.ibuildapp.inventory.fragment

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.ibuildapp.inventory.AddDataActivity
import com.ibuildapp.inventory.R
import com.ibuildapp.inventory.adapter.ItemsAdapter
import com.ibuildapp.inventory.callback.ItemsCallback
import com.ibuildapp.inventory.databinding.InventoryFragmentItemsBinding
import com.ibuildapp.inventory.stuff.LaunchUtils
import com.ibuildapp.inventory.viewmodel.ItemsViewModel
import kotlinx.coroutines.*

class ItemsFragment : BaseItemsFragment(), ItemsCallback {
    private var bottomSheetFragment: ChooseCreationFragment?= null
    private lateinit var binding: InventoryFragmentItemsBinding

    private var parentItemId: String? = null
    private var title: String? = null

    private val mainScope = MainScope()

    private val viewModel: ItemsViewModel by lazy {
        val factory = ItemsViewModel.ViewModelFactory(activity!!.application, parentItemId = parentItemId)
        ViewModelProviders.of(this, factory).get(ItemsViewModel::class.java)
    }

    private val itemsAdapter: ItemsAdapter by lazy { ItemsAdapter(this) }

    override fun onProcessArguments(arguments: Bundle) {
        parentItemId = arguments.getString(LaunchUtils.parentId)
        title = arguments.getString(LaunchUtils.title)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = InventoryFragmentItemsBinding.inflate(inflater)

        initView()
        initViewModel()

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        inventoryActivity?.let {
            it.getToolbar().setNavigationOnClickListener { onBackPressed() }
        }
    }

    private fun initViewModel() {
        viewModel.getItems().observe(viewLifecycleOwner, Observer {
            if (it.isNullOrEmpty())
                binding.gEmpty.visibility = View.VISIBLE
            else binding.gEmpty.visibility = View.GONE
            itemsAdapter.submitList(it)
        })

        viewModel.getFabLiveData().observe(viewLifecycleOwner, Observer {
            val needFab = it ?: return@Observer

            if (needFab)
                binding.fabCreate.show()
            else binding.fabCreate.hide()
        })
    }

    private fun showChooseDialog() {
        val manager = fragmentManager

        bottomSheetFragment = ChooseCreationFragment().apply {
            arguments = Bundle().apply {
                putString(LaunchUtils.title, title)
                putString(LaunchUtils.parentId, parentItemId)
            }

            this.show(manager, "create_choose")
        }
    }

    private fun initView() {
        inventoryActivity?.let {
            it.setSupportActionBar(it.getToolbar())

            if (parentItemId.isNullOrEmpty())
                it.getToolbar().title = getString(R.string.inventory_items_title)
            else it.getToolbar().title = title
        }

        inventoryActivity?.supportActionBar?.let { actionBar ->
            if (parentItemId.isNullOrEmpty()) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2)
                    actionBar.setHomeAsUpIndicator(null)

                actionBar.setHomeButtonEnabled(false)
                actionBar.setDisplayHomeAsUpEnabled(false)
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2)
                    actionBar.setHomeAsUpIndicator(R.drawable.svg_inventory_arrow_back)

                actionBar.setHomeButtonEnabled(true)
                actionBar.setDisplayHomeAsUpEnabled(true)
            }
        }

        binding.rvItems.layoutManager = LinearLayoutManager(requireContext())
        binding.rvItems.adapter = itemsAdapter

        binding.fabCreate.setOnClickListener{ showChooseDialog() }

        binding.srlRefresh.setOnRefreshListener {
            runBlocking {
                mainScope.launch {
                    binding.srlRefresh.isRefreshing = true

                    launch(Dispatchers.IO){
                        viewModel.loadDataFromNetwork()

                        mainScope.launch {
                            binding.srlRefresh.isRefreshing = false
                        }
                    }
                }
            }
        }

        binding.srlRefresh.setColorSchemeColors(ContextCompat.getColor(requireContext(), R.color.colorAccent))
    }

    override fun onBackPressed(): Boolean {
        if (bottomSheetFragment != null && bottomSheetFragment!!.isVisible){
            bottomSheetFragment?.let {
                it.dismiss()
                it
            }

            bottomSheetFragment = null
            return true

        }
        return super.onBackPressed()
    }

    override fun onFolderClick(id: String, name: String) {
        val bundle = Bundle().apply {
            putString(LaunchUtils.title, name)
            putString(LaunchUtils.parentId, id)
        }

        view?.let {
            Navigation.findNavController(it).navigate(R.id.action_items_to_items, bundle)
        }
    }

    override fun onSearchClick() {
        view?.let {
            val bundle = Bundle().apply {
                putString(LaunchUtils.parentId, parentItemId)
            }

            Navigation.findNavController(it).navigate(R.id.action_items_to_search, bundle)
        }
    }

    override fun onEditFolderClick(id: String) {
        val intent = Intent(activity, AddDataActivity::class.java)
        intent.putExtra(LaunchUtils.isAdd, false)
        intent.putExtra(LaunchUtils.isFolder, true)
        intent.putExtra(LaunchUtils.itemId, id)

        startActivity(intent)
    }

    override fun onDeleteFolderClick(id: String, isFolder: Boolean) {
        showDeleteItemDialog(id, true)
    }

    override fun onItemClick(id: String, name: String) {
        val intent = Intent(activity, AddDataActivity::class.java).apply {
            putExtra(LaunchUtils.isAdd, false)
            putExtra(LaunchUtils.isFolder, false)
            putExtra(LaunchUtils.itemId, id)
        }

        startActivity(intent)
    }

    override fun deleteItemClick(id: String) {
        showDeleteItemDialog(id, false)
    }

    override fun deleteItem(id: String) {
        runBlocking {
            mainScope.launch {
                binding.srlRefresh.isRefreshing = true

                launch(Dispatchers.IO){
                    viewModel.removeItem(id)
                    viewModel.loadDataFromNetwork()

                    mainScope.launch {
                        binding.srlRefresh.isRefreshing = false
                    }
                }
            }
        }
    }

    override fun deleteFolder(id: String) {
        runBlocking {
            mainScope.launch {
                binding.srlRefresh.isRefreshing = true

                launch(Dispatchers.IO){
                    viewModel.removeFolder(id)
                    viewModel.loadDataFromNetwork()

                    mainScope.launch {
                        binding.srlRefresh.isRefreshing = false
                    }
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        mainScope.cancel()
    }
}