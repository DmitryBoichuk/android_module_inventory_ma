package com.ibuildapp.inventory

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import androidx.navigation.fragment.NavHostFragment
import com.ibuildapp.inventory.databinding.InventoryCreateItemFromQrBinding
import com.ibuildapp.inventory.stuff.LaunchUtils
import com.ibuildapp.inventory.stuff.OnBackListener

class CreateItemFromQRActivity: AppCompatActivity() {
    private lateinit var binding: InventoryCreateItemFromQrBinding
    private var internalOnBackPressedListener: OnBackListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = InventoryCreateItemFromQrBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        setSupportActionBar(binding.toolbar)

        supportFragmentManager?.findFragmentById(R.id.create_item_host_fragment)?.let {
            it as NavHostFragment
            val navController = it.navController

            navController.setGraph(R.navigation.create_item_nav_graph, intent.extras)
        }

        binding.tvToolbarSkip.setOnClickListener {
            startCreateItem()
        }
    }

    fun getToolbar() = binding.toolbar

    fun setInternalOnBackPressedListener(internalOnBackPressedListener: OnBackListener) {
        this.internalOnBackPressedListener = internalOnBackPressedListener
    }

    override fun onBackPressed() {
        if (internalOnBackPressedListener != null && internalOnBackPressedListener!!.onBackPressed())
            return

        finish()
    }

    fun showSkip() {
        binding.tvToolbarSkip.visibility = View.VISIBLE
    }

    fun hideSkip() {
        binding.tvToolbarSkip.visibility = View.GONE
    }

    private fun startCreateItem() {
        finish()

        val parentId =  intent.getStringExtra(LaunchUtils.parentId) ?: ""

        val intent = Intent(this, AddDataActivity::class.java)
        intent.putExtra(LaunchUtils.isAdd, true)
        intent.putExtra(LaunchUtils.isFolder, false)
        intent.putExtra(LaunchUtils.parentId, parentId)

        startActivity(intent)
    }

    fun reopenQRActivity() {
        finish()

        val parentId =  intent.getStringExtra(LaunchUtils.parentId) ?: ""

        val intent = Intent(this, QRAddItemActivity::class.java)
        intent.putExtra(LaunchUtils.parentId, parentId)
        startActivity(intent)
    }
}