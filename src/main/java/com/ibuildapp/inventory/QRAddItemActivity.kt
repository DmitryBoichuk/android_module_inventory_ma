package com.ibuildapp.inventory

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.design.widget.TabLayout
import android.support.v7.app.AppCompatActivity
import com.google.zxing.BarcodeFormat

import com.ibuildapp.inventory.databinding.InventoryQrAddItemBinding
import com.ibuildapp.inventory.fragment.AddItemCameraFragment
import com.ibuildapp.inventory.fragment.AddItemQRFragment
import com.ibuildapp.inventory.stuff.LaunchUtils
import com.ibuildapp.inventory.stuff.Utils

class QRAddItemActivity: AppCompatActivity() {
    private lateinit var binding: InventoryQrAddItemBinding

    private val addItemCameraFragment = AddItemCameraFragment()
    private val addItemQRFragment = AddItemQRFragment()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = InventoryQrAddItemBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        setSupportActionBar(binding.toolbar)

        supportActionBar?.let {actionBar ->
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2)
                actionBar.setHomeAsUpIndicator(R.drawable.svg_inventory_arrow_back_white)

            actionBar.title = ""
            actionBar.setHomeButtonEnabled(true)
            actionBar.setDisplayHomeAsUpEnabled(true)
        }

        binding.tvToolbarTitle.text = getString(R.string.inventory_tab_scan)

        (binding.toolbar.layoutParams as ConstraintLayout.LayoutParams)
                .setMargins(0, Utils.getStatusBarHeight(this), 0, 0)

        binding.tlTabs.addTab(binding.tlTabs.newTab().setText(getString(R.string.inventory_tab_camera)).setTag("first"))
        binding.tlTabs.addTab(binding.tlTabs.newTab().setText(getString(R.string.inventory_tab_scan)).setTag("second"))
        binding.tlTabs.isSelected = true

        supportFragmentManager.beginTransaction().replace(R.id.fl_fragment_container, addItemQRFragment).commit()

        binding.tlTabs.addOnTabSelectedListener(object: TabLayout.OnTabSelectedListener {
            override fun onTabReselected(p0: TabLayout.Tab?) {
            }

            override fun onTabUnselected(p0: TabLayout.Tab?) {
            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                tab?.let {
                    if (it.tag == "first") {
                        supportFragmentManager.beginTransaction().replace(R.id.fl_fragment_container, addItemCameraFragment).commit()
                        binding.tvToolbarTitle.text = getString(R.string.inventory_tab_camera)
                    } else {
                        supportFragmentManager.beginTransaction().replace(R.id.fl_fragment_container, addItemQRFragment).commit()
                        binding.tvToolbarTitle.text = getString(R.string.inventory_tab_scan)
                    }
                }
            }
        })

        binding.let {
            binding.toolbar.setNavigationOnClickListener { finish() }
        }

        binding.tvToolbarSkip.setOnClickListener { startCreateItem() }
    }

    private fun startCreateItem() {
        finish()

        val parentId =  intent.getStringExtra(LaunchUtils.parentId) ?: ""

        val intent = Intent(this, AddDataActivity::class.java)
        intent.putExtra(LaunchUtils.isAdd, true)
        intent.putExtra(LaunchUtils.isFolder, false)
        intent.putExtra(LaunchUtils.parentId, parentId)

        startActivity(intent)
    }

    fun startAddItem(scannedText: String, barcodeFormat: BarcodeFormat?) {
        finish()

        val intent = Intent(this, CreateItemFromQRActivity::class.java)
                .apply {
                    putExtra(LaunchUtils.parentId, intent.getStringExtra(LaunchUtils.parentId))
                    putExtra(LaunchUtils.scannedText, scannedText)
                    putExtra(LaunchUtils.scannedFormat, barcodeFormat)
                }

        startActivity(intent)
    }
}