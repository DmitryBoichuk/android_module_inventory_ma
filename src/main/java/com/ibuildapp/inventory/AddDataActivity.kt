package com.ibuildapp.inventory

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import androidx.navigation.fragment.NavHostFragment
import com.ibuildapp.inventory.databinding.InventoryAddDataBinding
import com.ibuildapp.inventory.stuff.LaunchUtils
import com.ibuildapp.inventory.stuff.OnBackListener

class AddDataActivity: AppCompatActivity() {
    private lateinit var binding: InventoryAddDataBinding
    private var isFolder: Boolean = false
    private var internalOnBackPressedListener: OnBackListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        isFolder = intent.getBooleanExtra(LaunchUtils.isFolder, false)

        binding = InventoryAddDataBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        setSupportActionBar(binding.toolbar)

        supportFragmentManager?.findFragmentById(R.id.add_data_host_fragment)?.let {
            val navHostFragment = it as NavHostFragment
            val navController = navHostFragment.navController

            if (isFolder)
                navController.setGraph(R.navigation.add_folder_nav_graph, intent.extras)
            else navController.setGraph(R.navigation.add_item_nav_graph, intent.extras)
        }
    }

    fun getToolbar() = binding.toolbar

    fun setInternalOnBackPressedListener(internalOnBackPressedListener: OnBackListener) {
        this.internalOnBackPressedListener = internalOnBackPressedListener
    }

    override fun onBackPressed() {
        if (internalOnBackPressedListener != null && internalOnBackPressedListener!!.onBackPressed())
            return

       finish()
    }
}