package com.ibuildapp.inventory.navigation

import android.os.Bundle
import android.support.annotation.AnimRes
import android.support.annotation.AnimatorRes
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import com.ibuildapp.inventory.R
import com.ibuildapp.inventory.fragment.BaseMainFragment
import java.util.*

class MainNavController(private val mFragmentManager: FragmentManager, private val containerId: Int) {
    private var backStack = LinkedList<NavItem>()
    private var mNavigatedListener: OnNavigatedListener? = null

    fun setNavigatedListener(listener: OnNavigatedListener?): MainNavController {
        mNavigatedListener = listener
        return this
    }

    fun saveState(): Bundle {
        val state = Bundle()
        state.putSerializable(BACK_STACK, backStack)
        return state
    }

    fun restoreState(state: Bundle?) {
        if (state != null) {
            if (state.containsKey(BACK_STACK))
                backStack = if (state.getSerializable(BACK_STACK) is LinkedList<*>)
                    state.getSerializable(BACK_STACK) as LinkedList<NavItem>
                else LinkedList<NavItem>()
        }
    }

    fun onBackHandled(): Boolean {
        if (backStack.size <= 1) return false
        val currItem = backStack.removeLast()
        val destItem = backStack.last

        placeFragment(destItem, currItem)
        dispatchNavigated(destItem)
        //
        return true
    }

    fun startNavigation() {
        navigate(if (backStack.isEmpty()) NavItem.primary else backStack.peekLast());
    }

    public fun navigate(destItem: NavItem) {
        val primary = destItem.isPrimary
        check(!(backStack.size == 0 && !primary)) { "Use navigatePrimary() method to start with primary item" }
        val currItem = backStack.peekLast()

        if (destItem == currItem) return

        val destIndex = backStack.lastIndexOf(destItem)
        if (destIndex != -1) {
            if (!primary) {
                backStack.removeAt(destIndex)
                if (destIndex > 0 && backStack[destIndex].isPrimary
                        && backStack[destIndex - 1].isPrimary) backStack.removeAt(destIndex)
            }
        }

        backStack.add(destItem)
        placeFragment(destItem, currItem)
        dispatchNavigated(destItem)
    }

    private fun placeFragment(destItem: NavItem, currItem: NavItem?) {
        val animOptions = AnimOptions()
                .setEnterAnim(R.anim.nav_default_enter_anim)
                .setExitAnim(R.anim.nav_default_exit_anim)
        placeFragment(destItem, currItem, animOptions)
    }

    private fun placeFragment(destItem: NavItem, currItem: NavItem?, animOptions: AnimOptions?) {
        val prevClazz: Class<out BaseMainFragment>? = currItem?.clazz
        val prevFragment = createOrRestoreFragment(prevClazz)

        destItem.clazz.let { clazz ->
            val fragment = createOrRestoreFragment(clazz) ?: return
            val ft = mFragmentManager.beginTransaction()

            if (animOptions != null)
                ft.setCustomAnimations(animOptions.enterAnim, animOptions.exitAnim)

            if (prevFragment != null)
                ft.detach(prevFragment)

            if (fragment.isDetached)
                ft.attach(fragment)
            else ft.add(containerId, fragment, clazz.simpleName)

            ft.setPrimaryNavigationFragment(fragment)
            ft.commit()
        }
    }

    private fun createOrRestoreFragment(clazz: Class<out BaseMainFragment>?): Fragment? {
        val tag = clazz?.simpleName ?: return null

        var fragment = mFragmentManager.findFragmentByTag(tag)

        if (fragment == null) {
            fragment = createFragment(clazz)
            fragment.retainInstance = true
        }

        return fragment
    }

    private fun createFragment(clazz: Class<out Fragment>): Fragment {
        val f: Fragment
        f = try {
            clazz.newInstance()
        } catch (e: Fragment.InstantiationException) {
            throw RuntimeException(e)
        } catch (e: InstantiationException) {
            throw RuntimeException(e)
        } catch (e: IllegalAccessException) {
            throw RuntimeException(e)
        }

        return f
    }

    private fun dispatchNavigated(destItem: NavItem?) {
        if (destItem != null
                && mNavigatedListener != null) mNavigatedListener!!.onNavigated(destItem)
    }

    interface OnNavigatedListener {
        fun onNavigated(destItem: NavItem)
    }

    class AnimOptions {
        @AnimRes
        @AnimatorRes
        var enterAnim = 0

        @AnimRes
        @AnimatorRes
        var exitAnim = 0

        @AnimRes
        @AnimatorRes
        var popEnterAnim = 0

        @AnimRes
        @AnimatorRes
        var popExitAnim = 0

        fun setEnterAnim(@AnimRes @AnimatorRes enterAnim: Int): AnimOptions {
            this.enterAnim = enterAnim
            return this
        }

        fun setExitAnim(@AnimRes @AnimatorRes exitAnim: Int): AnimOptions {
            this.exitAnim = exitAnim
            return this
        }

        fun setPopEnterAnim(@AnimRes @AnimatorRes popEnterAnim: Int): AnimOptions {
            this.popEnterAnim = popEnterAnim
            return this
        }

        fun setPopExitAnim(@AnimRes @AnimatorRes popExitAnim: Int): AnimOptions {
            this.popExitAnim = popExitAnim
            return this
        }
    }

    companion object {
        private const val BACK_STACK = "back_stack"
    }

}