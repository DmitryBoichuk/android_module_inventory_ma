package com.ibuildapp.inventory.navigation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ibuildapp.inventory.databinding.InventoryFragmentMainSearchBinding
import com.ibuildapp.inventory.fragment.BaseMainFragment

class MainSearchFragment : BaseMainFragment() {
    private lateinit var binding: InventoryFragmentMainSearchBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = InventoryFragmentMainSearchBinding.inflate(inflater)
        return binding.root
    }
}