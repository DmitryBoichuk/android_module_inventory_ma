package com.ibuildapp.inventory.navigation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ibuildapp.inventory.databinding.InventoryFragmentMainItemsBinding
import com.ibuildapp.inventory.fragment.BaseMainFragment

class MainItemsFragment : BaseMainFragment() {
    private lateinit var binding: InventoryFragmentMainItemsBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = InventoryFragmentMainItemsBinding.inflate(inflater)
        return binding.root
    }
}