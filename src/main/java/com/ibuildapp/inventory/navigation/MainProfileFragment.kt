package com.ibuildapp.inventory.navigation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ibuildapp.inventory.databinding.InventoryFragmentMainProfileBinding
import com.ibuildapp.inventory.fragment.BaseMainFragment

class MainProfileFragment : BaseMainFragment() {
    private lateinit var binding: InventoryFragmentMainProfileBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = InventoryFragmentMainProfileBinding.inflate(inflater)
        return binding.root
    }
}