package com.ibuildapp.inventory.navigation

import android.support.annotation.IdRes
import com.ibuildapp.inventory.R
import com.ibuildapp.inventory.fragment.BaseMainFragment

enum class NavItem(val main: Boolean, val isPrimary: Boolean,
                   val itemId: Int, val clazz: Class<out BaseMainFragment>) {
    ITEMS(true, true,
            R.id.item_dashboard,
            MainItemsFragment::class.java
    ),
    SEARCH(true, true,
            R.id.item_timeline,
            MainSearchFragment::class.java
    ),
    PROFILE(true, true,
            R.id.item_profile,
            MainProfileFragment::class.java
    );

    companion object {
        val primary: NavItem
            get() {
                for (navItem in values()) {
                    if (navItem.isPrimary) return navItem
                }
                throw IllegalStateException("No primary nav item found")
            }

        fun getByMenuItem(@IdRes menuItem: Int): NavItem? {
            if (menuItem != 0) {
                for (navItem in values()) {
                    if (navItem.itemId == menuItem) return navItem
                }
            }
            return null
        }
    }
}