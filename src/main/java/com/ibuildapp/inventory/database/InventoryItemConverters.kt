package com.ibuildapp.inventory.database

import android.arch.persistence.room.TypeConverter
import com.ibuildapp.inventory.model.InventoryPlans
import java.util.*

class InventoryItemConverters {
    @TypeConverter
    fun fromDate(date: Date?): Long? = date?.time

    @TypeConverter
    fun toDate(mills: Long?): Date? = if (mills == null) null else Date(mills)

    @TypeConverter
    fun fromList(list: List<String>?): String? = list?.joinToString(separator = ",")

    @TypeConverter
    fun toList(string: String?): List<String>? = string?.split(",")?.map { it.trim() }

    @TypeConverter
    fun fromIntList(list: List<Int>?): String? = list?.joinToString(separator = ",")

    @TypeConverter
    fun toIntList(string: String?): List<Int>? = string?.split(",")?.map { it.trim().toInt() }

    @TypeConverter
    fun fromInventoryPlan(plan: InventoryPlans): String = plan.name

    @TypeConverter
    fun toInventoryPlan(planName: String): InventoryPlans = InventoryPlans.valueOf(planName)
}