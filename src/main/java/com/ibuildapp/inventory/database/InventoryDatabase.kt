package com.ibuildapp.inventory.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import android.content.Context
import com.ibuildapp.inventory.model.InventoryItem
import com.ibuildapp.inventory.model.User

@Database(entities = [
    User::class,
    InventoryItem::class
], version = 1, exportSchema = false)
@TypeConverters(InventoryItemConverters::class)
abstract class InventoryDatabase : RoomDatabase() {
    abstract fun getDao(): InventoryDao

    companion object {

        @Volatile private var instance: InventoryDatabase? = null

        fun getInstance(context: Context, userId: String): InventoryDatabase {
            return instance ?: synchronized(this) {
                instance ?: buildDatabase(context, userId).also { instance = it }
            }
        }

        private fun buildDatabase(context: Context, userId: String): InventoryDatabase {
            return Room.databaseBuilder(context, InventoryDatabase::class.java, "inventory_database_$userId")
                    .build()
        }

        fun clearInstance() {
            instance = null
        }
    }
}