package com.ibuildapp.inventory.database

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Transformations
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.ibuildapp.inventory.model.InventoryItem
import com.ibuildapp.inventory.model.InventoryItemWithCounts
import com.ibuildapp.inventory.model.User
import java.util.*

@Dao
abstract class InventoryDao {
    @Insert (onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertUser(user: User)

    @Query("""WITH RECURSIVE cte_inventory_items AS (
	SELECT 
		inventory_item_id, 
		inventory_item_is_folder, 
		inventory_item_parent_id, 
		inventory_item_name,
		inventory_item_name_lower,
		inventory_item_sku, 
		inventory_item_notes,
		inventory_item_image_url, 
		inventory_item_price, 
		inventory_item_category, 
		inventory_item_quantity, 
		inventory_item_barcode, 
		inventory_item_barcode_format, 
		inventory_item_created_at, 
		inventory_item_updated_at, 
		inventory_item_id as inventory_group_id
    FROM inventory_items
    WHERE CASE WHEN :parentItemId IS NULL THEN inventory_item_parent_id IS NULL ELSE inventory_item_parent_id = :parentItemId END 

    UNION ALL

    SELECT 
		inv_items.inventory_item_id, 
		inv_items.inventory_item_is_folder, 
		inv_items.inventory_item_parent_id, 
		inv_items.inventory_item_name,
		inv_items.inventory_item_name_lower,
		inv_items.inventory_item_sku, 
		inv_items.inventory_item_notes,
		inv_items.inventory_item_image_url, 
		inv_items.inventory_item_price, 
		inv_items.inventory_item_category, 
		inv_items.inventory_item_quantity, 
		inv_items.inventory_item_barcode, 
		inv_items.inventory_item_barcode_format, 
		inv_items.inventory_item_created_at, 
		inv_items.inventory_item_updated_at, 
		cte_items.inventory_group_id as inventory_group_id
    FROM inventory_items inv_items
    JOIN cte_inventory_items cte_items ON cte_items.inventory_item_id = inv_items.inventory_item_parent_id
),
 items_with_folders AS (
SELECT
		core.inventory_item_id AS inventory_item_id, 
		core.inventory_item_is_folder AS inventory_item_is_folder, 
		core.inventory_item_parent_id AS inventory_item_parent_id, 
		core.inventory_item_name AS inventory_item_name,
		core.inventory_item_name_lower AS inventory_item_name_lower,
		core.inventory_item_sku AS inventory_item_sku, 
		core.inventory_item_notes AS inventory_item_notes,
		core.inventory_item_image_url AS inventory_item_image_url, 
		core.inventory_item_price AS inventory_item_price, 
		core.inventory_item_category AS inventory_item_category, 
		core.inventory_item_quantity AS inventory_item_quantity, 
		core.inventory_item_barcode AS inventory_item_barcode, 
		core.inventory_item_barcode_format AS inventory_item_barcode_format, 
		core.inventory_item_created_at AS inventory_item_created_at, 
		core.inventory_item_updated_at AS inventory_item_updated_at,
		folder_count.inventory_folder_count AS inventory_folder_count,
		files_count.inventory_files_count AS inventory_files_count
	FROM 
	(SELECT * FROM cte_inventory_items order by inventory_group_id) core 
	LEFT JOIN 
		(SELECT inventory_group_id, COUNT(*) AS inventory_folder_count FROM cte_inventory_items WHERE inventory_item_is_folder = 1 		AND CASE WHEN :parentItemId IS NULL THEN inventory_item_parent_id IS NOT NULL ELSE inventory_item_parent_id <> :parentItemId END		GROUP BY inventory_group_id) folder_count 
		ON core.inventory_group_id = folder_count.inventory_group_id
	LEFT JOIN 
		(SELECT inventory_group_id, COUNT(*) AS inventory_files_count FROM cte_inventory_items WHERE inventory_item_is_folder = 0 		AND CASE WHEN :parentItemId IS NULL THEN inventory_item_parent_id IS NOT NULL ELSE inventory_item_parent_id <> :parentItemId END		GROUP BY inventory_group_id) files_count 
		ON core.inventory_group_id = files_count.inventory_group_id
)
SELECT *  FROM items_with_folders WHERE CASE WHEN :parentItemId IS NULL THEN inventory_item_parent_id IS NULL ELSE inventory_item_parent_id = :parentItemId END""")
    abstract fun getItemsForParent(parentItemId: String?): LiveData<List<InventoryItemWithCounts>>

    @Query("""WITH RECURSIVE cte_inventory_items AS (
	SELECT 
		inventory_item_id, 
		inventory_item_is_folder, 
		inventory_item_parent_id, 
		inventory_item_name,
		inventory_item_name_lower,
		inventory_item_sku, 
		inventory_item_notes,
		inventory_item_image_url, 
		inventory_item_price, 
		inventory_item_category, 
		inventory_item_quantity, 
		inventory_item_barcode, 
		inventory_item_barcode_format, 
		inventory_item_created_at, 
		inventory_item_updated_at, 
		inventory_item_id as inventory_group_id
    FROM inventory_items
    WHERE CASE WHEN :parentItemId IS NULL THEN inventory_item_parent_id IS NULL ELSE inventory_item_parent_id = :parentItemId END 

    UNION ALL

    SELECT 
		inv_items.inventory_item_id, 
		inv_items.inventory_item_is_folder, 
		inv_items.inventory_item_parent_id, 
		inv_items.inventory_item_name,
		inv_items.inventory_item_name_lower,
		inv_items.inventory_item_sku, 
		inv_items.inventory_item_notes,
		inv_items.inventory_item_image_url, 
		inv_items.inventory_item_price, 
		inv_items.inventory_item_category, 
		inv_items.inventory_item_quantity, 
		inv_items.inventory_item_barcode, 
		inv_items.inventory_item_barcode_format, 
		inv_items.inventory_item_created_at, 
		inv_items.inventory_item_updated_at, 
		cte_items.inventory_group_id as inventory_group_id
    FROM inventory_items inv_items
    JOIN cte_inventory_items cte_items ON cte_items.inventory_item_id = inv_items.inventory_item_parent_id
),
 items_with_folders AS (
SELECT
		core.inventory_item_id AS inventory_item_id, 
		core.inventory_item_is_folder AS inventory_item_is_folder, 
		core.inventory_item_parent_id AS inventory_item_parent_id, 
		core.inventory_item_name AS inventory_item_name,
		core.inventory_item_name_lower AS inventory_item_name_lower,
		core.inventory_item_sku AS inventory_item_sku, 
		core.inventory_item_notes AS inventory_item_notes,
		core.inventory_item_image_url AS inventory_item_image_url, 
		core.inventory_item_price AS inventory_item_price, 
		core.inventory_item_category AS inventory_item_category, 
		core.inventory_item_quantity AS inventory_item_quantity, 
		core.inventory_item_barcode AS inventory_item_barcode, 
		core.inventory_item_barcode_format AS inventory_item_barcode_format, 
		core.inventory_item_created_at AS inventory_item_created_at, 
		core.inventory_item_updated_at AS inventory_item_updated_at, 
		core.inventory_group_id AS inventory_group_id,
		folder_count.inventory_folder_count AS inventory_folder_count,
		files_count.inventory_files_count AS inventory_files_count
	FROM 
	(SELECT * FROM cte_inventory_items order by inventory_group_id) core 
	LEFT JOIN 
		(SELECT inventory_group_id, COUNT(*) AS inventory_folder_count FROM cte_inventory_items WHERE inventory_item_is_folder = 1 		AND CASE WHEN :parentItemId IS NULL THEN inventory_item_parent_id IS NOT NULL ELSE inventory_item_parent_id <> :parentItemId END		GROUP BY inventory_group_id) folder_count 
		ON core.inventory_group_id = folder_count.inventory_group_id
	LEFT JOIN 
		(SELECT inventory_group_id, COUNT(*) AS inventory_files_count FROM cte_inventory_items WHERE inventory_item_is_folder = 0 		AND CASE WHEN :parentItemId IS NULL THEN inventory_item_parent_id IS NOT NULL ELSE inventory_item_parent_id <> :parentItemId END		GROUP BY inventory_group_id) files_count 
		ON core.inventory_group_id = files_count.inventory_group_id
		WHERE CASE WHEN :parentItemId IS NULL THEN inventory_item_parent_id IS NULL ELSE inventory_item_parent_id = :parentItemId END)
SELECT *  FROM items_with_folders WHERE inventory_item_name_lower LIKE :filter""")
    abstract fun getItemsForSearch(parentItemId: String?, filter: String): LiveData<List<InventoryItemWithCounts>>

    // Потому что не можем адекватно на SQL посчитать количество вложений в каждой из вложенных папок
    // и в итоге разбираем все в ручную
    @Query("""WITH RECURSIVE cte_inventory_items AS (
	SELECT 
		inventory_item_id, 
		inventory_item_is_folder, 
		inventory_item_parent_id, 
		inventory_item_name,
		inventory_item_name_lower,
		inventory_item_sku, 
		inventory_item_notes,
		inventory_item_image_url, 
		inventory_item_price, 
		inventory_item_category, 
		inventory_item_quantity, 
		inventory_item_barcode, 
		inventory_item_barcode_format, 
		inventory_item_created_at, 
		inventory_item_updated_at, 
		inventory_item_id as inventory_group_id
    FROM inventory_items
    WHERE CASE WHEN :parentItemId IS NULL THEN inventory_item_parent_id IS NULL ELSE inventory_item_parent_id = :parentItemId END 

    UNION ALL

    SELECT 
		inv_items.inventory_item_id, 
		inv_items.inventory_item_is_folder, 
		inv_items.inventory_item_parent_id, 
		inv_items.inventory_item_name,
		inv_items.inventory_item_name_lower,
		inv_items.inventory_item_sku, 
		inv_items.inventory_item_notes,
		inv_items.inventory_item_image_url, 
		inv_items.inventory_item_price, 
		inv_items.inventory_item_category, 
		inv_items.inventory_item_quantity, 
		inv_items.inventory_item_barcode, 
		inv_items.inventory_item_barcode_format, 
		inv_items.inventory_item_created_at, 
		inv_items.inventory_item_updated_at, 
		cte_items.inventory_group_id as inventory_group_id
    FROM inventory_items inv_items
    JOIN cte_inventory_items cte_items ON cte_items.inventory_item_id = inv_items.inventory_item_parent_id
)
SELECT *  FROM cte_inventory_items """)
    abstract fun innerGetItemsForSearchWithSubFolders(parentItemId: String?): LiveData<List<InventoryItemWithCounts>>

    // Сначала считаем количество под элементов, а потом удаляем те что не подошли под фильтр
    fun getItemsForSearchWithSubFolders(parentItemId: String?, filter: String, originalFilter: String): LiveData<List<InventoryItemWithCounts>> {
        return Transformations.map(innerGetItemsForSearchWithSubFolders(parentItemId)){ input ->
            val mapItems = mutableMapOf<String, InventoryItemWithCounts>()

            val rootList = mutableListOf<InventoryItemWithCounts>()
            input?.forEach {
                mapItems[it.id] = it
            }

            input.forEach {
                if (mapItems.containsKey(it.parentId))
                    mapItems[it.parentId]!!.subItems.add(it)
                else rootList.add(it)
            }

            calculateItemsCountInFolders(rootList)

            val filteredInput = mutableListOf<InventoryItemWithCounts>()
            input.forEach {
                if (it.nameLower.contains(originalFilter.toLowerCase(Locale.ROOT)))
                    filteredInput.add(it)
            }

            filteredInput
        }
    }

    private fun calculateItemsCountInFolders(rootList: MutableList<InventoryItemWithCounts>) {
        rootList.forEach {
            it.calculateItemsCount()
        }

        println(rootList)
    }

    @Query("SELECT * FROM inventory_items WHERE inventory_item_id = :itemId")
    abstract fun getItemById(itemId: String): InventoryItem

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertInventoryItem(it: InventoryItem)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertInventoryItems(items: List<InventoryItem>)

    @Query ("""WITH RECURSIVE cte_inventory_items AS (
        SELECT 
            inventory_item_id, 
            inventory_item_parent_id 
        FROM inventory_items
        WHERE inventory_item_parent_id = :parentItemId 
    
        UNION ALL
    
        SELECT 
            inv_items.inventory_item_id, 
            inv_items.inventory_item_parent_id 
        FROM inventory_items inv_items
        JOIN cte_inventory_items cte_items ON cte_items.inventory_item_id = inv_items.inventory_item_parent_id
    )
    SELECT distinct inventory_item_id FROM cte_inventory_items """)
    abstract fun getSubItemsForId(parentItemId: String): MutableList<String>

    @Query("delete from inventory_items where inventory_item_id in (:itemsForRemove)")
    abstract fun deleteItemsByIds(itemsForRemove: MutableList<String>)

    @Query("""WITH RECURSIVE cte_inventory_items AS (
	SELECT 
		inventory_item_id, 
		inventory_item_is_folder, 
		inventory_item_parent_id, 
		inventory_item_name,
		inventory_item_name_lower,
		inventory_item_sku, 
		inventory_item_notes,
		inventory_item_image_url, 
		inventory_item_price, 
		inventory_item_category, 
		inventory_item_quantity, 
		inventory_item_barcode, 
		inventory_item_barcode_format, 
		inventory_item_created_at, 
		inventory_item_updated_at, 
		inventory_item_id as inventory_group_id
    FROM inventory_items
    WHERE inventory_item_barcode = :scannedText

    UNION ALL

    SELECT 
		inv_items.inventory_item_id, 
		inv_items.inventory_item_is_folder, 
		inv_items.inventory_item_parent_id, 
		inv_items.inventory_item_name,
		inv_items.inventory_item_name_lower,
		inv_items.inventory_item_sku, 
		inv_items.inventory_item_notes,
		inv_items.inventory_item_image_url, 
		inv_items.inventory_item_price, 
		inv_items.inventory_item_category, 
		inv_items.inventory_item_quantity, 
		inv_items.inventory_item_barcode, 
		inv_items.inventory_item_barcode_format, 
		inv_items.inventory_item_created_at, 
		inv_items.inventory_item_updated_at, 
		cte_items.inventory_group_id as inventory_group_id
    FROM inventory_items inv_items
    JOIN cte_inventory_items cte_items ON cte_items.inventory_item_id = inv_items.inventory_item_parent_id
)
SELECT *  FROM cte_inventory_items """)
    abstract fun innerFindInventoryItemByCode(scannedText: String): List<InventoryItemWithCounts>?

    fun findInventoryItemByCode(scannedText: String): InventoryItemWithCounts? {
        val items = innerFindInventoryItemByCode(scannedText) ?: return null

        val mapItems = mutableMapOf<String, InventoryItemWithCounts>()

        val rootList = mutableListOf<InventoryItemWithCounts>()
        items.forEach {
            mapItems[it.id] = it
        }

        items.forEach {
            if (mapItems.containsKey(it.parentId))
                mapItems[it.parentId]!!.subItems.add(it)
            else rootList.add(it)
        }

        calculateItemsCountInFolders(rootList)
        items.forEach {
            if (it.barcode == scannedText)
                return it
        }

        return null
    }

    @Query("SELECT * FROM users WHERE user_id = :userId")
    abstract fun getUserById(userId: String): LiveData<User>

    @Query("SELECT COUNT(DISTINCT inventory_item_id) FROM inventory_items")
    abstract fun getItemsCount(): LiveData<Int>
}