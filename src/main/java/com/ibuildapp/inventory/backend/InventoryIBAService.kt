package com.ibuildapp.inventory.backend

import com.ibuildapp.inventory.backend.model.*
import okhttp3.MultipartBody
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import java.util.concurrent.TimeUnit

interface InventoryIBAService {
    @POST("/api/v1/item/barcode")
    fun search(
            @Header("Authorization") token: String,
            @Body request: LookupDataRequest
    ): Call<LookupDataResponse>

    @POST("/api/login")
    fun login(
            @Body request: LoginDataRequest
    ): Call<UserInfoResponse>

    @POST("/api/register")
    fun register(
            @Body request: RegisterDataRequest
    ): Call<UserInfoResponse>

    @POST("/api/v1/user")
    fun getUserInfo(
            @Header("Authorization") token: String
    ): Call<UserInfoResponse>

    @POST("/api/v1/updates")
    fun getUpdates(
            @Header("Authorization") token: String,
            @Body request: UpdatesDataRequest
    ): Call<UpdatesDataResponse>

    @Multipart
    @POST("api/v1/item/update")
    fun updateItem(
            @Header("Authorization") token: String,
            @PartMap params: Map<String, @JvmSuppressWildcards RequestBody>,
            @Part files: List<MultipartBody.Part>? = null

    ): Call<ItemResponse>

    @Multipart
    @POST("api/v1/item/add")
    fun addItem(
            @Header("Authorization") token: String,
            @PartMap params: Map<String, @JvmSuppressWildcards RequestBody>,
            @Part files: List<MultipartBody.Part>?

    ): Call<ItemResponse>

    object Factory {
        fun create(): InventoryIBAService {
            val interceptor = HttpLoggingInterceptor()

            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
            val client: OkHttpClient = OkHttpClient.Builder()
                    .readTimeout(30, TimeUnit.SECONDS) //socket timeout (GET)
                    .writeTimeout(30, TimeUnit.SECONDS) //socket timeout (POST, PUT, etc)
                    .addInterceptor(interceptor)
                    .build()

            val retrofit = Retrofit.Builder()
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
//                    .baseUrl("http://192.168.99.100:3000")
                    .baseUrl("https://sandbox.britecheck.com/")
                    .build()
            return retrofit.create(InventoryIBAService::class.java)
        }


    }
}