package com.ibuildapp.inventory.backend.model

import com.google.gson.annotations.SerializedName
import okhttp3.MultipartBody

data class ItemResponse (
    var error: ResponseErrorData?,
    var data: ServerItemData? = null
)

data class ServerMultipartItem (
        var id :Int?,

        @SerializedName("inventory_id")
        var inventoryId :Int,

        @SerializedName("parent_id")
        var parentId :Int?,

        @SerializedName("is_folder")
        var isFolder :Boolean,

        var name: String?,

        var description: String?,

        var sku: String?,

        var barcode: String?,

        @SerializedName("barcode_format")
        var barcodeFormat: String?,

        var checked: Boolean = true,

        var quantity: Int?,

        var price: Float?,

        var order: Int? = null,

        var tags: List<Int?> = listOf(),

        var active: Boolean? = true,

        var images: List<String>?,

        var files: List<MultipartBody.Part>? = null
)




