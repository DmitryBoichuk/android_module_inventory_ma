package com.ibuildapp.inventory.backend.model

import com.ibuildapp.inventory.model.InventoryItem
import java.util.*

class UpdatesDataResponse {
    var error: ResponseErrorData? = null
    var data: UpdatesData? = null
}

data class UpdatesDataRequest (
    var timestamp: Long? = 0
)

class UpdatesData {
    var timestamp: Long? = null
    var inventories: List<UpdatesInventoryData>? = listOf()
    var categories: List<ServerCategory>? = listOf()
    var items: List<ServerItemData>? = listOf()
    var tags: List<UpdatesTagData>? = listOf()

}

class UpdatesInventoryData{
    var id :String? = null
    var name: String? = null
    var description: String? = null
    var active: Boolean? = true
}

data class ServerCategory (
    val id :String,
    val inventory: Int,
    val parentId: Int?,

    val name: String?,
    val description: String?,
    val images: List<String>?,

    val tags: List<Int>?,
    val active: Boolean,
    val createdAt: Long?,

    val barcode: String?,
    val barcodeFormat: String?,
    val updatedAt: Long?
) {
    fun toLocalCategory(): InventoryItem {
        return InventoryItem(
                id = "${id}_folder",
                isFolder = true,
                parentId = if(parentId != null) "${parentId}_folder" else null,

                inventoryId = inventory,
                active = active,
                name = name,

                notes = description,
                nameLower = name?.toLowerCase(Locale.ROOT) ?: "",
                createdAt = createdAt?.let { createdAt -> Date(createdAt) },

                updatedAt = updatedAt?.let { updatedAt -> Date(updatedAt) },
                imageUrl = images,
                barcode = barcode,
                barcodeFormat = barcodeFormat
        )
    }
}

data class ServerItemData (
    val id :String,
    val inventory: Int,
    val parentId: Int?,
    val category: Int?,

    val name: String?,
    val description: String?,
    val sku: String?,

    val barcode: String?,
    val barcodeFormat: String?,
    val checked: Boolean?,

    val quantity: Int?,
    val price: Float?,
    val order: Int?,

    val images: List<String>?,
    val tags: List<Int>?,
    val active: Boolean,

    val createdAt: Long?,
    val updatedAt: Long?
) {
    fun toLocalItem(folder: Boolean): InventoryItem {
        val convertedId = if (folder) "${id}_folder" else "${id}_item"
        val checkedParentId = if (folder) parentId else category

        return InventoryItem(
                id = convertedId,
                isFolder = folder,
                parentId = if(checkedParentId != null) "${checkedParentId}_folder" else null,

                inventoryId = inventory,
                name = name,
                active = active,

                notes = description,
                sku = sku,
                quantity = quantity,

                price = price,
                nameLower = name?.toLowerCase(Locale.ROOT) ?: "",
                barcode = barcode,

                barcodeFormat = barcodeFormat,
                createdAt = createdAt?.let { createdAt -> Date(createdAt)},
                updatedAt = updatedAt?.let { updatedAt -> Date(updatedAt)},
                
                imageUrl = images
        )
    }
}

class UpdatesTagData{
    var id :String? = null
    var inventoryId: Int? = null
    var value: String? = null
}
