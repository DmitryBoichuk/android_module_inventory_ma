package com.ibuildapp.inventory.backend.model
import com.ibuildapp.inventory.model.Result

class LoginDataResponse {
    var error: LoginErrorData? = null
    var data: LoginData? = null
}

class LoginErrorData {
    var type: Int = 200
    var message: String? = null
}

class LoginData {
    var id: String? = null
    var token: String? = null
    var isOwner: Boolean? = false
    var subscribeStatus: String? = null
    var inventory: List<Int>? = null
}

data class LoginDataRequest(val email: String, val password: String)

class LoginResult (val result: Result, val error: String? = null)

data class RegisterDataRequest(val firstName: String, val lastName: String, val email: String, val password: String)