package com.ibuildapp.inventory.backend.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class LookupDataResponse(
    val error: ResponseErrorData?,
    val data: Products
)

class Products {
    @SerializedName("products")
    @Expose
    var products: List<Product>? = null
}
data class LookupDataRequest(val barcode: String, val action: String)

enum class LookupStatus{
    SUCCESS, ERROR, NOTHING_FOUND
}

data class LookupResult(val status: LookupStatus, val product: Product? = null, val error: String? = null)

class Product: Serializable {
    @SerializedName("barcode_number")
    @Expose
    var barcodeNumber: String? = null

    @SerializedName("barcode_type")
    @Expose
    var barcodeType: String? = null

    @SerializedName("barcode_formats")
    @Expose
    var barcodeFormats: String? = null

    @SerializedName("mpn")
    @Expose
    var mpn: String? = null

    @SerializedName("model")
    @Expose
    var model: String? = null

    @SerializedName("asin")
    @Expose
    var asin: String? = null

    @SerializedName("product_name")
    @Expose
    var productName: String? = null

    @SerializedName("title")
    @Expose
    var title: String? = null

    @SerializedName("category")
    @Expose
    var category: String? = null

    @SerializedName("manufacturer")
    @Expose
    var manufacturer: String? = null

    @SerializedName("brand")
    @Expose
    var brand: String? = null

    @SerializedName("label")
    @Expose
    var label: String? = null

    @SerializedName("author")
    @Expose
    var author: String? = null

    @SerializedName("publisher")
    @Expose
    var publisher: String? = null

    @SerializedName("artist")
    @Expose
    var artist: String? = null

    @SerializedName("actor")
    @Expose
    var actor: String? = null

    @SerializedName("director")
    @Expose
    var director: String? = null

    @SerializedName("studio")
    @Expose
    var studio: String? = null

    @SerializedName("genre")
    @Expose
    var genre: String? = null

    @SerializedName("audience_rating")
    @Expose
    var audienceRating: String? = null

    @SerializedName("ingredients")
    @Expose
    var ingredients: String? = null

    @SerializedName("nutrition_facts")
    @Expose
    var nutritionFacts: String? = null

    @SerializedName("color")
    @Expose
    var color: String? = null

    @SerializedName("format")
    @Expose
    var format: String? = null

    @SerializedName("package_quantity")
    @Expose
    var packageQuantity: String? = null

    @SerializedName("size")
    @Expose
    var size: String? = null

    @SerializedName("length")
    @Expose
    var length: String? = null

    @SerializedName("width")
    @Expose
    var width: String? = null

    @SerializedName("height")
    @Expose
    var height: String? = null

    @SerializedName("weight")
    @Expose
    var weight: String? = null

    @SerializedName("release_date")
    @Expose
    var releaseDate: String? = null

    @SerializedName("description")
    @Expose
    var description: String? = null

    @SerializedName("features")
    @Expose
    var features: List<String>? = null

    @SerializedName("images")
    @Expose
    var images: List<String>? = null

    @SerializedName("stores")
    @Expose
    var stores: List<Any>? = null

}