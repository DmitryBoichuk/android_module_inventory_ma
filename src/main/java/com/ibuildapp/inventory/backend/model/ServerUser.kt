package com.ibuildapp.inventory.backend.model

import com.google.gson.annotations.SerializedName
import com.ibuildapp.inventory.model.User

class UserInfoResponse (
    var error: ResponseErrorData?,
    var data: ServerUser?
    )

data class ResponseErrorData (
    var type: Int? = 0,
    var message: String?
)

class PlanInfo{
    var name: String = "unknown"
    var limitItems: Int = 0
    var status: String? = null
}

data class ServerUser(
    val id: String,
    val role: String,
    val email: String,

    val firstName: String,
    val lastName: String,
    val active: Boolean,

    val inventories: List<Int>,

    @SerializedName(value = "updateAt")
    val updatedTime: Long,
    val isOwner: Boolean,

    val plan: PlanInfo,
    val token: String,
    var buttonName: String,
    var buttonLink: String
) {
    fun toLocalUser(): User {
        return User(id = id, firstName = firstName,
                lastName = lastName, email = email,
                planName = plan.name, planLimit = plan.limitItems,
                buttonLink = buttonLink, buttonName = buttonName,
                isOwner = isOwner, inventories = inventories )
    }
}
