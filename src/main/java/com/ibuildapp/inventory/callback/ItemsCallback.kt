package com.ibuildapp.inventory.callback

interface ItemsCallback {
    fun onFolderClick(id: String, name: String)

    fun onSearchClick()

    fun onEditFolderClick(id: String)

    fun onDeleteFolderClick(id: String, isFolder: Boolean)

    fun onItemClick(id: String, name: String)

    fun deleteItemClick(id: String)
}