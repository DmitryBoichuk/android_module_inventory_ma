package com.ibuildapp.inventory

import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.design.widget.BottomNavigationView.OnNavigationItemReselectedListener
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.fragment.FragmentNavigator
import com.ibuildapp.inventory.databinding.InventoryMainBinding
import com.ibuildapp.inventory.navigation.MainNavController
import com.ibuildapp.inventory.navigation.NavItem
import com.ibuildapp.inventory.stuff.OnBackListener

class InventoryActivity : BaseActivity() {
    private val NAV_CONTROLLER_STATE = "nav_controller_state"

    private lateinit var navController: MainNavController
    private lateinit var binding: InventoryMainBinding

    private var internalOnBackPressedListener: OnBackListener? = null

    private val mItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item: MenuItem ->
        NavItem.getByMenuItem(item.itemId)?.let {
            navController.navigate(it)
        }
        true
    }

    private val mItemReselectedListener = OnNavigationItemReselectedListener { item: MenuItem ->
        NavItem.getByMenuItem(item.itemId)?.let {
            navigateReselected(it)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = InventoryMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        initViews(savedInstanceState)
    }

    private fun initViews(savedInstanceState: Bundle?) {
        var navControllerState: Bundle? = null

        setSupportActionBar(binding.toolbar)
        supportActionBar?.let {
            it.title = ""
        }

        binding.vgBottomNavigation.setOnNavigationItemSelectedListener(mItemSelectedListener)
        binding.vgBottomNavigation.setOnNavigationItemReselectedListener(mItemReselectedListener)

        if (savedInstanceState != null)
            navControllerState = savedInstanceState.getBundle(NAV_CONTROLLER_STATE)

        navController = MainNavController(supportFragmentManager, R.id.vg_main_container)
        navController.restoreState(navControllerState)

        navController.setNavigatedListener (object : MainNavController.OnNavigatedListener {
            override fun onNavigated(destItem: NavItem) {
                markItem(destItem)
            }
        })

        navController.startNavigation()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putBundle(NAV_CONTROLLER_STATE, navController.saveState())
    }

    private fun markItem(navItem: NavItem) {
        val menu: Menu = binding.vgBottomNavigation.getMenu()
        menu.findItem(navItem.itemId).isChecked = true
    }

    private fun navigateReselected(navItem: NavItem) {
        val tag: String = navItem.clazz.simpleName
        val main = supportFragmentManager.findFragmentByTag(tag)

        main?.let {
            val navHost = main.childFragmentManager.fragments[0]
            if (navHost != null) {
                val navController = Navigation.findNavController(this, navHost.id)
                val startDest = navController.graph.startDestination
                popUntilStart(navController, startDest)
            }
        }
    }

    private fun popUntilStart(navController: NavController, startDest: Int) {
        val fn = navController.navigatorProvider.getNavigator(FragmentNavigator::class.java)
        var pop = true

        while (pop) {
            fn.onSaveState()?.let {state ->
                val key = state.keySet().iterator().next()

                state.getIntArray(key)?.let {ids ->
                    val backStackSize = ids.size

                    pop = backStackSize > 1
                    if (pop) {
                        navController.popBackStack()
                        navController.popBackStack(startDest, false)
                    }
                }
            }
        }
    }

    override fun onBackPressed() {
        if (internalOnBackPressedListener != null && internalOnBackPressedListener!!.onBackPressed())
            return

        if (!navController.onBackHandled()) super.onBackPressed()
    }

    fun setInternalOnBackPressedListener(internalOnBackPressedListener: OnBackListener) {
        this.internalOnBackPressedListener = internalOnBackPressedListener
    }

    override fun getToolbar() = binding.toolbar
}